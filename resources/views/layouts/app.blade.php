<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="yandex-verification" content="c91652f826bcf52c" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('layouts.partials.favicon')

    <meta name="yandex-verification" content="993f7e2fc546e96e" />

    @if(!View::hasSection('head-meta'))
        <title>{{ site_config('site_name')->value }}</title>
        <meta name="keywords" content="{{ site_config('metakey')->value }}">
        <meta name="description" content="{{ site_config('metadesc')->value }}">
    @endif

    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400i|Roboto:400,400i,700,700i&display=swap&subset=cyrillic" rel="stylesheet">
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

    @yield('head-meta')

    <script>
        window.mxtcore = @json(['csrfToken' => csrf_token()])
    </script>

    <style> {!! includePublicTextFile('/css/reboot.css') !!}  </style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show ">

@if(agent()->isMobile())
    @include('layouts.partials.header.mobile_header')
@else
    @include('layouts.partials.header.header')
@endif

<div id="app" class="loaded" style="flex: 1;">
    @yield('top')
    <div class="container">
        @yield('breadcrumbs')
        @yield('content_top')
        <div class="row">

            <div class="col-md-{{ View::hasSection('aside_right') ? '9' : '12'}}">
                @yield('content')
            </div>

            @hasSection('aside_right')
                <div class="col-md-3">
                    @yield('aside_right')
                </div>
            @endif
        </div>
        @yield('content_bottom')
    </div>
    <modal-dialog></modal-dialog>
</div>

@include('layouts.partials.footer')

<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,700,700i,800,800i" rel="stylesheet">
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<script src="{{ asset('js/app.js') }}"></script>

@if(app()->environment('production'))
    @include('layouts.partials.metrics')
@endif

</body>
</html>

<!-- {{ (microtime(true) - LARAVEL_START) }} -->
