<footer class="footer">
    <div class="container">
        <div class="footer_content pt-4 pb-2">
            <div class="row">
                <address class="col-md-6">
                    420139 РТ г. Казань, ул. К. Габишева, 2 <br>
                    <a href="{{ route('frontend.content.category', ['categories' => 'kontakty']) }}">Схема проезда</a>
                    <br>
                    +7 (843) 233-37-17 | +7 (843) 249-12-38 <br>
                    Заказать звонок <br>
                </address>
                <div class="col-md-6">
                    <ul class="footer_menu" style="height: 100px">
                        <li><a href="{{ url('realty/filters?category=1') }}">Продажа квартир</a></li>
                        <li><a href="{{ url('realty/filters?category=3') }}">Продажа домов</a></li>
                        <li><a href="{{ route('frontend.content.category', ['category' => 'uslugi/ipoteka']) }}">Ипотека</a></li>
                        <li><a href="{{ url('uslugi/nasledstvo') }}">Наследство</a></li>
                        <li><a href="{{ url('uslugi/mezhevanie') }}">Межевание</a></li>
                        <li>
                            <a href="{{ route('frontend.content.category', ['category' => 'uslugi']) }}">Услуги</a>
                        </li>
                        <li><a href="{{ route('frontend.content.reviews') }}">Отзывы</a></li>
                        <li><a href="#">Вакансии</a></li>
                    </ul>
                </div>
            </div>

            <div class="row footer-textSmall">
                <div class="col-md-8">
                    <a href="">Пользовательское соглашение</a> | <a href="">Условия обработки и хранения персональных
                        данных</a> <br>
                    Информация, представленная на сайте не является договором публичной оферты.
                </div>
            </div>
        </div>
    </div>

    <div class="footer_content footer_content-darken footer-textSmall py-2">
        <div class="container">
            © 2009-{{ date('Y') }}, «Ваш надежный риелтор и НЛБ» <a href="{{ route('home') }}">агентство недвижимости в
                Казани</a>
            <a href="{{route('frontend.content.sitemap')}}" class="ml-4"> Карта сайта</a>

            @auth
                <a href="{{ url('/logout') }}"
                   class="ml-4"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                >
                    <i class="fa fa-lock"></i> Выход
                    <form id="logout-form"
                          action="{{ url('/logout') }}"
                          method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </a>
            @else
                <a class="ml-4" href="{{ route('login') }}">
                    <i class="icon-login"></i> Вход
                </a>
            @endif
        </div>
    </div>
</footer>
