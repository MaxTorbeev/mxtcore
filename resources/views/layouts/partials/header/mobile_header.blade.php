<header class="header header-light pb-1">
    <div class="container">
        <div class="header_brand">
            <a href="{{ route('home') }}" class="header_brand">
                <img src="{{ config('mxtcore.frontend.logo.full') }}" class="img-fluid header_brand_img"
                     alt="{{ site_config('site_name')->value }}">
            </a>
        </div>

        <div class="row no-gutters pb-2">
            <div class="col-6">
                <a href="#"
                   class="btn btn-sm btn-success-withShadow btn-rounded"
                   onclick='event.preventDefault(); modalShow("back-call",  @json(["apiUrl" => route("frontend.content.feedback.backcall")]))'
                >
                    Заказать звонок
                </a>
            </div>

            <div class="col-6 align-self-center pl-2">
                <div class=" text-right">
                    <a href="tel:{{ site_config('site_header_phone')->value }}">
                        {{ parse_phone(site_config('site_header_phone')->value ) }}
                    </a>
                </div>
            </div>
        </div>

        <div class="header_horizontalMenu header_horizontalMenu-textLight">
            @widget('categories')
        </div>
    </div>
</header>
