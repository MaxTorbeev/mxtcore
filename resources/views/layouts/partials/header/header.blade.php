<header class="header header-light">
    <div class="container">
        <div class="row text-center text-sm-left py-3">
            <div class="col-md-5 align-self-center">
                <div class="header_brand">
                    <a href="{{ route('home') }}" class="header_brand">
                        <img src="{{ config('mxtcore.frontend.logo.full') }}" class="img-fluid header_brand_img" alt="{{ site_config('site_name')->value }}">
                    </a>
                </div>
            </div>
            <div class="col-md-7 align-self-center">
                <div class="row">
                    <div class="col-auto align-self-center ml-md-auto">
                        <a href="#" class="btn btn-success-withShadow btn-rounded"
                           onclick='event.preventDefault(); modalShow("back-call",  @json(["apiUrl" => route("frontend.content.feedback.backcall")]))'
                        >
                            Заказать обратный звонок
                        </a>
                    </div>
                    <div class="col-auto align-self-center text-sm-center">
                        <a href="tel:{{ site_config('site_header_phone')->value }}" class="header_phone">
                            <span class="header_phone-small">
                                {{ $headerPhone['code'] }}({{ $headerPhone['area'] }})
                            </span>
                            <span class="header_phone-large">
                                {{ $headerPhone['prefix'] }}-{{ $headerPhone['number'] }}
                            </span>
                        </a>
                    </div>
                    <div class="col-auto align-self-center">
                        <ul class="socialMenu socialMenu-inHeader">
                            @foreach(config('mxtcore.frontend.socials') as $socialName => $socialUrl)
                                <li>
                                    <a href="{{ url($socialUrl) }}"
                                       class="socialMenu_item socialMenu_item-{{ $socialName }}"
                                       target="_blank"
                                       rel="nofollow">
                                        <i class="fa fa-{{$socialName}}"></i>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="header_horizontalMenu header_horizontalMenu-textLight">
        @widget('categories')
    </div>
</header>