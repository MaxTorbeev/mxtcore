@if (count($breadcrumbs))
    <ol class="breadcrumb mt-3">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$loop->last)
                <li class="breadcrumb-item">
                    <a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>

                    <i class="fas fa-chevron-right"></i>
                </li>
            @else
                <li class="breadcrumb-item active">
                    {{ $breadcrumb->title }}
                </li>
            @endif
        @endforeach
    </ol>

@endif