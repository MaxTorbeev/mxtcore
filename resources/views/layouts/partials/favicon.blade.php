<link rel="apple-touch-icon" href="{{ asset('images/templates/favicons/apple-touch-icon-57x57.png') }}" sizes="57x57">
<link rel="apple-touch-icon" href="{{ asset('images/templates/favicons/apple-touch-icon-60x60.png') }}" sizes="60x60">
<link rel="apple-touch-icon" href="{{ asset('images/templates/favicons/apple-touch-icon-72x72.png') }}" sizes="72x72">
<link rel="apple-touch-icon" href="{{ asset('images/templates/favicons/apple-touch-icon-76x76.png') }}" sizes="76x76">
<link rel="apple-touch-icon" href="{{ asset('images/templates/favicons/apple-touch-icon-114x114.png') }}" sizes="114x114">
<link rel="apple-touch-icon" href="{{ asset('images/templates/favicons/apple-touch-icon-120x120.png') }}" sizes="120x120">
<link rel="apple-touch-icon" href="{{ asset('images/templates/favicons/apple-touch-icon-144x144.png') }}" sizes="144x144">
<link rel="apple-touch-icon" href="{{ asset('images/templates/favicons/apple-touch-icon-152x152.png') }}" sizes="152x152">
<link rel="apple-touch-icon" href="{{ asset('images/templates/favicons/apple-touch-icon-180x180.png') }}"  sizes="180x180">
<link rel="icon" href="{{ asset('images/templates/favicons/favicon-16x16.png') }}" type="image/png" sizes="16x16">
<link rel="icon" href="{{ asset('images/templates/favicons/favicon-32x32.png') }}" type="image/png" sizes="32x32">
<link rel="icon" href="{{ asset('images/templates/favicons/favicon-96x96.png') }}" type="image/png" sizes="96x96">
<link rel="icon" href="{{ asset('images/templates/favicons/favicon-160x160.png') }}" type="image/png" sizes="160x160">
<link rel="icon" href="{{ asset('images/templates/favicons/favicon-192x192.png') }}" type="image/png" sizes="192x192">
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">