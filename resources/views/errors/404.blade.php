@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center">Страница не найдена</h1>

        <div class="card">
            <div class="card-body">
                Вы не можете посетить текущую страницу по причине:
                <ul>
                    <li>просроченная закладка/избранное</li>
                    <li>пропущен адрес</li>
                    <li>поисковый механизм, у которого просрочен список для этого сайта</li>
                    <li>у вас нет права доступа на эту страницу</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
