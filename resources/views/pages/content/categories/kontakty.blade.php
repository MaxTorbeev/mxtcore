@extends('content::frontend.categories.show')

@inject('postsWidget', 'MaxTor\Content\Widgets\PostsWidget')
@inject('contactsWidget', 'MaxTor\Personal\Widgets\ContactsWidget')

@section('content')
    <div class="container">
        <section class="section content content_list">
            <h1>{{ $category->name }}</h1>
            @foreach($categoryPosts as $post)
                <h5 class="h5">
                    <a href="{{ $post->frontend_url }}">
                        {{ $post->name }}
                    </a>
                </h5>
            @endforeach
            <div class="row">
                <div class="col-md-4">
                    @if($category->description)
                        <div class="section_description mb-md-5">
                            {!! $category->description   !!}
                        </div>
                    @endif
                </div>
                <div class="col-md-8">
                    <div class="yandex-map"
                         style="height: 400px"
                         data-map-height="400"
                         data-zoom="16"
                         data-map-text="Ваш надежный риелтор и НЛБ"
                         data-map-coordinate="55.740656,49.226374">
                    </div>
                </div>
            </div>

            <ul class="list-group list-group-flush">
                @foreach($category->children as $children)
                    <li class="list-group-item">
                        <a href="{{ $children->frontend_url }}">{{ $children->name }}</a>
                    </li>
                @endforeach
            </ul>

            {!! $contactsWidget->handle(['view' => 'cards']) !!}

        </section>
    </div>
@endsection