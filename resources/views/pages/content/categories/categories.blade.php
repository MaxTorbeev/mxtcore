@extends('content::frontend.categories.show')

@inject('postsWidget', 'MaxTor\Content\Widgets\PostsWidget')
@inject('contactsWidget', 'MaxTor\Personal\Widgets\ContactsWidget')

@section('content')
    <div class="container">
        <section class="section content content_list">
            <h1>{{ $category->name }}</h1>

            @if($category->description)
                <div class="section_description mb-md-5">
                    {!! $category->description   !!}

                    <a href="#" class="btn btn-success-withShadow btn-rounded"
                       onclick='event.preventDefault(); modalShow("back-call",  @json(["apiUrl" => route("frontend.content.feedback.backcall")]))'
                    >
                        Заказать консультацию
                    </a>

                </div>
            @endif


        </section>
    </div>
@endsection
