@extends('layouts.app')

@inject('postsWidget', 'MaxTor\Content\Widgets\PostsWidget')
@inject('contactsWidget', 'MaxTor\Personal\Widgets\ContactsWidget')
@inject('carouselsWidget', 'MaxTor\Content\Widgets\CarouselsWidget')

@section('top')
    <div class="realty_top"></div>
    <div class="container">
        <a href="http://grrt.info/" class="realty_top_grrt_btn" target="_blank" rel="nofollow">
            <img src="{{ asset('/images/templates/realty/logo/grrt_logo.png') }}" alt="">
        </a>
    </div>
    @widget('realtyFiltersForms')
@endsection

@section('content')
    <section class="section-abouts mt-4">
        <div class="row card-group">
            <article class="card card-aboutService card-withImageOverlay card-withShadow card-hovered mx-md-3">
                <img class="card-img-overlay" src="{{ asset('/images/templates/sections/abouts/img_4.jpg') }}" alt="">
                <div class="card-header">
                    <h4>НОВАЯ УСЛУГА АН «ВАШ НАДЕЖНЫЙ РИЕЛТОР & НЛБ»</h4>
                </div>
                <div class="card-body pb-5">
                    <p class="card-text">
                        Наше агентство является Удостоверяющим центром! Подпишите договор купли-продажи и отправьте
                        документы на регистрацию без посещения Росреестра или МФЦ.
                        Это съэкономит ваше время и нервы! Запись по телефону (843)2333-717
                    </p>
                </div>
                <div class="card-footer">
                    <div class="row">
                        @if(!agent()->isMobile())
                            <div class="col-md-3">
                                <img class="card-footer-more_img  card-footer-img-rounded"
                                     src="{{ asset('/images/templates/sections/abouts/img_4.jpg') }}" alt=""
                                >
                            </div>
                        @endif
                        <div class="col-md-9">
                            <a href="{{ url('/articles/servis-elektronnaya-registraciya') }}" class="btn btn-success-withShadow btn-rounded btn-block card-footer-more">
                                Подробнее
                            </a>
                        </div>
                    </div>
                </div>
            </article>
            <article class="card card-aboutService card-withImageOverlay card-withShadow card-hovered mx-3">
                <img class="card-img-overlay" src="{{ asset('/images/templates/sections/abouts/img.jpg') }}" alt="">
                <div class="card-header">
                    <h4>Рассрочка без переплат</h4>
                </div>
                <div class="card-body pb-5">
                    <p class="card-text">
                        Вы можете стать счастливым обладателем новой квартиры сразу, либо в рассрочку до 10 лет без %
                        и переплат не посещая банка! Планируй свой бюджет!
                    </p>
                </div>
                <div class="card-footer">
                    <div class="row">
                        @if(!agent()->isMobile())
                        <div class="col-md-3">
                            <img class="card-footer-more_img  card-footer-img-rounded" src="{{ asset('/images/templates/sections/abouts/img_small.jpg') }}" alt="">
                        </div>
                        @endif
                        <div class="col-md-9">
                            <a href="#" class="btn btn-success-withShadow btn-rounded btn-block card-footer-more">
                                Подробнее
                            </a>
                        </div>
                    </div>
                </div>
            </article>
            <article class="card card-aboutService card-withImageOverlay card-withShadow card-hovered mx-3">
                <img class="card-img-overlay" src="{{ asset('/images/templates/sections/abouts/img_2.jpg') }}" alt="">
                <div class="card-header">
                    <h4>Возможность приобретения по мат. капиталу и военному сертификату</h4>
                </div>
                <div class="card-body pb-5">
                    <p class="card-text">
                        Новостройку можно приобрести имея на руках любой сертификат: материнский капитал или военный
                        сертификат!
                    </p>
                </div>
                <div class="card-footer">
                    <div class="row">
                        @if(!agent()->isMobile())
                        <div class="col-md-3">
                            <img class="card-footer-more_img  card-footer-img-rounded"
                                 src="{{ asset('/images/templates/sections/abouts/img_2.jpg') }}" alt="">
                        </div>
                        @endif
                        <div class="col-md-9">
                            <a href="#" class="btn btn-success-withShadow btn-rounded btn-block card-footer-more">
                                Подробнее
                            </a>
                        </div>
                    </div>
                </div>
            </article>
            <article class="card card-aboutService card-withImageOverlay card-withShadow card-hovered mx-3">
                <img class="card-img-overlay" src="{{ asset('/images/templates/sections/abouts/img_3.jpg') }}" alt="">
                <div class="card-header">
                    <h4>Купите новую квартиру выгодно!</h4>
                </div>
                <div class="card-body pb-5">
                    <p class="card-text">
                        В акции участвуют жилые комплексы "Новые горки", "Яркий", "Беседа", "Грин Сити", "Манхэттен", "Столичный", "Станция "Спортивная",
                        "Сказочный лес", "Палитра", "Времена года", "Залесный сити" и "Южный парк".
                    </p>
                </div>
                <div class="card-footer">
                    <div class="row">
                        @if(!agent()->isMobile())
                        <div class="col-md-3">
                            <img class="card-footer-more_img  card-footer-img-rounded"
                                 src="{{ asset('/images/templates/sections/abouts/img_3.jpg') }}" alt="">
                        </div>
                        @endif
                        <div class="col-md-9">
                            <a href="http://xn----7sbajnkidicdf6aepba9akiv.xn--p1ai/" target="_blank" rel="nofollow" class="btn btn-success-withShadow btn-rounded btn-block card-footer-more">
                                Подробнее
                            </a>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <div class="row">
        <div class="col-md-9">
            <section class="section mt-4">
                <div class="card card-withShadow card-strongShadow">
                    <div class="card-heading">
                        <h4>
                            Новости и статьи
                        </h4>
                        <a href="{{ route('frontend.content.category', ['slug' => 'news']) }}" class="text-right">
                            Все новости и статьи
                        </a>
                    </div>
                    <div class="card-body">
                        {!! $postsWidget->handle(['view' => 'carousel', 'category_slug' => 'news']) !!}
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-3">
            <div class="mt-4"></div>
            <iframe style="width: 100%; height: 250px;" src="https://www.youtube.com/embed/ulzgXGRURbA" frameborder="0" allowfullscreen=""></iframe>
        </div>
    </div>

    <div class="row">
        <div class="col-md-9">
            <section class="section mt-4">
                <div class="card card-withShadow card-strongShadow mb-4">
                    <div class="card-heading">
                        <h4>
                            Наша команда
                        </h4>
                    </div>
                    <div class="card-body">
                        {!! $contactsWidget->handle(['view' => 'carousel']) !!}
                    </div>
                </div>
                {!! $carouselsWidget->handle(['view' => 'partners', 'slug' => 'partners']) !!}
            </section>
        </div>

        <div class="col-md-3">
            <div class="mt-4">
                <iframe style="width: 100%; height: 250px;" src="https://www.youtube.com/embed/ulzgXGRURbA" frameborder="0" allowfullscreen=""></iframe>
            </div>
        </div>
    </div>
@endsection
