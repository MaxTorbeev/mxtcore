import './bootstrap';
import './store';

import Vue from 'vue';
import notify from 'Core/Notification'
import VModal from 'vue-js-modal'

import ModalDialog from 'Components/MXTCore/ModalDialog'
import BackCall from 'Components/MXTCore/Feedbacks/BackCall'
import BootstrapVue from 'bootstrap-vue'


window.Vue = Vue;
window.events = new Vue();

Vue.use(VModal);
Vue.use(BootstrapVue);


new Vue({
    el: '#app',
    components: {
        ModalDialog,
        BackCall
    }
});

window.flash = function (message, level = 'success') {
    notify.create(message, level).show();
};

window.flash = function (message, level = 'success') {
    notify.create(message, level).show();
};

window.modalShow = function (component = 'back-call', attributes = null) {
    window.events.$emit('modalDialog:open', {component, attributes});
};
