import './bootstrap';
import './store';

import Vue from 'vue';
import notify from 'Core/Notification';
// import AuthLoginForm        from 'Components/MXTCore/Auth/Login';
import VModal from 'vue-js-modal'

import UploadFiles from 'Components/MXTCore/UploadFiles'
import FileThumbs from 'Components/Dashboard/FileManager/FileThumbs'
import FileUploadOnce from 'Components/Dashboard/FileManager/FileUploadOnce'
import EditorFiles from 'Components/Dashboard/FileManager/Editor/Files';
// import Reviews              from 'Components/Content/Reviews/Reviews';

import ModalDialog from 'Components/MXTCore/ModalDialog';
import Tinymce from 'Components/MXTCore/TinyMCE'
import Delete from 'Components/MXTCore/Delete'

import AddToCart from 'Components/Trade/Goods/Components/AddToCart.vue'
/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

window.Vue = Vue;
window.events = new Vue();

window.flash = function (message, level = 'success') {
    notify.create(message, level).show();
};

window.flash = function (message, level = 'success') {
    notify.create(message, level).show();
};

window.modalShow = function (component = 'back-call', attributes = null) {
    window.events.$emit('modalDialog:open', {component, attributes});
};

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */


Vue.component('back-call', require('Components/MXTCore/Feedbacks/BackCall'));

Vue.use(VModal, {dynamic: true});

new Vue({
    el: '#app',
    components: {
        AddToCart,
        Delete,
        FileThumbs,
        FileUploadOnce,
        EditorFiles,
        Tinymce,
        ModalDialog,
        UploadFiles
    }
});