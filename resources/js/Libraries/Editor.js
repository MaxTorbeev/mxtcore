import AbstractNativeComponent from 'Core/NativeComponent';
import Asset from 'Core/Asset';

export default class Editor extends AbstractNativeComponent {

    init() {
        if (window.tinymce)
            this.getEditor();
        else
            Asset.scriptUrl('/editor/tinymce/tinymce.min.js').then(() => this.getEditor());
    }

    getEditor() {
        tinymce.init({
            baseURL: "/editor/tinymce/",
            document_base_url: '/',
            selector: this.selector,
            language: 'ru',
            height: 900,
            content_css: `${window.location.origin}/css/reboot.css, ${window.location.origin}/css/app.css`,
            image_title: true,
            automatic_uploads: true,
            file_picker_types: 'image',

            images_upload_handler: function (blobInfo, success, failure) {
                let formData = new FormData();
                formData.append('files', blobInfo.blob());
                axios
                    .post('/admin/editor/upload', formData, {headers: {'Content-Type': 'multipart/form-data'}})
                    .then((response) => success(response.data.path))
                    .catch(() => flash('FAILURE!!'));
            },

            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen codesample',
                'insertdatetime media table contextmenu paste code link'
            ],

            toolbar: 'codesample | undo redo | link image | code | link | insertfile undo redo | styleselect | bold italic ' +
            '| alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',

            codesample_languages: [
                {text: 'HTML/XML', value: 'markup'},
                {text: 'JavaScript', value: 'javascript'},
                {text: 'CSS', value: 'css'},
                {text: 'PHP', value: 'php'}
            ],

            image_class_list: [
                {title: 'None', value: ''},
                {title: 'Img fluid', value: 'img-fluid'},
                {title: 'Img thumbnail', value: 'img-thumbnail'},
                {title: 'Rounded' , value: 'rounded'},
            ],

            relative_urls: false,
            remove_script_host: true
        });
    }
}