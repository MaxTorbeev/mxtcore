import AbstractNativeComponent from 'Core/NativeComponent'
import '@fancyapps/fancybox/dist/jquery.fancybox.min.css'

class Fancybox extends AbstractNativeComponent {
    init() {
        require('@fancyapps/fancybox');

        $.fancybox.defaults.hash = false;
        $.fancybox.defaults.hideScrollbar = false;

        $().fancybox({
            padding: 0,
            selector: this.selector,
            loop: true,
            fitToView: false,
        });
    }
}

export default Fancybox;