import Asset from 'Core/Asset'
import AbstractNativeComponent from 'Core/NativeComponent'

export default class YandexMap extends AbstractNativeComponent {

    beforeInit() {
        this.elements.forEach((element) => {
            element.style.height = element.dataset.mapHeight;
            element.style.overflow = 'hidden';
        })
    }

    init() {
        if(window.ymaps === undefined){
            return ;
        }

        ymaps.ready(() => this.initMap());
    }

    initMap() {
        this.elements.forEach((element) => {
                let coordinates = element.dataset.mapCoordinate.split(',');
                let myMap = new ymaps.Map(element, {
                    center: coordinates,
                    zoom: element.dataset.zoom
                });
                myMap.balloon.open(coordinates, element.dataset.mapText, {
                    closeButton: true
                });
            }
        )
    }
}