import AbstractNativeComponent from 'Core/NativeComponent'
// import _ from 'lodash'

class MMenu extends AbstractNativeComponent {
    init() {
        $(this.selector).mmenu({
            'extensions': [
                'theme-dark'
            ],
            'iconbar': {
                'add': true,
                'top': [
                    '<a href="#/"><i class="fa fa-home"></i></a>',
                    '<a href="#/"><i class="fa fa-user"></i></a>'
                ],
                'bottom': [
                    '<a href="#/"><i class="fa fa-twitter"></i></a>',
                    '<a href="#/"><i class="fa fa-facebook"></i></a>',
                    '<a href="#/"><i class="fa fa-linkedin"></i></a>'
                ]
            },
            'iconPanels': true
        });
    }
}

export default MMenu;