import AbstractNativeComponent from 'Core/NativeComponent'
import Swiper from 'swiper'
import 'swiper/dist/css/swiper.min.css'

export default class Carousel extends AbstractNativeComponent {
    init(){
        this.elements.forEach((element) => {
            new Swiper(element, {
                direction: element.dataset.direction == null ? 'horizontal' : element.dataset.direction,
                freeMode: element.dataset.freeMode == null ? false : element.dataset.freeMode,
                slidesPerView: element.dataset.slidesPerView == null ? 1 : element.dataset.slidesPerView,
                spaceBetween: element.dataset.spaceBetween == null ? 0 : parseInt(element.dataset.spaceBetween),
                loop: element.dataset.loop == null ? 0 : Boolean(element.dataset.loop),
                mousewheel: element.dataset.mousewheel == null ? 0 : Boolean(element.dataset.mousewheel),

                navigation: {
                    nextEl: element.dataset.navNextEl == null ? '.swiper-navigation .next' : element.dataset.navNextEl,
                    prevEl: element.dataset.navPrevEl == null ? '.swiper-navigation .prev' : element.dataset.navPrevEl
                },

                breakpoints: {
                    480: {
                        slidesPerView: element.dataset.slidesPerViewXs == null ? 2 : element.dataset.slidesPerViewXs,
                        spaceBetween: 20
                    }
                },
                on: {
                    init: () => {
                        element.classList.add('hasBeenLoad')
                    },
                }
            });
        });
    }
}
