import AbstractNativeComponent from 'Core/NativeComponent'

class Tabs extends AbstractNativeComponent {
    init() {
        $(this.selector).tab('show')
    }
}

export default Tabs;

