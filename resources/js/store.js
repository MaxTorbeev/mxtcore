import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

window.mxtcore.store = new Vuex.Store({
    state: {
        addToCartUrl: '/api/trade/cart/add'
    },
});