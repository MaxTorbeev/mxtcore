window._ = require('lodash');
window.Popper = require('popper.js').default;

import Select           from './Libraries/Select2';
import Datepicker       from './Libraries/Datepicker'
import AsyncLoadImage   from './Libraries/AsyncLoadImage'
import Carousel         from './Libraries/Carousel'
import Tooltip          from './Libraries/Tooltip'
import Tabs             from './Libraries/Tabs'
import Fancybox         from './Libraries/Fancybox'
import FontAwesome      from './Libraries/FontAwesome'
import CarouselGallery  from './Libraries/CarouselGallery'
import YandexMap        from './Libraries/YandexMap'

try {
    window.$ = window.jQuery = require('jquery');
    require('bootstrap');

    document.addEventListener("DOMContentLoaded", () => {
        document.getElementById('app').classList.add('hasBeenLoad');

        new Tabs('[data-native-component="tabs"]');
        new Tooltip('[data-toggle="tooltip"]');
        new Datepicker('[data-datetimepicker]');
        new Fancybox('[data-fancybox]');
        new Select('.select2');
        new Carousel('[data-slides]');
        new AsyncLoadImage('img.lazy');
        new FontAwesome('body');
        new CarouselGallery('[data-carousel-gallery]');
        new YandexMap('.yandex-map');
    });

    require('perfect-scrollbar/dist/perfect-scrollbar.min.js');
    require('@coreui/coreui/dist/js/coreui.min.js');

} catch (e) {
    console.error(e);
}

window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'xsrfHeaderName': mxtcore.csrfToken
};