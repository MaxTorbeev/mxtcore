import Vue from 'vue';
import Vuex from 'vuex';
import Asset from 'Core/Asset'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        buildFormUrl: '/api/realty/filters/build',
        buildFormData: '/api/realty/filters/build',
        categories: null,
        count: 0
    },
    mutations: {
        increment(state) {
            state.count++
        },

        categories(state, categories) {
            state.categories = categories;
        },

        buildFormData(state, data) {
            state.buildFormData = data;
        }
    }
});