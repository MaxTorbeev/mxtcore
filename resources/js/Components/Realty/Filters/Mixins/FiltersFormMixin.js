import Asset from 'Core/Asset'
import Form from 'Core/Form'

export default {
    methods: {
        onSubmit() {
            if (!this.$store.state.buildFormUrl) {
                flash('Необходимо указать sendUrl', 'error');
                return;
            }

            this.form.submit('post', this.$store.state.buildFormUrl)
                .then((response) => {
                    // this.form = new Form(response.data);
                })
                .catch(errors => {
                    flash(errors.message, 'error')
                })
        },
    },
    computed: {
        minCost: function () {
            return this.$store.state.buildFormData.ranges.min_price;
        },
        maxCost: function () {
            return this.$store.state.buildFormData.ranges.max_price;
        }
    }
}