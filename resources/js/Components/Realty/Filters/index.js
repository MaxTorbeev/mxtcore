import Vue from 'vue'
import FiltersForm from './FiltersForm'
import FiltersRouter from './Routes'
import './../../../bootstrap';

import { store } from './Store';

new Vue({
    el: '#realty-filters',
    store: store,
    render: h => h(FiltersForm),
    router: FiltersRouter
});
