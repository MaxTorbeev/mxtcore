import Asset from 'Core/Asset'
import Form from 'Core/Form'

export default {
    props: {
        sendMethod: {
            default: 'post',
            type: String
        },
        actionUrl: {
            default: null,
            type: String
        },
        deleteUrl: {
            default: '',
            type: String
        }
    },
    data() {
        return {
            form: new Form({}),

            item: {
                default: null,
                type: Object
            },

            hasLoaded: {
                default: false,
                type: Boolean
            },

            success: false,
            fail: false,

            params: {
                published: {
                    0: 'Нет',
                    1: 'Да'
                }
            }
        }
    },

    computed: {
        formName: function () {
            return this.$parent.formName ? this.$parent.formName : '';
        }
    },

    methods: {
        onSubmit() {
            if (!this.actionUrl) {
                flash('Необходимо указать sendUrl', 'error');
                return;
            }

            console.log();

            this.form.submit(this.sendMethod.toLowerCase(), this.actionUrl)
                .then((response) => {
                    this.form = new Form(response.data);
                    this.success = true;
                    events.$emit(this.formName + '-form:success', response);
                })
                .catch(errors => {
                    flash(errors.message, 'error');
                    this.fail = true;
                    events.$emit(this.formName + '-form:error', errors);
                })

        },
    },
}