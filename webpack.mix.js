const mix = require('laravel-mix');
const path = require('path');
const copyWebpackPlugin = require('copy-webpack-plugin');
const glob = require('glob-all');
require('laravel-mix-purgecss');

mix
    .js('resources/js/app.js', 'public/js')
    .js('resources/js/app.dashboard.js', 'public/js')
    .sass('resources/sass/app.dashboard.scss', 'public/css')
    // .sass('resources/sass/app.editor.scss', 'public/css')
    .sass('resources/sass/reboot.scss', 'public/css')
    .sass('resources/sass/app.scss', 'public/css')
    // .purgeCss({
    //     enabled: mix.inProduction() ? true : false,
    //     paths: () => glob.sync([
    //         path.join(__dirname, 'resources/**/*.blade.php'),
    //         path.join(__dirname, 'packages/maxtor/**/*.blade.php'),
    //     ]),
    //     extensions: ['html', 'js', 'php', 'vue'],
    //     whitelistPatterns: [/language/, /hljs/],
    //     whitelistPatternsChildren: [/^markdown$/],
    // })
    // .browserSync('realty.local')
    .webpackConfig({
        resolve: {
            alias: {
                Components: path.resolve(__dirname, 'resources/js/Components'),
                Core: path.resolve(__dirname, 'resources/js/Core'),
                Libraries: path.resolve(__dirname, 'resources/js/Libraries')
            }
        },
        plugins: [
            // Если потребуется редактор tinymce
            new copyWebpackPlugin([
                {from: './node_modules/tinymce/plugins', to: './editor/tinymce/plugins'},
                {from: './node_modules/tinymce/themes', to: './editor/tinymce/themes'},
                {from: './node_modules/tinymce/skins', to: './editor/tinymce/skins'},
                {from: './node_modules/tinymce/tinymce.min.js', to: './editor/tinymce/tinymce.min.js'},
            ]),
        ]
    });
