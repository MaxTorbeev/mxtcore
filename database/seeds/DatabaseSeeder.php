<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(MXTCoreDatabaseSeeder::class);
         $this->call(ContentDatabaseSeeder::class);
         $this->call(LocationsDatabaseSeeder::class);
         $this->call(RealtyDatabaseSeeder::class);
         $this->call(TradeDatabaseSeeder::class);

         $this->changeAllPermissionToRole();
    }

    /**
     * Change all permissions to change role.
     *
     * @param string $role
     * @return Void
     */
    protected function changeAllPermissionToRole($role = 'root'):Void
    {
        $user = \App\User::where('id', 1)->first();
        $user->assignRole($role);

        $changedRole = \MaxTor\MXTCore\Models\Role::where('name', $role)->first();

        foreach(\MaxTor\MXTCore\Models\Permission::all() as $permission){
            $changedRole->givePermissionTo($permission);
        }
    }
}
