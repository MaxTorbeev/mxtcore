<?php

namespace MaxTor\MXTCore\Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateSectionTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Гость и обычный пользователь без прав не может создать секцию сайта.
     *
     * @test
     */
    public function guest_may_not_created_section()
    {
        $this->withOutExceptionHandling();

        $this->get('admin/sections/create')->assertRedirect('/login');
        $this->post('admin/sections')->assertRedirect('/login');
    }

    /**
     * Пользователь может посмотреть список секций.
     *
     * @test
     */
    function auth_user_can_see_sections_list()
    {
        $this->withOutExceptionHandling();

        $this->signIn(create(User::class), 'root', ['access_dashboard']);

        $this->get('/admin/sections')->assertStatus(200);
    }
}
