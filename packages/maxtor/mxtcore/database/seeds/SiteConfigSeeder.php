<?php

use Illuminate\Database\Seeder;

class SiteConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('site_configs')->insert([
            [
                'name' => 'site_name',
                'label' => 'Название сайта',
                'value' => 'Ваш надежный риэлтор в Казани',
            ],
            [
                'name' => 'metakey',
                'label' => 'Ключевые слова',
                'value' => 'Ваш надежный риелтор, Квартиры Казань, новостройки казани цены',
            ],
            [
                'name' => 'metadesc',
                'label' => 'Мета-описание',
                'value' => 'Риелторская компания «Ваш надежный риелтор» оказывает полный спектр услуг по покупке, аренде и продаже недвижимости в Казани',
            ],
            [
                'name' => 'site_header_phone',
                'label' => 'Номер телефона в шапке сайта',
                'value' => '+78432333717'
            ]
        ]);
    }
}
