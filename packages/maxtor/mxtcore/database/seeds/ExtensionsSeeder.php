<?php

use Illuminate\Database\Seeder;

class ExtensionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('extensions')->insert([
                [
                    'name' => 'Статьи',
                    'controller' => 'MaxTor\Content\Controllers\Frontend\PostsController',
                    'created_user_id' => 1,
                ],
                [
                    'name' => 'Портфолио локации',
                    'controller' => 'MaxTor\Portfolio\Controllers\Frontend\LocationsController',
                    'created_user_id' => 1,
                ],
                [
                    'name' => 'Портфолио объекты',
                    'controller' => 'MaxTor\Portfolio\Controllers\Frontend\ObjectsController',
                    'created_user_id' => 1,
                ]
            ]
        );
    }
}
