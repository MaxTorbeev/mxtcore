<?php

use Illuminate\Database\Seeder;
use \MaxTor\MXTCore\Models\Menu;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu_types')->insert([
                [
                    'slug' => 'no-type',
                    'name' => 'Без типа меню',
                    'description' => 'Тим меню не выбран',
                    'created_user_id' => 1,
                ],
                [
                    'slug' => 'dashboard',
                    'name' => 'Меню панели управления',
                    'description' => 'Главное меню в панели управления',
                    'created_user_id' => 1
                ],
                [
                    'slug' => 'mainmenu',
                    'name' => 'Главное меню',
                    'description' => 'Главное меню сайт',
                    'created_user_id' => 1
                ]
            ]
        );

        DB::table('menu')->insert([
                [
                    'menu_type_id' => 2,
                    'name' => 'Менеджер меню',
                    'route_name' => 'admin.menu.index',
                    'parent_id' => null,
                    'created_user_id' => 1
                ],
                [
                    'menu_type_id' => 2,
                    'name' => 'Пункты меню',
                    'route_name' => 'admin.menu.index',
                    'parent_id' => 1,
                    'created_user_id' => 1
                ],
                [
                    'menu_type_id' => 2,
                    'name' => 'Типы меню',
                    'route_name' => 'admin.menu-types.index',
                    'parent_id' => 1,
                    'created_user_id' => 1
                ],
                [
                    'menu_type_id' => 2,
                    'name' => 'Менеджер контента',
                    'route_name' => 'admin.content.index',
                    'parent_id' => null,
                    'created_user_id' => 1
                ],
                [
                    'menu_type_id' => 2,
                    'name' => 'Посты',
                    'route_name' => 'admin.content.posts.index',
                    'parent_id' => 4,
                    'created_user_id' => 1
                ],
                [
                    'menu_type_id' => 2,
                    'name' => 'Категории',
                    'route_name' => 'admin.content.categories.index',
                    'parent_id' => 4,
                    'created_user_id' => 1
                ],
                [
                    'menu_type_id' => 2,
                    'name' => 'Теги',
                    'route_name' => 'admin.content.tags.index',
                    'parent_id' => 4,
                    'created_user_id' => 1
                ],
                [
                    'menu_type_id' => 2,
                    'name' => 'Слайдеры',
                    'route_name' => 'admin.content.carousels.index',
                    'parent_id' => 4,
                    'created_user_id' => 1
                ],
                [
                    'menu_type_id' => 2,
                    'name' => 'Менеджер расширений',
                    'route_name' => 'admin.extensions.index',
                    'parent_id' => null,
                    'created_user_id' => 1
                ],
                [
                    'menu_type_id' => 2,
                    'name' => 'Секции сайта',
                    'route_name' => 'admin.sections.index',
                    'parent_id' => null,
                    'created_user_id' => 1
                ],
                [
                    'menu_type_id' => 2,
                    'name' => 'Отзывы',
                    'route_name' => 'admin.content.reviews.index',
                    'parent_id' => null,
                    'created_user_id' => 1
                ],
            ]
        );
    }
}
