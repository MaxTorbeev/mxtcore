<?php

namespace MaxTor\MXTCore\Services\Import;

use GuzzleHttp\Client;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;

abstract class AbstractXMLImport
{
    protected $url;
    protected $feeds;

    public function setFeeds()
    {
        try {
            $this->str = $this->sendRequest();
        } catch (\Exception $exception) {
            /** @todo Написать экзепшен */
        }

        if ($str = $this->sendRequest()) {
            if ($this->isXml($str)) {
                $xml = new \SimpleXMLElement((string)$str);

                $this->feeds = $xml;
            }
        }

        return $this;
    }

    public function sendRequest()
    {
        $client = new Client();

        $response = $client->get($this->getUrl());

        return $response->getStatusCode() == 200
            ? $response->getBody()
            : null;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url): void
    {
        $this->url = $url;
    }

    public function getFeeds()
    {
        return $this->feeds;
    }

    public function isXml($xmlstr)
    {
        $xml = explode("\n", $xmlstr);

        return stristr($xml[0], '<?xml version="1.0"');
    }

    public function saveTempFile($filePath): UploadedFile
    {
        $info = pathinfo($filePath);

        $file = new Filesystem();

        $tempDir = public_path('/upload/.tmp/');

        if(!$file->exists($tempDir)){
            $file->makeDirectory($tempDir, 0755, true );
        }

        $contents = file_get_contents($filePath);

        $file = $tempDir . $info['basename'];

        file_put_contents($file, $contents);

        return new UploadedFile($file, $info['basename']);
    }
}