<?php

namespace MaxTor\MXTCore;

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\ServiceProvider;
use MaxTor\MXTCore\Interfaces\PackageHelperInterface;

abstract class AbstractPackageHelper
{
    public static function getPackageName()
    {
        return null;
    }

    /**
     * Breadcrumbs
     * @docs https://github.com/davejamesmiller/laravel-breadcrumbs/tree/master
     */
    public static function getAdminBreadcrumbs($bundle)
    {
        Breadcrumbs::for('admin.' . self::getPackageName(), function ($trail) {
            $trail->parent('admin');
            $trail->push('Магазин', route('admin.' . self::getPackageName() . '.index'));
        });

        Breadcrumbs::for('admin.' .  self::getPackageName() . '.' . $bundle . '.index', function ($trail) use ($bundle) {
            $trail->parent('admin.' . self::getPackageName());
            $trail->push($bundle, route('admin.' . self::getPackageName() . '.' . $bundle .'.index'));
        });

        Breadcrumbs::for('admin.' .  self::getPackageName() . '.' . $bundle . '.create', function ($trail) use ($bundle) {
            $trail->parent('admin.' . self::getPackageName() . '.' . $bundle . '.index');
            $trail->push('new ' . $bundle, route('admin.content.posts.create'));
        });

        Breadcrumbs::for('admin.' .  self::getPackageName() . '.' . $bundle . '.edit', function ($trail, $bundle, $model) {
            $trail->parent('admin.' . self::getPackageName() . '.' . $bundle . '.index' );
            $trail->push($model->name, route('admin.' . self::getPackageName() . '.' . $bundle . '.edit', ['id' => $model->id]));
        });
    }
}