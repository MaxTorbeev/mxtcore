<?php

namespace MaxTor\MXTCore\Filters;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

abstract class Filters
{
    protected $filters = [];
    protected $request, $builder;

    /**
     * ThreadFilters constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * We apply our filters to the builder.
     *
     * @param $builder
     * @return mixed
     */
    public function apply($builder)
    {
        $this->builder = $builder;

        foreach ($this->getFilters() as $filter => $value) {
            if (method_exists($this, $filter)) {
                $this->$filter($value);
            } else {
                // Если параметр, который указывается в фильтре не найден, то возращает пустой массив
                return $this->builder->whereNull('id');
            }
//            if (Schema::hasColumn($this->builder->getModel()->getTable(), $filter)) {
//                if (method_exists($this, $filter)) {
//                    $this->$filter($value);
//                }
//            } else {
//                // Если параметр, который указывается в фильтре не найден, то возращает пустой массив
//                return $this->builder->whereNull('id');
//            }
        }

        return $this->builder;
    }

    public function getFilters()
    {
        return $this->request->only($this->filters);
    }

    public function getFilterValues($params = null)
    {
        $pluck = $params['puck'] ?? ['id', 'name'];

        return $this->builder->getModel()->pluck($pluck);
    }
}