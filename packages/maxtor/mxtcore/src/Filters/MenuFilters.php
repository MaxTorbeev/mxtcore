<?php

namespace MaxTor\MXTCore\Filters;

use App\User;
use Illuminate\Http\Request;
use MaxTor\MXTCore\Filters\Filters;

class MenuFilters extends Filters
{
    protected $filters = ['by', 'parent', 'type'];

    /**
     * Filter the query by a give username.
     *
     * @param $username
     * @return mixed
     */
    protected function by($username)
    {
        $user = User::where('name', $username)->firstOrFail();

        return $this->builder->where('created_user_id', $user->id);
    }

    /**
     * Filter the query according to most popular threads.
     *
     * @return mixed
     */
    protected function parent($id)
    {
        // Сбрасываем всю сортировку
        $this->builder->getQuery()->orders = [];

        return $this->builder->where('parent_id', $id);
    }

    protected function type($type_id)
    {
        // Сбрасываем всю сортировку
        $this->builder->getQuery()->orders = [];

        return $this->builder->where('menu_type_id', $type_id);
    }
}