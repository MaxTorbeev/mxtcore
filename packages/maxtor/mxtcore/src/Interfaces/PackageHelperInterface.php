<?php

namespace MaxTor\MXTCore\Interfaces;

interface PackageHelperInterface
{
    /**
     * Getting package name.
     *
     * @return mixed
     */
    public static function getPackageName();


}

