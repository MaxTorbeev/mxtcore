<?php

use Illuminate\Support\Facades\File;
use Jenssegers\Agent\Agent;

define('MXTCORE_PATH', __DIR__);

if (!function_exists('includePublicTextFile')) {
    function includePublicTextFile($filePath)
    {
        if (file_exists(public_path() . $filePath)) {
            return file_get_contents(public_path() . $filePath);
        }

        return 'file ' . public_path() . $filePath . ' not found';
    }
}


if (!function_exists('camalToCebab')) {
    /**
     * camalCase case to cebab-case string transformer
     *
     * @param $string
     * @param string $delimiter
     * @return string
     */
    function camalToCebab($string, $delimiter = '-'): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', $delimiter . '$0',
            $string
        ));
    }
}

if (!function_exists('mxtcore_path')) {
    /**
     * Get the path to the mxtcore package of the install.
     *
     * @param  string $path
     * @return string
     */
    function mxtcore_path($path = '')
    {
        return MXTCORE_PATH . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }
}

if (!function_exists('mxtcore_resources_path')) {
    /**
     * Get the path to the mxtcore resources package of the install.
     *
     * @param  string $path
     * @return string
     */
    function mxtcore_resources_path($path = '')
    {
        return MXTCORE_PATH . '/../resources/' . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }
}

if (!function_exists('mxtcore_config_path')) {
    /**
     * Get the path to the mxtcore configs package of the install.
     *
     * @param  string $path
     * @return string
     */
    function mxtcore_config_path($path = '')
    {
        return MXTCORE_PATH . '/../config/' . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }
}

if (!function_exists('site_config')) {
    function site_config($configName, $default = null)
    {
        try {
            $config = \Cache::rememberForever('siteConfig:' . $configName, function () use ($configName) {
                return \MaxTor\MXTCore\Models\SiteConfig::where('name', $configName)->first();
            });
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        if ($config === null) {
            $config = new stdClass();

            if ($default) {
                $config->value = $default;
            } else {
                $config->value = __('mxtcore.site_configs.errors.configNotFound', compact('configName'));
            }

            return $config;
        }

        return $config;
    }
}

if (!function_exists('capital_letter')) {
    function capital_letter($word)
    {
        return mb_convert_case($word, MB_CASE_TITLE, 'UTF-8');
    }
}

if (!function_exists('upload_file')) {

    /**
     * Upload file function.
     *
     * @param $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    function upload_file($request)
    {
        $file = $request->file('files');

        $fileName = md5_file($file) . '.' . $file->getClientOriginalExtension();
        $fileFolder = '/upload/editor/' . substr($fileName, 0, 2);
        $file->move(public_path($fileFolder), $fileName);
        $file_path = $fileFolder . '/' . $fileName;

        $imageMimeTypes = ['image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/svg+xml'];

        $mimeType = File::mimeType(public_path($file_path));

        if ($mimeType == 'image/jpeg' || $mimeType == 'image/png') {
            $thumbsFolder = "{$fileFolder}/thumbs/";
            if (!is_dir(public_path($thumbsFolder))) {
                File::makeDirectory(public_path($thumbsFolder));
            }
            $thumbPath = "{$thumbsFolder}lg_{$fileName}";

            \Image::make(public_path($file_path))
                ->fit(1200, null, function ($constraint) {
                    $constraint->upsize();
                })
                ->encode('jpg', 75)
                ->save(public_path($thumbPath));
        }

        $fileData['path'] = $file_path;
        $fileData['type'] = $mimeType;

        if (isset($thumbPath)) {
            $fileData['thumb'] = $thumbPath;
        }

        return response($fileData);
    }
}
if (!function_exists('parse_phone')) {

    /**
     * Функция для парсинга номеров телефона.
     *
     * @param $phone
     * @param bool $canSee
     * @param array $params - 'plain' - returned plain phone number | 'asArray' - returning array scheme
     * @return array|string
     */
    function parse_phone($phone, $canSee = true, $params = [])
    {
        if ($phone !== null) {
            if (substr($phone, 0, 2) === '+7')
                $phone = substr($phone, 2);

            else if (substr($phone, 0, 1) === '8')
                $phone = substr($phone, 1);

            $phone = str_replace([' ', '(', ')', '-'], ['', '', ''], $phone);

            $sArea = substr($phone, 0, 3);
            $sPrefix = substr($phone, 3, 3);
            $sNumber = substr($phone, 6, 4);

            if (!$canSee) {
                $sPrefix = '***';
                $sNumber = '**' . substr($phone, 8, 2);
            }

            if (isset($params['plain']) && $params['plain']) {
                return '+7' . $phone;
            }

            if (isset($params['asArray']) && $params['asArray']) {
                return [
                    'code' => '+7',
                    'area' => $sArea,
                    'prefix' => $sPrefix,
                    'number' => $sNumber
                ];
            }

            return $phone = '+7(' . $sArea . ')' . $sPrefix . '-' . $sNumber;
        }

        return null;
    }
}

if (!function_exists('route_params_merge')) {
    function route_params_merge($name, array $params = [])
    {
        return route($name, array_merge(request()->all(), $params));
    }
}

if (!function_exists('agent')) {
    function agent()
    {
        return new Agent();
    }
}




