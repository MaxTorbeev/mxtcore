<?php

Route::group([
    'prefix' => '/admin',
    'namespace' => 'MaxTor\MXTCore\Controllers',
    'middleware' => 'web'
], function() {

    Route::get('/', [
        'as'    => 'dashboard',
        'uses'  => 'DashboardController@startpage'
    ]);

    Route::match(['get', 'post'], '/editor/upload', [
        'as'    => 'admin.editor.upload',
        'uses'  => 'DashboardController@upload'
    ]);

    Route::resource('/extensions', 'ExtensionsController', ['as' => 'admin']);
    Route::resource('/menu', 'MenuController', ['as' => 'admin']);
    Route::resource('/menu-types', 'MenuTypesController', ['as' => 'admin']);
    Route::resource('/sections', 'SectionsController', ['as' => 'admin']);
    Route::resource('/users', 'UsersController', ['as' => 'admin']);
});
Route::group([
    'prefix' => '/',
    'namespace' => 'MaxTor\MXTCore\Controllers',
    'middleware' => 'web'
], function() {
    Route::get('/api/layouts/{type}/{name}', [
        'as' => 'layouts',
        'uses' => 'ApiController@layouts'
    ]);
});


/**
 * Breadcrumbs
 * @docs https://github.com/davejamesmiller/laravel-breadcrumbs/tree/master
 */
// Admin

Breadcrumbs::for('admin', function ($trail) {
    $trail->push('Панель управления', route('dashboard'));
});

// Admin > Extensions
Breadcrumbs::for('admin.extensions', function ($trail) {
    $trail->parent('admin');
    $trail->push('Расширения', route('admin.extensions.index'));
});

// Admin > Extensions > Create
Breadcrumbs::for('admin.extensions.create', function ($trail) {
    $trail->parent('admin.extensions');
    $trail->push('Новое расширение', route('admin.extensions.create'));
});

// Admin > Extensions > Edit
Breadcrumbs::for('admin.extensions.edit', function ($trail, $extension) {
    $trail->parent('admin.extensions');
    $trail->push('Редактировать расширение', route('admin.extensions.edit', ['id' => $extension->id]));
});

// Admin > Menu
Breadcrumbs::for('admin.menu', function ($trail) {
    $trail->parent('admin');
    $trail->push('Пункты меню', route('admin.menu.index'));
});

// Admin > Menu > Edit
Breadcrumbs::for('admin.menu.edit', function ($trail, $menu) {
    $trail->parent('admin.menu');
    $trail->push($menu->name, route('admin.menu.index', ['id' => $menu->id]));
});

// Admin > Menu types
Breadcrumbs::for('admin.menu-types', function ($trail) {
    $trail->parent('admin');
    $trail->push('Типы меню', route('admin.menu.index'));
});

// Admin > Menu types > Edit
Breadcrumbs::for('admin.menu-types.edit', function ($trail, $menuType) {
    $trail->parent('admin.menu-types');
    $trail->push($menuType->name, route('admin.menu-types.edit', ['id' => $menuType->id]));
});

// Admin > Sections
Breadcrumbs::for('admin.sections', function ($trail) {
    $trail->parent('admin');
    $trail->push(__('mxtcore.sections_package_name'), route('admin.sections.index'));
});

// Admin > Sections > Create
Breadcrumbs::for('admin.sections.create', function ($trail) {
    $trail->parent('admin.sections');
    $trail->push('Создание секции', route('admin.sections.create'));
});

// Admin > Sections > Edit
Breadcrumbs::for('admin.sections.edit', function ($trail, $section) {
    $trail->parent('admin.sections');
    $trail->push('Редактирование секции', route('admin.sections.create', $section));
});

// Admin > Users
Breadcrumbs::for('admin.users', function ($trail) {
    $trail->parent('admin');
    $trail->push('Пользователи', route('admin.users.index'));
});

// Admin > Users > Create
Breadcrumbs::for('admin.users.create', function ($trail) {
    $trail->parent('admin.users');
    $trail->push('Новый пользователь', route('admin.users.create'));
});

// Admin > Users > Edit
Breadcrumbs::for('admin.users.edit', function ($trail, $user) {
    $trail->parent('admin.users');
    $trail->push('Редактирование пользователя', route('admin.users.edit', ['id' => $user->id]));
});
