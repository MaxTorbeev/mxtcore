<?php

namespace MaxTor\MXTCore;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use MaxTor\MXTCore\Blueprints\UserManagement;
use MaxTor\MXTCore\Models\MenuType;
use Illuminate\Support\Facades\Blade;
use MaxTor\MXTCore\Models\Role;
use MaxTor\MXTCore\Repositories\DirectivesRepository;
use MaxTor\MXTCore\Widgets\Widget;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;

class MXTCoreServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //Helpers
        include_once __DIR__ . '/helpers.php';

        $this->loadViewsFrom(mxtcore_resources_path('/views/'), 'mxtcore');
        $this->loadRoutesFrom(mxtcore_path('/routes.php'));

        $this->publishes([
            mxtcore_config_path('/config/mxtcore.php') => config_path('mxtcore.php'),
        ]);

        //Migration
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        //Factories
        $this->app->make(EloquentFactory::class)->load(__DIR__ . '/../database/factories/');

        $this->registerDirectives();
        $this->composeDashboardNavigation();
        $this->fixCloudHostingSupport();

        $this->composeHeader();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MXTCorePackageHelper::class, function ($app) {
            return new MXTCorePackageHelper();
        });

        Blueprint::macro('userManagement', function () {
            UserManagement::columns($this);
        });
    }

    /**
     * Compose the navigation bar.
     *
     * @description Метод передает массив в partials.navigator при загрузке страницы.
     */
    public function composeDashboardNavigation()
    {
        view()->composer('mxtcore::dashboard.partials.sidebar', function ($view) {

            $dashboard = MenuType::whereSlug('dashboard')->firstOrFail();

            $view->with('dashboardMenu', $dashboard->menu()->where('parent_id', null)->where('enabled', 1)->get());
        });
    }

    protected function registerDirectives()
    {
        Blade::directive('widget', function ($expression) {
            $segments = explode(',', preg_replace("/[\(\)\\\]/", '', $expression));

            if (!array_key_exists(1, $segments)) {
                return '<?php echo (new MaxTor\MXTCore\Widgets\Widget)->get(' . $segments[0] . '); ?>';
            }

            return '<?php echo (new MaxTor\MXTCore\Widgets\Widget)->get(' . $segments[0] . ',' . $segments[1] . '); ?>';
        });

        Blade::directive('widget2', function($expression){

            if (Str::contains($expression, ',')) {
                $expression = DirectivesRepository::parseMultipleArgs($expression);

                dd($expression);
            }

//            $expressionValues = preg_split('/\s+/', $array['expression']);
//
//            dd($expressionValues);
//
//            foreach ($expressionValues as $value) {
/*                return '<?php echo (new MaxTor\MXTCore\Widgets\Widget)->get(' . $value . '); ?>';*/
//            }
//
/*            return "<?php echo {$array['string']}; ?>";*/
        });
    }

    /**
     * Для совместимости серверов с апачем и хостерами, которые не прописывают DocumentRoot
     *
     * @todo fix 302 status on 404.html
     */
    protected function fixCloudHostingSupport()
    {
        if (preg_match('~^' . env('APP_URL') . '/(public|resources|app|database)/~', url()->full())) {
            abort('404', 'Страница не найдена');
        }
    }

    protected function composeHeader()
    {
        view()->composer('layouts.partials.header.header', function ($view) {
            $phone = site_config('site_header_phone')->value;
            $view->with('headerPhone', parse_phone($phone, true, ['asArray' => true]));
        });
    }

}
