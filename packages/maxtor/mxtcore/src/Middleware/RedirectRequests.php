<?php

namespace MaxTor\MXTCore\Middleware;

use Closure;
use MaxTor\Content\Models\Redirect;

class RedirectRequests
{
    public function handle($request, Closure $next)
    {
//        $response = $next($request);
//
//        if($request->isMethod('get') && $response->status() === '404'){
//            if($this->redirects())
//                return \redirect($this->redirects());
//        }

        return $next($request);
    }

    protected function redirects()
    {
        $queryParams = request()->getQueryString() ? '?' . request()->getQueryString() : '';
        $redirect = Redirect::where('redirect_from', request()->path() . $queryParams)->first();

        if(isset($redirect->subject_type)){
            $redirectEntity = ($redirect->subject_type)::where('id', $redirect->subject_id)->first();

            if(isset($redirectEntity->frontend_url)){
                return $redirectEntity->frontend_url;
            }
        }

        return null;
    }
}
