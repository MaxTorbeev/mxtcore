<?php

namespace MaxTor\MXTCore\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class OptimizeMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if ($response instanceof BinaryFileResponse || !app()->environment('production')) {
            return $response;
        } else {
            $buffer = $response->getContent();

            if (strpos($buffer, '<pre>') !== false) {
                $replace = [
                    '/<!--[^\[](.*?)[^\]]-->/s'     => '',
                    "/<\?php/"                      => '<?php ',
                    "/\r/"                          => '',
                    "/>\n</"                        => '><',
                    "/>\s+\n</"                     => '><',
                    "/>\n\s+</"                     => '><',
                ];
            } else {
                $replace = [
//                    '/<!--[^\[](.*?)[^\]]-->/s'     => '',
                    "/<\?php/"                      => '<?php ',
                    "/\n([\S])/"                    => '$1',
                    "/\r/"                          => '',
                    "/\n/"                          => '',
                    "/\t/"                          => '',
                    "/ +/"                          => ' ',
                ];
            }

            $buffer = preg_replace(array_keys($replace), array_values($replace), $buffer);
            $response->setContent($buffer);

            ini_set('zlib.output_compression', 'On'); //enable GZip, too!

            return $response;
        }
    }
}
