<?php

namespace MaxTor\MXTCore\Layouts;

use App\User;
use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Carousel;
use MaxTor\MXTCore\Layouts\AbstractTable;
use MaxTor\MXTCore\Fields\TD;

class SiteConfigLayout extends AbstractTable
{
    public $data = '';

    public function fields(): array
    {
        return [
            TD::name('id')->title('Код'),
            TD::name('name')->title('Ключ'),
            TD::name('label')->title('Название'),
            TD::name('value')->title('Значение')
        ];
    }
}