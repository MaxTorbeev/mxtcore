<?php

namespace MaxTor\MXTCore\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use MaxTor\MXTCore\Models\Role;
use Illuminate\Support\Facades\Hash;
use MaxTor\MXTCore\Requests\UserRequest;
use MaxTor\Personal\Layouts\Admin\UsersListLayout;

class UsersController extends AbstractAdminController
{
    public function __construct()
    {
        $this->middleware('check.permission:access_dashboard');
    }

    public function index()
    {
        $users = User::all();

        $table = new UsersListLayout($users);

        return view('mxtcore::dashboard.users.index', compact('table'));
    }

    public function create()
    {
        $this->authorize('create_user', User::class);

        return view('mxtcore::dashboard.users.create', [
            'roles' => Role::pluck('label', 'id')
        ]);
    }

    public function store(Request $request)
    {
        $this->authorize('create_user', User::class);

        $request->validate([
            'name' => 'required|max:191',
            'email' => 'required|email|unique:users',
            'new_password' => 'required|string|min:6|confirmed',
        ]);

        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => Hash::make(request('new_password'))
        ]);

        $user->roles()->sync(request('role'));

        return redirect(
            route('admin.users.edit', $user))->with('flash', "Пользователь {$user->name} создан");
    }

    public function edit(User $user)
    {
        $this->authorize('create_user', User::class);

        $roles = Role::pluck('label', 'id');

        return view('mxtcore::dashboard.users.edit', compact('user', 'roles'));
    }

    public function update(User $user, UserRequest $request)
    {
        $this->authorize('create_user', User::class);

//        $user = User::where('id', $id)->firstOrFail();

//        $request->validate([
//            'name' => 'required|max:191',
//            'email' => 'required|email|unique:users',
//            'new_password' => 'confirmed'
//        ]);

        $user->update($request->all());

        $user->name = request('name');
        $user->email = request('email');

        if (request('new_password')) {
            $user->password = Hash::make(request('new_password'));
        }

        $user->save();

        $user->roles()->sync(request('role'));

        return redirect(route('admin.users.edit', ['id' => $user->id]))
            ->with('flash', "Пользователь {$user->name} обновлен");
    }

    public function destroy(User $user)
    {
        $this->authorize('delete_user');

        if ($user->delete()) {
            return response(['message' => 'Пользователь был удален'], 204);
        }

        return response(['message' => 'Удаление невозможно'], 403);
    }

}
