<?php

namespace MaxTor\MXTCore\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use MaxTor\MXTCore\Models\Section;
use MaxTor\MXTCore\Requests\SectionRequests;

class SectionsController extends DashboardController
{
    public function index()
    {
        $sections = Section::all();

        return view('mxtcore::dashboard.sections.index', compact('sections'));
    }

    public function create(Section $section)
    {
        return view('mxtcore::dashboard.sections.create', compact('section'));
    }

    public function edit(Section $section)
    {
        return view('mxtcore::dashboard.sections.edit', compact('section'));
    }

    public function store(SectionRequests $request)
    {
        $section = Section::create($request->all());

        if (request()->wantsJson()) {
            return response([
                'message' => 'Пункт меню успешно создан',
                'data' => $section
            ], 200);
        }

        return redirect(route('admin.sections.edit', $section))->with('flash', 'Новая секция создана');
    }

    public function update(Section $section, SectionRequests $request)
    {
        $section->update($request->all());

        if (request()->wantsJson()) {
            return response([
                'message' => 'Пункт меню успешно обновлен',
                'data' => $section
            ], 200);
        }

        return redirect(route('admin.sections.edit', $section))
            ->with('flash', 'Секция обновлена');
    }

    public function destroy(Section $section)
    {
        $section->delete();

        return 'Удалено';
    }
}
