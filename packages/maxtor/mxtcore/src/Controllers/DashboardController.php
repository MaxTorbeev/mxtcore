<?php

namespace MaxTor\MXTCore\Controllers;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use MaxTor\Content\Models\Tag;
use MaxTor\MXTCore\Models\Menu;
use Image;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('check.permission:access_dashboard');
    }

    public function startpage()
    {
        return view('mxtcore::dashboard.index');
    }

    public function upload(Request $request)
    {

        if ($request->isMethod('get')) {
            return view('mxtcore::dashboard.system.editor.image-dialog');

        } else if ($request->isMethod('post')) {

            $file = $request->file('files');

            $fileName = md5_file($file) . '.' . $file->getClientOriginalExtension();
            $fileFolder = '/upload/editor/' . substr($fileName, 0, 2);
            $file->move(public_path($fileFolder), $fileName);
            $file_path = $fileFolder . '/' . $fileName;

            $imageMimeTypes = ['image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/svg+xml'];

            $mimeType = File::mimeType(public_path($file_path));

            if ($mimeType == 'image/jpeg' || $mimeType == 'image/png') {

                $thumbsFolder = "{$fileFolder}/thumbs/";

                if (!is_dir(public_path($thumbsFolder))) {
                    File::makeDirectory(public_path($thumbsFolder));
                }

                $thumbPath = "{$thumbsFolder}lg_{$fileName}";

                Image::make(public_path($file_path))
//                    ->fit(1440, 1400, function ($constraint) {
//                        $constraint->upsize();
//                    })
                    ->encode('jpg', 75)
                    ->save(public_path($thumbPath));
            }


            $fileData['path'] = $file_path;
            $fileData['type'] = $mimeType;

            if (isset($thumbPath)) {
                $fileData['thumb'] = $thumbPath;
            }

            return response($fileData);
        }
    }

    public function getList($model, $pushNull = false)
    {
        $model = $model->pluck('name', 'id');

        if (!$pushNull) {
            return $model;
        }

        return $model->push('Не выбрано', '0');
    }


    protected function syncTags($model, array $tags)
    {
        $tagsInDb = [];

        foreach ($tags as $i => $tag) {

            if (is_numeric($tag)) {
                $tagsInDb[] = Tag::firstOrCreate(['id' => $tag]);
            } else {
                $tagsInDb[] = Tag::firstOrCreate(['name' => $tag]);
            }
        }

        $model->tags()->sync(collect($tagsInDb)->pluck('id'));
    }

}
