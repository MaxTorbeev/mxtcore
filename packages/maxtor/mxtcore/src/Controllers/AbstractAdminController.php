<?php

namespace MaxTor\MXTCore\Controllers;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use MaxTor\Content\Models\Tag;
use MaxTor\MXTCore\Layouts\ApiLayout;
use MaxTor\MXTCore\Models\Menu;

abstract class AbstractAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('check.permission:access_dashboard');
    }

    public function view($view = null, $data = [], $mergeData = [])
    {
        $folder = Str::snake(str_replace('Controller', '', class_basename($this)), '-');
        $view = $this->helper->getPackageName() . '::dashboard.' . $folder . '.' . $view;

        return view($view, $data, $mergeData);
    }

    public function syncTags($model, array $tags)
    {
        $tagsInDb = [];

        foreach ($tags as $i => $tag) {

            if (is_numeric($tag)) {
                $tagsInDb[] = Tag::firstOrCreate(['id' => $tag]);
            } else {
                $tagsInDb[] = Tag::firstOrCreate(['name' => $tag]);
            }
        }

        $model->tags()->sync(collect($tagsInDb)->pluck('id'));
    }
}