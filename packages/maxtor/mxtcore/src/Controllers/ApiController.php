<?php

namespace MaxTor\MXTCore\Controllers;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MaxTor\Content\Models\Tag;
use MaxTor\MXTCore\Layouts\ApiLayout;
use MaxTor\MXTCore\Models\Menu;

class ApiController extends Controller
{
    public function layouts($type, $name)
    {
        return (new ApiLayout($type, $name))->handle();
    }
}