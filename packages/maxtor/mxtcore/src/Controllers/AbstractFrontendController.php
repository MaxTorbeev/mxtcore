<?php

namespace MaxTor\MXTCore\Controllers;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use MaxTor\Content\Models\Tag;
use MaxTor\MXTCore\Layouts\ApiLayout;
use MaxTor\MXTCore\Models\Menu;

abstract class AbstractFrontendController extends Controller
{
    public function view($view = null, $data = [], $mergeData = [])
    {
        $folder = Str::snake(str_replace('Controller', '', class_basename($this)), '-');
        $view = $this->config->getPackageName() . '::frontend.' . $folder . '.' . $view;

        return view($view, $data, $mergeData);
    }
}