<?php

namespace MaxTor\MXTCore\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use MaxTor\MXTCore\Models\Section;
use MaxTor\MXTCore\Requests\SectionRequests;

class SiteConfigsController extends DashboardController
{
    public function index()
    {
        return view();
    }
}