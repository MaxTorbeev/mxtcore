<?php

namespace MaxTor\MXTCore\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use MaxTor\MXTCore\Models\Extension;
use MaxTor\MXTCore\Requests\ExtensionsRequests;

class ExtensionsController extends Controller
{
    public function index()
    {
        $extensions = Extension::all();

        return view('mxtcore::dashboard.extensions.index', compact('extensions'));
    }

    public function create()
    {
        $this->authorize('create_extension');

        $extension = new Extension();

        return view('mxtcore::dashboard.extensions.create', compact('extension'));
    }

    public function store(ExtensionsRequests $request)
    {
        $this->authorize('create_extension');

        $extension =  Auth::user()->extensions()->create($request->all());

        return redirect(route('admin.extensions.edit', ['menu' => $extension->id]))
            ->with('flash', 'Расширение создано успешно');
    }

    public function edit(Extension $extension)
    {
        $this->authorize('create_extension');

        return view('mxtcore::dashboard.extensions.edit', compact('extension'));
    }

    public function update(Extension $extension, Request $request)
    {
        $this->authorize('create_extension');

        $extension->update($request->all());

        return redirect(route('admin.extensions.edit', ['menu' => $extension->id]))
            ->with('flash', 'Расширение создано отредактировано');
    }
    
    public function destroy(Extension $extension)
    {
        $this->authorize('delete_extension');

        if($extension->delete()){
            return redirect(route('admin.extensions.index'))->with('flash', 'Расширение успешно удалено');
        }

        return redirect(route('admin.extensions.index'))->with('flash-errors', 'Расширение неможет быть удалено');
    }

}
