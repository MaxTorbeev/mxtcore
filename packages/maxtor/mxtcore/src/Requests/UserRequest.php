<?php

namespace MaxTor\MXTCore\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class UserRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
            case 'PATCH':
                return [
                    'name' => 'required|max:191',
                    'email' => 'required|email|unique:users,email,' . $this->user->id,
                ];

            case 'POST':
            default:
                return [
                    'name' => 'required|max:191',
                    'email' => 'required|email|unique:users',
                    'new_password' => 'required|string|min:6|confirmed',
                ];
        }
    }
}