<?php

namespace MaxTor\MXTCore\Widgets;

use Illuminate\Support\Str;

class Widget implements WidgetContractInterface
{
    public $packageName = 'mxtcore';

    public $cacheTime = 360;
    public $cacheKey = null;
    public $attributes = null;
    public $view = 'default';

    /**
     * @inheritdoc
     */
    public function get(string $key, $attributes = null)
    {
        $class = config('widgets.' . $key);

        if (!class_exists($class)) {
            return view('mxtcore::components.alert', [
                'type' => 'danger',
                'title' => __('mxtcore.widget.errors.title'),
                'slot' => __('mxtcore.widget.errors.classNotFound', compact('class'))
            ]);
        }

        $widget = new $class();

        try {
            return $widget->handle($attributes);
        } catch (\InvalidArgumentException $e) {
            return view('mxtcore::components.alert', [
                'type' => 'danger',
                'title' => __('mxtcore.widget.errors.title'),
                'slot' => $e->getMessage()
            ]);
        }

    }

    /**
     * @inheritdoc
     */
    public function handle()
    {
    }

    public function view($view = null, $data = [], $mergeData = [])
    {
        if (view()->exists($view))
            return view($view, $data, $mergeData)->render();

        $view = $this->packageName . '::widgets.' . $this->getWidgetViewFolder() . '.' . $view;

        return view($view, $data, $mergeData)->render();
    }

    public function getDefaultCacheKey()
    {
        return 'widgets::' . $this->packageName . '.' . $this->cacheKey . $this->getWidgetViewFolder() . '.' . $this->view;
    }

    public function getWidgetViewFolder()
    {
        return Str::snake(str_replace('Widget', '', class_basename($this)), '-');
    }
}