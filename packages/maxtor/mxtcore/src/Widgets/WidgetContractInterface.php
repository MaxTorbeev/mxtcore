<?php

namespace MaxTor\MXTCore\Widgets;

interface WidgetContractInterface
{
    /**
     * Getting widget
     *
     * @param $key
     * @return mixed
     */
    public function get(string $key);

    /**
     * Handle an incoming request.
     *
     * @param mixed
     * @return mixed
     */
    public function handle();
}