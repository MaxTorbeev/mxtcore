<?php

namespace MaxTor\MXTCore;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use MaxTor\MXTCore\Models\MenuType;
use Illuminate\Support\Facades\Blade;
use MaxTor\MXTCore\Models\Role;
use MaxTor\MXTCore\Widgets\Widget;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MXTCorePackageHelper extends AbstractPackageHelper
{

}