<?php

namespace MaxTor\MXTCore\Models;

use Illuminate\Database\Eloquent\Model;
use MaxTor\MXTCore\Traits\UserManagement;

class Extension extends Model
{
    use UserManagement;

    protected $table = 'extensions';

    protected $guarded = ['id'];

    protected static $allowedScopes = [
        'Controllers',
    ];

    public static function getDeclaredClasses()
    {
        return array_filter(get_declared_classes() , function($class) {

            $classes = [];

            if(preg_match('/MaxTor\/', $class)){
                $classes[] = $class;
            }

            return $classes;
        });
    }

    public function getTypeByMenuSlug()
    {
        return $this->belongsTo('MaxTor\MXTCore\Models\Menu', 'alias');
    }


}
