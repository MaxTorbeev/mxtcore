<?php

namespace MaxTor\MXTCore\Traits;

trait Cacheable
{
//    protected $cacheKeys = [];

    protected static function bootCacheable()
    {
        foreach (static::getSectionsToRecord() as $event) {
            static::$event(function ($model) use ($event) {
                $model->clearCache();
            });
        }
    }

    protected static function getSectionsToRecord()
    {
        return ['creating', 'updating', 'deleting'];
    }

    protected function clearCache()
    {
        if(isset($this->cacheKeys)){
            foreach ($this->cacheKeys as $key){
                \Cache::forget($key);
            }
        }
    }

    public function cacheKey()
    {
        $timestamp = $this->updated_at->timestamp ?? $this->id;

        return sprintf(
            "%s/%s-%s",
            $this->getTable(),
            $this->getKey(),
            $timestamp
        );
    }
}