<?php

namespace MaxTor\MXTCore\Traits;

use App\User;
use Illuminate\Support\Facades\App;

/**
 * Trait SetCreators.
 *
 * Заполняем поля id пользователя создателя и пользователя модификатора
 *
 * @package MaxTor\MXTCore\Traits
 */
trait UserManagement
{
    protected static function bootUserManagement()
    {
        if (!app()->environment('testing')) {
            if (isset(auth()->user()->id)) {
                static::creating(function ($model) {
                    $model->created_user_id = auth()->user()->id;
                });

                static::updating(function ($model) {
                    $model->modified_user_id = auth()->user()->id;
                });
            }
        }
    }


    /**
     * An post is owned by a user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'created_user_id');
    }

    public function scopeEnabled($query, $ability = 'create_post')
    {
        return $query->where(with(new static)->getTable() . '.enabled', true);
    }
}
