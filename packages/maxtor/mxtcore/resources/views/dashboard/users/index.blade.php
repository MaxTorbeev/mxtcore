@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.users') }} @endsection

@section('content')
    <div class="btn-group mb-4">
        <a class="btn btn-success" href="{{ route('admin.users.create') }}">Новый пользователь</a>
    </div>

    <div class="card">
        <div class="card-header">Пользователи</div>
        <div class="card-body">
            {!! $table->build() !!}
        </div>
    </div>
@endsection
