<li class="nav-item {{ $item->children ? 'nav-dropdown' : '' }}">
    @if($item->url_path)
        <a href="{{ $item->url_path }}" class="nav-link {{ $item->children->count() ? 'nav-dropdown-toggle ' : '' }}">
            {{ $item->parent ? '-' : '' }} {{$item->name}}
        </a>
    @else
        <span class="nav-link">{{ $item->name }}</span>
    @endif

    @if ($item->children)
        <ul class="nav-dropdown-items">
            @foreach($item->children as $item)
                @include('mxtcore::dashboard.partials.sidebar.menu', $item)
            @endforeach
        </ul>
    @endif
</li>
