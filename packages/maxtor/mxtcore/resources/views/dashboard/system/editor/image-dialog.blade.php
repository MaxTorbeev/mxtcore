<html>
<head>
    <meta charset="UTF-8">
    <title>Image Upload Dialog</title>
    <link href="/css/app.dashboard.css" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        window.mxtcore = <?php echo json_encode([ 'csrfToken' => csrf_token(), ]); ?>
    </script>
</head>
<body>
<div  class="container">
    <div id="app">

        @dd(route('admin.editor.upload'));

        <editor-files url="{{ route('admin.editor.upload') }}" field-name="files"></editor-files>
    </div>
    <script type="text/javascript" src="{{ asset('/js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/editor/custom/plugins/imageupload/imageUpload.js') }}"></script>
</div>
</body>
</html>