@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.extensions') }} @endsection

@section('content')

    <nav class="navbar">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="btn btn-success" href="{{ route('admin.extensions.create') }}">Добавить расширение</a>
            </li>
        </ul>
    </nav>

    <div class="card">
        <div class="card-header">
            Расширения
        </div>
        <div class="card-body">
            <table class="table table-sm table-responsive-sm table-hover table-outline mb-0">
                <thead>
                <tr>
                    <td> id</td>
                    <td> Название</td>
                    <td> Контроллер</td>
                    <td> Опубликовано</td>
                    <td> Создатель</td>
                    <td> Создано</td>
                </tr>
                </thead>
                <tbody>
                @foreach($extensions as $extension)
                    <tr>
                        <td> {{ $extension->id }}</td>
                        <td>
                            <a href="{{ route('admin.extensions.edit', ['id' => $extension->id]) }}">
                                {{ $extension->name }}
                            </a>
                        </td>
                        <td> {{ $extension->controller }}</td>
                        <td> {{ $extension->published }}</td>
                        <td> {{ $extension->author->name }}</td>
                        <td> {{ $extension->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection