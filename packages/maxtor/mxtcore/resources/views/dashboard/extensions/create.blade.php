@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.extensions.create') }} @endsection

@section('content')

    <div class="card">
        <div class="card-header">
            <i class="fa fa-plus"></i> Новое расширение
        </div>
        <div class="card-body">
            {!! Form::model($extension, ['url' => route('admin.extensions.store')]) !!}
            @include ('mxtcore::dashboard.extensions.form', ['submitButtonText' => 'Добавить новое расширение' ])
            {!! Form::close() !!}
        </div>
    </div>

@endsection