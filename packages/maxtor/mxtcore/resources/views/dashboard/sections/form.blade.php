<div class="form-row">
    <div class="form-group col-md-6 {{ $errors->has('name') ? 'has-danger' : '' }}">
        {!! Form::label('name', 'Название:') !!}
        {!! Form::text('name', $section->title, ['class'=>'form-control']) !!}
        @if ($errors->has('name'))
            <small class="form-control-feedback">{{ $errors->first('name') }}</small>
        @endif
    </div>

    <div class="form-group col-md-3 {{ $errors->has('slug') ? 'has-danger' : '' }}">
        {!! Form::label('slug', 'Псевдоним секции (англ, без пробелов):') !!}
        <div class="col col-xs-8">
            {!! Form::text('slug', $section->slug, ['class'=>'form-control']) !!}
            @if ($errors->has('slug'))
                <small class="form-control-feedback">{{ $errors->first('slug') }}</small>
            @endif
        </div>
    </div>

    <div class="form-group col-md-3">
        <label for="published">Опубликовано</label>
        {!! Form::select('published', ['0' => 'Нет', '1' => 'Да'], $section->published, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group {{ $errors->has('description') ? 'has-danger' : '' }}">
    {!! Form::label('description', 'Описание секции:') !!}

    <tinymce name="description" html="{{ $section->description }}"></tinymce>

    @if ($errors->has('description'))
        <small class="form-control-feedback">{{ $errors->first('description') }}</small>
    @endif
</div>

<div class="form-group {{ $errors->has('attribs') ? 'has-danger' : '' }}">
    {!! Form::label('attribs', 'Атрибуты секции:') !!}
    <section-attribs field="attribs" rows="{{ $section->attribs }}"></section-attribs>
</div>

<div class="form-group row {{ $errors->has('note') ? 'has-danger' : '' }}">
    {!! Form::label('note', 'Комментарий:', ['class' => 'col-auto col-form-label']) !!}
    <div class="col col-xs-8">
        {!! Form::text('note', $section->note, ['class'=>'form-control']) !!}
        @if ($errors->has('note'))
            <small class="form-control-feedback">{{ $errors->first('note') }}</small>
        @endif
    </div>
</div>

<div class="form-group row {{ $errors->has('priority') ? 'has-danger' : '' }}">
    {!! Form::label('priority', 'Приортитет:', ['class' => 'col-auto col-form-label']) !!}
    <div class="col col-xs-8">
        {!! Form::text('priority', $section->priority === null ? 500 : $section->priority, ['class'=>'form-control']) !!}
        @if ($errors->has('priority'))
            <small class="form-control-feedback">{{ $errors->first('priority') }}</small>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="form-check">
        <input name="homepage_only" type="checkbox" class="form-check-input"  {{ $section->homepage_only ? 'checked' : '' }} id="homepage_only">
        <label class="form-check-label" for="homepage_only">
            Показывать только на главной:
        </label>
    </div>
</div>

<div class="form-row">
    <div class="col-auto">
        {!! Form::submit( $submitButtonText, ['class' => 'btn btn-primary']) !!}
    </div>
    <div class="col-auto">
        <a href="{{ route('admin.sections.index') }}" class="btn btn-dark">Отменить и перейти к списку</a>
    </div>
</div>