@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.sections.edit', $section) }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            Редактирование секции
        </div>
        <div class="card-body">
            {!! Form::model($section, ['url' => route('admin.sections.update', ['id' => $section->id])]) !!}
            {{ method_field('PUT') }}
            @include ('mxtcore::dashboard.sections.form', ['submitButtonText' => 'Редактировать секцию' ])
            {!! Form::close() !!}
        </div>
    </div>
@endsection