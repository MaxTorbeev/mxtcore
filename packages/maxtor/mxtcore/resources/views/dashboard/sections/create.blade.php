@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.sections.create') }} @endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Добавить новую секцию
        </div>
        <div class="card-body">

            {{--<section-form send-url="{{ route('admin.sections.store') }}" send-method="POST"></section-form>--}}

            {!! Form::model($section, ['url' => route('admin.sections.store')]) !!}
            @include ('mxtcore::dashboard.sections.form', ['submitButtonText' => 'Добавить новую секцию' ])
            {!! Form::close() !!}
        </div>
    </div>
@endsection