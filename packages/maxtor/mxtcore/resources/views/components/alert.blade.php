<div class="alert alert-{{ $type }} alert-dismissible fade show" role="alert">
    <div class="alert-title">{{ $title }}</div>
    <small>{{ $slot }}</small>
</div>