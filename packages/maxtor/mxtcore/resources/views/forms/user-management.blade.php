<div class="form-row">
    <div class="form-group col-md-6">
        <label for="published">Включено</label>
        {!! Form::select('enabled', ['Нет', 'Да'], $model->enabled === null ? 1 : $model->enabled, ['class' => 'form-control']) !!}
        @if ($errors->has('enabled'))
            <small class="form-control-feedback">{{ $errors->first('enabled') }}</small>
        @endif
    </div>

    <div class="form-group col-md-6">
        <label for="locked">Заблокировано</label>
        {!! Form::select('locked', ['Нет', 'Да'], $model->locked, ['class' => 'form-control']) !!}
        @if ($errors->has('locked'))
            <small class="form-control-feedback">{{ $errors->first('locked') }}</small>
        @endif
    </div>
</div>
