{{--{!! $filters or '' !!}--}}

<div class="bg-white-only  bg-auto no-border-xs">
    <div class="table-responsive">
        <table class="table table-sm table-responsive-sm table-striped table-hover table-outline mb-0">
            <thead>
            <tr>
                @foreach($form['fields'] as $th)
                    <th width="{{$th->width}}">{{$th->title}}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach($form['data'] as $key => $datum)
                <tr>
                    @foreach($form['fields'] as $tdKey => $td)
                        <td>
                            @if(!is_null($td->render))
                                {!! $td->handler($datum) !!}
                            @else
                                {{ array_get($datum, $td->name) }}
                            @endif
                        </td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>

        @isset($form['paginateLinks'])
            {!! $form['paginateLinks'] !!}
        @endisset
    </div>
</div>