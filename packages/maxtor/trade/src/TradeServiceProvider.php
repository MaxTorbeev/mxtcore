<?php

namespace MaxTor\Trade;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\IntlMoneyFormatter;
use Money\Money;

class TradeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'trade');

        //Migration
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        //Factories
        $this->app->make(EloquentFactory::class)->load(__DIR__ . '/../database/factories/');

        //Seeds
//        $this->app->make(Seeder::class)->load(__DIR__ . '/../database/seeds/');

        Route::middleware('web')
            ->namespace('MaxTor\Trade\Controllers\Frontend')
            ->group(__DIR__ . '/../routers/frontend_routes.php');
    }

    public function register()
    {

    }
}