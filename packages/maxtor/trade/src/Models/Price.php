<?php

namespace MaxTor\Trade\Models;

use Illuminate\Database\Eloquent\Model;
use MaxTor\MXTCore\Traits\Cacheable;

class Price extends Model
{
    use Cacheable;

    protected $table = 'prices';

    protected $guarded = ['id'];

    protected $with = ['currency', 'unit'];

    public function type()
    {
        return $this->hasOne(PriceType::class, 'id', 'price_type_id')->with('prices');
    }

    public function currency()
    {
        return $this->hasOne(PriceCurrency::class, 'id', 'currency_id');
    }

    public function setCurrencyByName($name)
    {
        if (strlen((string)$name) > 0) {
            $this->currency_id = PriceCurrency::firstOrCreate(['name' => (string)$name])->id;
        }

        $this->currency_id = PriceCurrency::firstOrCreate(['name' => 'RUR'])->id;
    }


    public function period()
    {
        return $this->hasOne(PricePeriod::class, 'id', 'period_id');
    }

    public function setPeriodByName($name)
    {
        if (strlen((string)$name) > 0) {
            $this->period_id = PricePeriod::firstOrCreate(['name' => (string)$name])->id;
        }

        return null;
    }

    public function unit()
    {
        return $this->hasOne(PriceUnit::class, 'id', 'unit_id');
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = floatval($value);
    }

    public function setUnitByName($name)
    {
        if (strlen((string)$name) > 0) {
            $this->unit_id = PriceUnit::firstOrCreate(['name' => (string)$name])->id;
        }

        return null;
    }

    public function setUtilitiesAttribute($attribute)
    {
        $this->utilities = floatval($attribute);
    }
}
