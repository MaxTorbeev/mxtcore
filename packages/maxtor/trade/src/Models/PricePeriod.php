<?php

namespace MaxTor\Trade\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\UserManagement;

class PricePeriod extends Model
{
    use Cacheable, UserManagement, SoftDeletes;

    protected $table = 'price_periods';

    protected $guarded = ['id'];
}
