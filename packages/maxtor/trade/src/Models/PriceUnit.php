<?php

namespace MaxTor\Trade\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\UserManagement;

class PriceUnit extends Model
{
    use Cacheable, UserManagement, SoftDeletes;

    protected $table = 'price_units';

    protected $guarded = ['id'];
}
