<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->increments('id');

            $table->decimal('value', 12, 2);
            $table->unsignedInteger('price_type_id');
            $table->unsignedInteger('currency_id');
            $table->unsignedInteger('period_id')->nullable();
            $table->unsignedInteger('unit_id')->nullable();
            $table->decimal('utilities', 12, 2)->nullable()->comment('коммунальные услуги');

            $table->foreign('price_type_id')
                ->references('id')
                ->on('price_types')->onDelete('cascade');

            $table->foreign('currency_id')
                ->references('id')
                ->on('price_currencies')->onDelete('cascade');

            $table->foreign('period_id')
                ->references('id')
                ->on('price_periods')->onDelete('cascade');

            $table->foreign('unit_id')
                ->references('id')
                ->on('price_units')->onDelete('cascade');

            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
