<?php

use App\User;
use MaxTor\Content\Models\Post;
use MaxTor\Content\Models\Category;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\MaxTor\Trade\Models\PriceUnit::class, function (Faker $faker) {
    return [
        'name' => $faker->title,

        'permission_id' => function () {
            return factory(\MaxTor\MXTCore\Models\Permission::class)->create()->id;
        },
        'role_id' => function () {
            return factory(\MaxTor\MXTCore\Models\Role::class)->create()->id;
        },
        'locked' => false,
        'enabled' => true,
        'created_user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'modified_user_id' => function () {
            return factory(User::class)->create()->id;
        }
    ];
});