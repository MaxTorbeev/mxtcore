<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\Services\SlugService;
use MaxTor\Location\Models\Address;

class PriceTypesDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('price_types')->insert([
            [
                'name' => 'Общая стоимость объекта',
                'slug' => 'total-cost',
                'is_base' => true
            ],
            [
                'name' => 'Стоимость за единицу измерения',
                'slug' => 'cost-per-unit',
                'is_base' => false
            ]
        ]);
    }
}
