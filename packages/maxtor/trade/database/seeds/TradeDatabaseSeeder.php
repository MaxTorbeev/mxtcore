<?php

use Illuminate\Database\Seeder;

class TradeDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(PriceTypesDatabaseSeeder::class);
    }
}
