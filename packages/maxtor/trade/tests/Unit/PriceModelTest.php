<?php

namespace MaxTor\Trade\Tests;


use MaxTor\Trade\Models\Price;
use MaxTor\Trade\Models\PriceCurrency;
use MaxTor\Trade\Models\PricePeriod;
use MaxTor\Trade\Models\PriceType;
use MaxTor\Trade\Models\PriceUnit;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PriceModelTest extends TestCase
{
    use RefreshDatabase;

    protected $price;

    public function setUp(): void
    {
        parent::setUp();
        $this->price = create(Price::class);
    }

    /** @test */
    public function a_price_has_type()
    {
        $this->assertInstanceOf(PriceType::class, $this->price->type);
    }

    /** @test */
    public function a_price_has_currency()
    {
        $this->assertInstanceOf(PriceCurrency::class, $this->price->currency);
    }

    /** @test */
    public function a_price_has_unit()
    {
        $this->assertInstanceOf(PriceUnit::class, $this->price->unit);
    }

    /** @test */
    public function a_price_has_period()
    {
        $this->assertInstanceOf(PricePeriod::class, $this->price->period);
    }
}
