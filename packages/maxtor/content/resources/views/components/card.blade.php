<article class="card card-withShadow">
    <a href="{{ $item->frontend_url }}" title="{{ $item->name }}">
        @isset($item->photo_preview)
            <img class="card-img-top" src="{{ $item->photo_preview->getThumb(262, 190) }}" alt="{{ $item->name }}">
        @else
            <img class="card-img-top" src="{{ config('realty.no-photo') }}" alt="{{ $item->name }}">
        @endif
    </a>
    <div class="card-body">
        <h3 class="card-title h6">
            <a href="{{ $item->frontend_url }}">
                {{ $item->name }}
            </a>
        </h3>
        @if($item->intro_text)
            <p class="card-text text-value-sm">
                <small>
                    {{ $item->intro_text }}
                </small>
            </p>
        @endif
    </div>
</article>