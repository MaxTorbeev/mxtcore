@if($item->photos->count())
    <div class="carousel_gallery">
        <div class="swiper-container gallery-thumbs">
            <div class="swiper-wrapper">
                @foreach($item->photos as $photo)
                    <div class="swiper-slide" style="background-image:url({{ $photo->getThumb(200, 200) }})"></div>
                @endforeach
            </div>
        </div>
        <div class="swiper-container gallery-top">
            <div class="swiper-wrapper">
                @foreach($item->photos as $photo)
                    <div class="swiper-slide">
                        <a href="{{ $photo->url }}" data-fancybox="photo-{{ $item->id }}">
                            <img src="{{ $photo->getThumb(820, 460) }}" alt="">
                        </a>
                    </div>
                @endforeach
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div>
        </div>
    </div>
@endif
