@if($categories->count())
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-mainMenu">
            <button class="navbar-toggler" data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
            >
                <i class="fas fa-bars"></i>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item {{ url()->current() === route('home') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('home') }}">Главная</a>
                    </li>

                    <li class="nav-item {{ url()->current() === route('frontend.realty.index') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('frontend.realty.index') }}">Недвижимость</a>
                    </li>

                    @foreach($categories as $item)
                        <li class="nav-item {{ url()->current() === $item->frontend_url ? 'active' : '' }}">
                            <a class="nav-link" href="{{ $item->frontend_url }}">{{ $item->name }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </nav>
    </div>
@endif