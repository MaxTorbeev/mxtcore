<div class="card card-withShadow">
    <ul class="list-group list-group-flush">
    @foreach($posts as $post)
        <li class="list-group-item">
            <time datetime="{{ $post->published_at->format('Y-m-d\TH:i:sP') }}" class="text-left mt-2 article_card_date">
                {{ $post->published_at->format('Y-m-d') }}
            </time>
            <h4 class="article_card_title">
                <a href="{{ $post->frontend_url }}">
                    {{ $post->name }}
                </a>
            </h4>
            <p class="article_card_intro">
                {{ $post->introtext }}
            </p>
        </li>
    @endforeach
    </ul>
</div>
