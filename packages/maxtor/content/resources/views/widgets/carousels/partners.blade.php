<div class="card card-withShadow card-strongShadow">
    <div class="card-heading">
        <h4>
            {{ $slider->name }}
        </h4>
    </div>
    <div class="card-body">
        @isset($slider->photos)
            <div class="swiper-container mb-4"
                 data-slides
                 data-slides-per-view="5"
                 data-slides-per-view-xs="2"
            >
                <div class="swiper-wrapper">
                    @foreach($slider->photos as $slide)
                        <div class="swiper-slide">
                            <img src="{{ asset($slide->getThumb(null, 75, ['scale' => true])) }}"
                                 data-src="{{ asset($slide->getThumb(null, 75, ['scale' => true])) }}"
                                 data-srcset="{{ asset($slide->getThumb(null, 150, ['scale' => true])) }} 2x, {{ asset($slide->getThumb(null, 75, ['scale' => true])) }} 1x"
                                 data-title="{{ $slide->original_name }}"
                                 class="img-fluid lazy"
                            >
                        </div>
                    @endforeach
                </div>

                <div class="sliderNavigators sliderNavigators-inWrapper swiper-navigation">
                    <div class="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
                    <div class="next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
                </div>
            </div>
        @endisset
    </div>
</div>

