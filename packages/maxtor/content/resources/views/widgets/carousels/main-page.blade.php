@isset($slider->photos)
    <div class="loaded swiper-container swiper-mainpage swiper-mainpage-textLight mb-4"
         data-slides
         data-slides-per-view="1"
         data-slides-per-view-xs="1"
         {{--data-lazy="0"--}}
         {{--data-loop="1"--}}
    >
        <div class="swiper-wrapper">
            @foreach($slider->photos as $slide)
                <div class="swiper-slide">
                    <img src="{{ asset($slide->getThumb(1440, 80, ['blur' => 50])) }}"
                         data-src="{{ asset($slide->getThumb(1440, 80)) }}"
                         data-srcset="{{ asset($slide->getThumb(1440, 80)) }} 2x, {{ asset($slide->getThumb(1440, 80)) }} 1x"
                         class="img-fluid lazy"
                         style="width: 100%"
                    >
                </div>
            @endforeach
        </div>

        {{--<div class="sliderNavigators sliderNavigators-inWrapper swiper-navigation">--}}
            {{--<div class="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>--}}
            {{--<div class="next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>--}}
        {{--</div>--}}
    </div>
@endisset