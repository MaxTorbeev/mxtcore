<div class="form-group">
    {!! Form::label('redirect_from', 'Redirect from:') !!}
    {!! Form::text('redirect_from', $model->redirect->redirect_from ?? null, ['class'=>'form-control ']) !!}

    @if ($errors->has('redirect_from'))
        <div class="invalid-feedback">{{ $errors->first('redirect_from') }}</div>
    @endif
</div>
