<div class="form-row">
    {!! Form::label('publish_down', 'Дата публикации:') !!}
    <div class="input-group mb-2">
        <div class="input-group-prepend">
            <div class="input-group-text">
                <i class="fa fa-calendar"></i>
            </div>
        </div>
        {!! Form::input('text', 'published_at', $model->published_at === null ? \Carbon\Carbon::now() : $model->published_at, ['class'=>'form-control', 'data-datetimepicker', 'data-timepicker' => 'true' ]) !!}
    </div>
</div>

<div class="form-row">
    {!! Form::label('publish_up', 'Дата начала публикации:') !!}
    <div class="input-group mb-2">
        <div class="input-group-prepend">
            <div class="input-group-text">
                <i class="fa fa-calendar"></i>
            </div>
        </div>
        {!! Form::input('text', 'publish_up', $model->publish_up, ['class'=>'form-control', 'data-default-date' => 0, 'data-datetimepicker', 'data-timepicker' => 'true' ]) !!}
    </div>
</div>

<div class="form-row">
    {!! Form::label('publish_down', 'Дата окончания публикации:') !!}
    <div class="input-group mb-2">
        <div class="input-group-prepend">
            <div class="input-group-text">
                <i class="fa fa-calendar"></i>
            </div>
        </div>
        {!! Form::input('text', 'publish_down', $model->publish_down, ['class'=>'form-control', 'data-default-date' => 0, 'data-datetimepicker', 'data-timepicker' => 'true' ]) !!}
    </div>
</div>