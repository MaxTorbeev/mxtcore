<div class="form-group">
    {!! Form::label('head_title', 'head title:') !!}
    {!! Form::text('head_title', $model->head_title, ['class'=>'form-control ']) !!}
    @if ($errors->has('head_title'))
        <div class="invalid-feedback">{{ $errors->first('head_title') }}</div>
    @endif
</div>

<div class="form-group">
    {!! Form::label('metadesc', 'meta-description:') !!}
    {!! Form::textarea('metadesc', $model->metadesc, ['class'=>'form-control', 'rows' => 3]) !!}
    @if ($errors->has('metadesc'))
        <div class="invalid-feedback">{{ $errors->first('metadesc') }}</div>
    @endif
</div>

<div class="form-group">
    {!! Form::label('metakey', 'meta-keywords:') !!}
    {!! Form::textarea('metakey', $model->metakey, ['class'=>'form-control', 'rows' => 3]) !!}
    @if ($errors->has('metakey'))
        <div class="invalid-feedback">{{ $errors->first('metadesc') }}</div>
    @endif
</div>
