@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.content.categories.edit', $category) }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            Редактирование категория
        </div>

        <div class="card-body">
            {!! Form::model($category, ['url' => route('admin.content.categories.update', [$category->id]), 'files' => true]) !!}
            @method('PATCH')
            @include ('content::dashboard.categories.form', ['submitButtonText' => 'Редактировать категорию'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection