<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('name', 'name:') !!}
            {!! Form::text('name', null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('slug', 'Ссылка:') !!}
            {!! Form::text('slug', null, ['class'=>'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::label('description', 'Описание:') !!}
            @if ($errors->has('description'))
                <small class="form-control-feedback">{{ $errors->first('description') }}</small>
            @endif
            <tinymce name="description" html="{{ $category->description }}"></tinymce>
        </div>
    </div>
    <div class="col-sm-4">
        @if($category->exists )
            <file-upload-once
                    upload-url="{{ route('admin.content.categories.photo', ['id' => $category->id]) }}"
                    file-list-url="{{ route('api.content.photo.getPhotoList', ['subject_id' => $category->id, 'type' => $category->getPhotoType()]) }}"
            ></file-upload-once>
        @else
            <div class="alert alert-info">
                Что бы добавить изображение, необходимо создать категорию
            </div>
        @endif

        <div class="form-group">
            {!! Form::label('parent_id', 'Родительская категория:') !!}

            <select id="parent_id" name="parent_id" class="form-control select2">
                <option value="0">Корневая категория</option>
                @foreach($categories as $catId => $parentCategory)
                    <option value="{{ $catId }}" {{ $catId == $category->parent_id ? 'selected' : ''}}>{{ $parentCategory }}</option>
                @endforeach
            </select>
        </div>

        @include ('mxtcore::forms.user-management', ['model' => $category])
        @include ('content::forms.publish-timestamps', ['model' => $category])
        @include ('content::forms.seo', ['model' => $category])
    </div>
</div>

<div class="form-group">
    {!! Form::submit( $submitButtonText, ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.content.categories.index') }}" class="btn btn btn-secondary">Отмена</a>

    @if($category->exists)
        <button type="submit" name="action" value="delete" class="pull-right btn btn btn-danger">Удалить</button>
    @endif
</div>

@section('footer')

@endsection