@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.content.categories.create') }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            Новая категория
        </div>

        <div class="card-body">
            {!! Form::model($category = new \MaxTor\Content\Models\Category(), ['url' => route('admin.content.categories.store'), 'files' => true, 'method' => 'POST']) !!}
            @include ('content::dashboard.categories.form', ['submitButtonText' => 'Добавить категорию'])
            {!! Form::close() !!}
        </div>
    </div>

@endsection