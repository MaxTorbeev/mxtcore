@extends('mxtcore::layouts.dashboard')

{{--@section('breadcrumbs') {{ Breadcrumbs::render('admin.content.reviews.edit', $review) }} @endsection--}}

@section('content')
    <div class="card">
        <div class="card-header">Редактировать отзыв</div>

        <div class="card-body">
            {!! Form::model($review, ['url' => route('admin.content.reviews.update', $review)]) !!}
            @method('PUT')
            @include ('content::dashboard.reviews.form', ['submitButtonText' => 'Редактировать отзыв' ])
            {!! Form::close() !!}
        </div>
    </div>
@endsection
