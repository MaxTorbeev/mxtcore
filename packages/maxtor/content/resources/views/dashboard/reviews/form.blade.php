<div class="form-row">
    <div class="col-md-3">
        <div class="form-group  {{ $errors->has('user_name') ? 'has-danger' : '' }}">
            {!! Form::label('user_name', 'Ваше имя:') !!}
            {!! Form::text('user_name', null, ['class'=>'form-control']) !!}
            @if ($errors->has('user_name'))
                <small class="form-control-feedback">{{ $errors->first('user_name') }}</small>
            @endif
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group {{ $errors->has('user_email') ? 'has-danger' : '' }}">
            {!! Form::label('user_email', 'Ваш email:') !!}
            {!! Form::text('user_email', null, ['class'=>'form-control']) !!}
            @if ($errors->has('user_email'))
                <small class="form-control-feedback">{{ $errors->first('user_email') }}</small>
            @endif
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group {{ $errors->has('user_phone') ? 'has-danger' : '' }}">
            {!! Form::label('user_phone', 'Номер телефона:') !!}
            {!! Form::text('user_phone', null, ['class'=>'form-control']) !!}
            @if ($errors->has('user_phone'))
                <small class="form-control-feedback">{{ $errors->first('user_phone') }}</small>
            @endif
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group {{ $errors->has('rating') ? 'has-danger' : '' }}">
            {!! Form::label('rating', 'Ваша оценка:') !!}
            {!! Form::select('rating',  [ 'Без оценки', '1', '2', '3', '4', '5' ], null, ['class'=>'form-control custom-select']) !!}
            @if ($errors->has('rating'))
                <small class="form-control-feedback">{{ $errors->first('rating') }}</small>
            @endif
        </div>
    </div>
</div>

<div class="form-row">
    <div class="col-md-8">
        <div class="form-group {{ $errors->has('title') ? 'has-danger' : '' }}">
            {!! Form::label('name', 'Заголовок:') !!}
            {!! Form::text('name', null, ['class'=>'form-control']) !!}
            @if ($errors->has('name'))
                <small class="form-control-feedback">{{ $errors->first('name') }}</small>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{ $errors->has('status') ? 'has-danger' : '' }}">
            {!! Form::label('status', 'Статус публикации:') !!}
            {!! Form::select('status',  $review->statuses, $review->status, ['class'=>'form-control custom-select']) !!}
            @if ($errors->has('status'))
                <small class="form-control-feedback">{{ $errors->first('status') }}</small>
            @endif
        </div>
    </div>
</div>

<div class="form-row">
    <div class="col-md-12">
        <div class="form-group  {{ $errors->has('body') ? 'has-danger' : '' }}">
            {!! Form::label('body', 'Отзыв:') !!}
            {!! Form::textarea('body', null, ['class'=>'form-control', 'rows'=>'6' ]) !!}
            @if ($errors->has('body'))
                <small class="form-control-feedback">{{ $errors->first('body') }}</small>
            @endif
        </div>
    </div>
</div>

<div class="form_buttonGroups">
    <div class="form-group">
        {!! Form::submit( $submitButtonText, ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('admin.content.reviews.index') }}" class="btn btn btn-secondary">Отменить и вернуться к списку</a>
    </div>
</div>

