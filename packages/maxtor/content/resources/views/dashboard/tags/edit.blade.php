@extends('mxtcore::layouts.dashboard')

@section('content')
    {!! Form::model($tag , ['url' => route('admin.content.tags.update', ['id' => $tag->id]), 'files' => true]) !!}
        @include ('content::dashboard.tags.form', ['submitButtonText' => 'Редактировать тег' ])
    {!! Form::close() !!}
@endsection
