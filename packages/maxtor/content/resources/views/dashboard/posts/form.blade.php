<div class="form-row">
    <div class="col-md-9">
        <div class="form-group  {{ $errors->has('name') ? 'has-danger' : '' }}">
            {!! Form::label('name', 'Название:') !!}
            {!! Form::text('name', null, ['class'=>'form-control']) !!}
            @if ($errors->has('name'))
                <small class="form-control-feedback">{{ $errors->first('name') }}</small>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group  {{ $errors->has('slug') ? 'has-danger' : '' }}">
            {!! Form::label('slug', 'Ссылка:') !!}
            {!! Form::text('slug', null, ['class'=>'form-control']) !!}
            @if ($errors->has('slug'))
                <small class="form-control-feedback">{{ $errors->first('slug') }}</small>
            @endif
        </div>
    </div>
</div>

<div class="form-row">
    <div class="col-sm-9">
        <div class="form-group  {{ $errors->has('intro_text') ? 'has-danger' : '' }}">
            {!! Form::label('intro_text', 'Вводный текст:') !!}
            {!! Form::textarea('intro_text', null, ['class'=>'form-control', 'rows'=>'4' ]) !!}
            @if ($errors->has('intro_text'))
                <small class="form-control-feedback">{{ $errors->first('intro_text') }}</small>
            @endif
        </div>
        <div class="form-group  {{ $errors->has('full_text') ? 'has-danger' : '' }}">
            {!! Form::label('full_text', 'Полный текст:') !!}

            <tinymce name="full_text" html="{{ $post->full_text }}"></tinymce>
            @if ($errors->has('full_text'))
                <small class="form-control-feedback">{{ $errors->first('full_text') }}</small>
            @endif
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group  {{ $errors->has('category_id') ? 'has-danger' : '' }}">
            {!! Form::label('category_id', 'Категории:') !!}
            {!! Form::select('category_id', $categories, null, ['class'=>'form-control select2']) !!}
        </div>

        <div class="form-group  {{ $errors->has('_lft') ? 'has-danger' : '' }}">
            {!! Form::label('_lft', 'Показывать после:') !!}
            {!! Form::select('_lft', $posts, null, ['class'=>'form-control select2']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('tags', 'Tags:') !!}
            {!! Form::select('tags[]', $tags, $post->tags, ['class'=>'form-control select2', 'multiple', 'data-tags' => 'true']) !!}
        </div>

        @include ('mxtcore::forms.user-management', ['model' => $post])
        @include ('content::forms.publish-timestamps', ['model' => $post])
        @include ('content::forms.seo', ['model' => $post])
        @include ('content::forms.redirect', ['model' => $post])
    </div>
</div>

<div class="panel form_buttonGroups form_buttonGroups-fixed">
    <div class="form-group">
        {!! Form::submit( $submitButtonText, ['class' => 'btn btn-primary']) !!}
        <button type="submit" name="action" value="save_and_close" class="btn btn btn-success">Сохранить и перейти к списку</button>
        <button type="submit" name="action" value="save_and_create" class="btn btn btn-success">Сохранить и создать новый</button>
        <a href="{{ route('admin.content.posts.index') }}" class="btn btn btn-warning text-right">Не сохранять и перейти к списку</a>

        @if($post->exists)
            <button type="submit" name="action" value="delete" class="pull-right btn btn btn-danger">Удалить</button>
        @endif
    </div>
</div>

@section('footer')

@endsection