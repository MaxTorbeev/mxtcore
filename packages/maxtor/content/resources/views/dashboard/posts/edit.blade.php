@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.content.posts.edit', $post) }} @endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Редактирование поста
        </div>

        <div class="card-body">
            <div class="form-row">
                <div class="col-md-9">
                    {!! Form::model($post, ['url' =>  route('admin.content.posts.update', [$post->id]), 'files' => true, 'method' => 'PATCH']) !!}
                    @include ('content::dashboard.posts.form', [
                        'submitButtonText'  => 'Редактировать пост',
                        'deletePost'        => 'true'
                    ])
                    {!! Form::close() !!}
                </div>

                <div class="col-md-3">
                    <upload-files url="{{ route('admin.content.posts.photo', ['id' => $post->id]) }}" field-name="photos"></upload-files>
                    <file-thumbs url="{{  route('api.content.posts.photo', ['id' => $post->id]) }}" ></file-thumbs>
                </div>
            </div>

        </div>
    </div>

@endsection