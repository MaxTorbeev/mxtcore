<div class="row">
    <div class="col-lg-6">
        <div class="form-row">
            {!! Form::label('published_at', 'Дата публикации:') !!}
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
                {!! Form::input('text', 'published_at', $post->published_at == null ? date('Y-m-d H:i:s') : $post->published_at, ['class'=>'form-control', 'data-datetimepicker', 'data-timepicker' => 'true' ]) !!}
            </div>
        </div>

        <div class="form-row">
            {!! Form::label('published_at', 'Дата окончания публикации:') !!}
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
                {!! Form::input('text', 'publish_down', $post->publish_down, ['class'=>'form-control', 'data-default-date' => 0, 'data-datetimepicker', 'data-timepicker' => 'true' ]) !!}
            </div>
        </div>

        <div class="form-row">
            {!! Form::label('published_at', 'Поместить в архив после:') !!}
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
                {!! Form::input('text', 'archive_transfer_time', $post->archive_transfer_time, ['class'=>'form-control', 'data-default-date' => 0, 'data-datetimepicker', 'data-timepicker' => 'true' ]) !!}
            </div>
        </div>
    </div>
</div>