@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.content.carousels.create') }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            Новый слайдер
        </div>

        <div class="card-body">
            {!! Form::model($model = new \MaxTor\Content\Models\Carousel(), ['url' => route('admin.content.carousels.store'), 'files' => true]) !!}
            @include ('content::dashboard.carousels.form', ['submitButtonText' => 'Добавить новый слайдер' ])
            {!! Form::close() !!}
        </div>
    </div>
@endsection