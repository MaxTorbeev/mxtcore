<div class="row">
    <div class="col-md-8">
        <div class="form-group  {{ $errors->has('name') ? 'has-danger' : '' }}">
            {!! Form::label('name', 'Название:') !!}
            {!! Form::text('name', null, ['class'=>'form-control']) !!}
            @if ($errors->has('name'))
                <small class="form-control-feedback">{{ $errors->first('name') }}</small>
            @endif
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group  {{ $errors->has('slug') ? 'has-danger' : '' }}">
            {!! Form::label('slug', 'Псевдоним:') !!}
            {!! Form::text('slug', null, ['class'=>'form-control']) !!}
            @if ($errors->has('slug'))
                <small class="form-control-feedback">{{ $errors->first('slug') }}</small>
            @endif
        </div>


    </div>
</div>

<div class="row">
    <div class="col-md-9">
        @if( !$model->exists )
            <div class="alert alert-warning">
                Создайте слайдер, что бы начать добавлять изображения
            </div>
        @else
            <upload-files url="{{ route('admin.content.carousels.photo', ['id' => $model->id]) }}" field-name="photos"></upload-files>
            <hr>
            <file-thumbs url="{{ route('api.content.photo.getPhotoList', ['subject_id' => $model->id, 'type' => $model->getPhotoType()]) }}">
            </file-thumbs>
        @endif
    </div>
    <div class="col-md-3">
        @include ('mxtcore::forms.user-management', ['model' => $model])
        {{--@include ('content::forms.publish-timestamps', ['model' => $model])--}}
    </div>
</div>

<div class="panel form_buttonGroups form_buttonGroups-fixed">
    <div class="form-group">
        {!! Form::submit( $submitButtonText, ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('admin.content.carousels.index') }}" class="btn btn btn-secondary">Отменить и перейти к списку</a>
    </div>
</div>
