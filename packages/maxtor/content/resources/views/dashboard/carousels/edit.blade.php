@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.content.carousels.edit', $model) }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            {{ $model->name }}
        </div>

        <div class="card-body">
            {!! Form::model($model, ['url' => route('admin.content.carousels.update', $model), 'files' => true, 'method' => 'PUT']) !!}
            @include ('content::dashboard.carousels.form', ['submitButtonText' => 'Редактировать слайдер' ])
            {!! Form::close() !!}
        </div>
    </div>
@endsection