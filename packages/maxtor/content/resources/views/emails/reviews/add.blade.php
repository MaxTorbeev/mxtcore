<h1>Был добавлен новый отзыв</h1>
<ul>
    <li>Имя: {{ $review->user_name }}</li>
    <li>Email: {{ $review->user_email }}</li>
    @if($review->user_phone)
        <li>Номер телефона: {{ $review->user_phone }}</li>
    @endif
    <li>Оценка: {{ $review->rating }}</li>
    <li>Заголовок: {{ $review->title }}</li>
    <li>Отзыв: {{ $review->body }}</li>
</ul>

<p>
    Что бы опубликовать отзыв, следует перейти в панель администратор и найти отзыв под # {{ $review->id }}
</p>
