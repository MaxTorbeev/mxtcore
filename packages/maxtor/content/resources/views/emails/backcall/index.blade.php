<h1>Поступила заявка на «запись на консультацию»</h1>
<ul>
    <li>Имя: {{ $request['name'] }}</li>
    <li>Телефон: {{ $request['phone'] }}</li>

    @isset($request['message'])
        <li>Сообщение: {{ $request['message'] }}</li>
    @endif
</ul>
