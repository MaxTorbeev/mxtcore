<div class="row mb-4 mt-sm-4">
    @isset($post->photo_preview)
        <div class="col-md-4">
            <a href="{{ $post->frontend_url }}" title="{{ $post->name }}">
                <img src="{{ $post->photo_preview->getThumb(388,200) }}" alt="{{ $post->photo_preview->name }}" class="img-fluid">
            </a>
        </div>
    @endisset
    <div class="{{ $post->photo_preview ? 'col-md-8' : 'col-md-12' }}">
        <h3 class="content_name pb-0">
            <a href="{{ $post->frontend_url }}" class="h5 my-5" title="{{ $post->name }}">
                {{ $post->name }}
                @if(!$post->published)
                    <span class="badge badge-warning">@lang('mxtcore.unpublished')</span>
                @endif
            </a>
        </h3>

        <div class="content_published">
            <time datetime="{{ $post->published_at->format('Y-m-d\TH:i:sP') }}">
                {{ trans_choice('content.published_at', null, ['date' => $post->published_at->format('Y-m-d')]) }}
            </time>
        </div>

        <div class="content_intro_text">
            {{ $post->intro_text }}
        </div>
    </div>
</div>