@if($photos->count() >= 2)
    <div class="row no-gutters content_slider py-3">
        <div class="col-md-10">
            <div class="swiper-container"
                 data-slides-per-view="4"
                 {{--data-free-mode="true"--}}
                 data-space-between="30"
                 data-nav-prev-el="#portfolioPhotosNavs .prev"
                 data-nav-next-el="#portfolioPhotosNavs .next"
            >
                <div class="swiper-wrapper">
                    @foreach($photos as $photo)
                        <div class="swiper-slide">
                            <a href="{{ $photo->url }}"
                               data-caption="{{ $photo->original_name }} <br> {{ nl2br($photo->description) }}"
                               data-fancybox="gallery">
                                <img src="{{ $photo->thumb }}" class="img-fluid" alt="{{ $photo->original_name }}">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div id="portfolioPhotosNavs" class="sliderNavigators">
                <div class="prev">
                    <i class="fa fa-chevron-left" aria-hidden="true"></i>
                </div>
                <div class="next">
                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
@endif