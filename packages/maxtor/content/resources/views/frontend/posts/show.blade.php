@extends('layouts.app')

@inject('postsWidget', 'MaxTor\Content\Widgets\PostsWidget')

@section('head-meta')
    <title>{{ $post->name }}</title>
    <meta name="keywords" content="{{ $post->metakey }}">
    <meta name="description" content="{{ $post->metadesc }}">
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('home.content.categories.post', $post) }}
@endsection

@section('content')
    <div class="content_introBlock">
        <div class="container">
            <div class="row align-items-start">
                <div class="col-md-12">
                    <h1 class="content_title mt-2">{{ $post->name }}</h1>
                    @isset($post->photo_preview)
                        <div>
                            <img src="{{ $post->photo_preview->url }}" class="img-thumbnail" alt="{{ $post->name }}">
                        </div>
                    @endisset
                    <p class="lead">
                        {!! $post->intro_text !!}
                    </p>
                    <time datetime="{{ $post->published_at->format('Y-m-d\TH:i:sP') }}" class="text-left mt-2">
                        {{ trans_choice('content.published_at', null, ['date' => $post->published_at->format('Y-m-d') ]) }}
                    </time>
                </div>
            </div>
        </div>
    </div>

    <div class="container my-3">
        <div class="content_fullTextBlock">
            @include('content::frontend.posts.components.photo-gallery', ['photos' => $post->photos])
            {!! $post->full_text  !!}
            <div class="content_tagsBlock mt-3">
                @foreach($post->tags as $tag)
                    <a href="{{ route('frontend.content.tags.show', ['slug' => $tag->slug]) }}"
                       class="btn btn-outline-dark btn-sm my-1">
                        {{ $tag->name }}
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('aside_right')
    {!! $postsWidget->handle([
        'view' => 'vertical',
        'category_slug' => $post->category->slug
    ]) !!}
@endsection