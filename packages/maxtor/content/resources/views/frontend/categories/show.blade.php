@extends('layouts.app')

@section('head-meta')
    <title>{{ $category->name }}</title>
    <meta name="keywords" content="{{ $category->metakey }}">
    <meta name="description" content="{{ $category->metadesc }}">
@endsection

@section('breadcrumbs') {{ Breadcrumbs::render('home.content.categories', $category) }} @endsection

@section('content')
    <div class="container">
        <section class="section content content_list">
            <h1>{{ $category->name }}</h1>
            <div class="row">
                @foreach($categoryPosts as $post)
                    <div class="col-xl-2 col-md-3 col-6 mb-4">
                        @include('content::components.card', ['item' => $post])
                    </div>
                @endforeach
            </div>
			
			{{ $categoryPosts->appends(request()->input())->links() }}

            @if($category->description)
                <div class="section_description mb-md-5">
                    {!! $category->description   !!}
                </div>
            @endif

            <ul class="list-group list-group-flush">
                @foreach($category->children as $children)
                    <li class="list-group-item">
                        <a href="{{ $children->frontend_url }}">{{ $children->name }}</a>
                    </li>
                @endforeach
            </ul>
			
        </section>
    </div>
@endsection