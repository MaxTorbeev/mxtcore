@extends('layouts.app')

@section('head-meta')
    <title>{{ $tag->name }}</title>
    <meta name="keywords" content="{{ $tag->metakey }}">
    <meta name="description" content="{{ $tag->metadesc }}">
@endsection

@section('breadcrumbs') {{ Breadcrumbs::render('home.content.tag', $tag) }} @endsection

@section('content')

    <div class="container">
        <section class="section">

            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-8">
                    <h1 class="h1">{{ $tag->name }}</h1>

                    <div class="section_description mb-md-5">
                        {!! $tag->description   !!}
                    </div>

                    @forelse($tag->posts as $post)
                        @include('content::frontend.posts.components.post-row', $post)
                    @endforeach
            </div>
        </section>
    </div>



@endsection