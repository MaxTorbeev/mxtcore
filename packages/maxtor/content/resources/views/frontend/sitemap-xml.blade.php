<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{ url('/') }}</loc>
        <lastmod>2018-08-23T11:32:37+03:00</lastmod>
        <changefreq>always</changefreq>
        <priority>1</priority>
    </url>

    @foreach($realty as $item)
        <url>
            <loc>{{ route('frontend.realty.show', $item) }}</loc>
            @isset($item->updated_at)
                <lastmod>{{ $item->updated_at->format('Y-m-d\TH:i:sP') }}</lastmod>
            @endisset
            <changefreq>always</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach

    @foreach($categories as $category)
        <url>
            <loc>{{ $category->frontend_url }}</loc>
            @isset($category->updated_at)
                <lastmod>{{ $category->updated_at->format('Y-m-d\TH:i:sP') }}</lastmod>
            @else
                <lastmod>2018-08-23T11:32:37+03:00</lastmod>
            @endisset
            <changefreq>always</changefreq>
            <priority>1</priority>
        </url>

        @forelse($category->posts as $post)
            <url>
                <loc>{{ $post->frontend_url }}</loc>
                @isset($post->updated_at)
                    <lastmod>{{ $post->updated_at->format('Y-m-d\TH:i:sP') }}</lastmod>
                @else
                    <lastmod>2018-08-23T11:32:37+03:00</lastmod>
                @endisset
                <changefreq>always</changefreq>
                <priority>1</priority>
            </url>
        @empty
        @endforelse
    @endforeach
</urlset>
