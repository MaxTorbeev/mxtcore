<?php

namespace MaxTor\Content\Widgets;

use MaxTor\Content\Models\Carousel;
use MaxTor\MXTCore\Widgets\Widget;

class CarouselsWidget extends Widget
{
    public $packageName = 'content';

    public function handle($attributes = null)
    {
        $view = $attributes['view'] ?? 'default';

        $this->cacheKey = 'widgets::content.carousels::' . $view;

        if($slider = $this->getSlides($attributes)){
            return \Cache::remember($this->cacheKey, $this->cacheTime, function () use ($slider, $view) {
                return $this->view($view, compact('slider'));
            });
        }

        return null;
    }

    protected function getSlides($attributes)
    {
        return Carousel::where('slug', $attributes['slug'])->enabled()->first();
    }
}