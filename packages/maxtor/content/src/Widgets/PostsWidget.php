<?php

namespace MaxTor\Content\Widgets;

use MaxTor\Content\Models\Category;
use MaxTor\MXTCore\Widgets\Widget;

class PostsWidget extends Widget
{
    public $packageName = 'content';

    public function handle($attributes = null)
    {
        $this->attributes = $attributes;

        $this->view = $attributes['view'] ?? 'default';

        return $this->view($this->view, [
            'posts' => $this->getPosts(),
            'attributes' => $this->attributes
        ]);
    }

    protected function getPosts()
    {
        $posts = null;
        $this->cacheKey = $this->attributes['cacheKey'] ?? $this->getDefaultCacheKey() . '::' . $this->attributes['category_slug'];

        if ($this->cacheKey !== null) {
            $posts = \Cache::remember($this->cacheKey, $this->cacheTime, function () {
                if (isset($this->attributes['category_slug']) && $this->attributes['category_slug']) {
                    return Category::where('slug', $this->attributes['category_slug'])
                        ->latest()
                        ->published()
                        ->first()
                        ->posts()
                        ->limit($this->attributes['limit'] ?? 10)
                        ->get();
                }
            });
        } else {
            throw new \InvalidArgumentException('Cache key is not specified');
        }

        return $posts;
    }
}