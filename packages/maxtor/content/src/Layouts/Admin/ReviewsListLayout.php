<?php

namespace MaxTor\Content\Layouts\Admin;

use App\User;
use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Review;
use MaxTor\MXTCore\Layouts\AbstractTable;
use MaxTor\MXTCore\Fields\TD;

class ReviewsListLayout extends AbstractTable
{

    public $data = '';

    public function fields(): array
    {
        return [
            TD::name('id')->title('Код')
                ->setRender(function ($model) {
                    return $this->editAnchor($model);
                }),

            TD::name('user_name')->title(''),

            TD::name('status')->title('Статус')
                ->setRender(function ($model) {
                    return (new Review())->statuses[$model['status']];
                }),

            TD::name('type')->title('Отзыв для')
                ->setRender(function ($model) {
                    return $this->getType($model);
                }),

            TD::name('user_phone')->title('Номер телефона'),
            TD::name('user_email')->title('Email'),
            TD::name('user_ip')->title('IP адрес'),

            TD::name('created_user_id')->title('Автор'),

            TD::name('updated_at')->title('Обновлено'),
            TD::name('created_at')->title('Создано'),

            TD::name('delete')->title('')
                ->setRender(function ($model) {
                    return $this->deleteButtonHtml(route('admin.content.reviews.destroy', ['id' => $model['id']]));
                }),
        ];
    }

    protected function editAnchor($model)
    {
        return '<a href="' . route('admin.content.reviews.edit', ['id' => $model['id']]) . '">' . $model['id'] . '</a>';
    }

    protected function getType($model)
    {
        if($model['subject_type']){
            return (new $model['subject_type'])->where('id', $model['subject_id'])->first()->name;
        } else {
            return '--';
        }
    }
}