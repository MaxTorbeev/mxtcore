<?php

namespace MaxTor\Content\Layouts\Admin;

use App\User;
use MaxTor\Content\Models\Category;
use MaxTor\MXTCore\Layouts\AbstractTable;
use MaxTor\MXTCore\Fields\TD;

class CategoriesListLayout extends AbstractTable
{

    public $permissions = ['create_post'];

    public $data = '';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('id')
                ->title('Код'),

            TD::name('name')
                ->title('Название')
                ->width(550)
                ->setRender(function ($model) {
                    return $this->editCategoryAnchor($model);
                }),

            TD::name('author')
                ->title('Родитель')
                ->setRender(function ($model) {
                    return $this->editCategoryAnchor($model['parent_id']);
                }),

            TD::name('enabled')
                ->title('')
                ->setRender(function ($model) {
                    return $this->publishedBadgeHtml($model);
                }),

            TD::name('hits')
                ->title('Просмотры'),

            TD::name('author')
                ->title('Автор')
                ->setRender(function ($model) {
                    return '<a href="' . route('admin.users.edit', ['id' => $model['created_user_id']]) . '">'
                        . User::whereId($model['created_user_id'])->first()->name .
                        '</a>';
                }),

            TD::name('updated_at')
                ->title('Обновлено'),

            TD::name('created_at')
                ->title('Создано'),

            TD::name('created_at')->title('Переместить')
                ->setRender(function ($model) {
                    return $this->moveModelAnchors($model);
                }),

            TD::name('delete')->title('')
                ->setRender(function ($model) {
                    return '<delete action="' . route('admin.content.categories.destroy', ['id' => $model['id']]) . '">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </delete>';
                }),
        ];
    }

    private function editCategoryAnchor($category)
    {
        return $category['id'] !== null
            ? '<a href="' . route('admin.content.categories.edit', ['id' => $category['id']]) . '">'
            . str_repeat('-- ', $category['depth']) . $category['name'] .
            '</a>'
            : '';
    }

    private function moveModelAnchors($model)
    {
        return '
            <a href="' . route('admin.content.categories.move', ['category' => $model['id'], 'direction' => 'up']) . '">Вверх</a>
            |
            <a href="' . route('admin.content.categories.move', ['category' => $model['id'], 'direction' => 'down']) . '">Вниз</a>
            ';
    }
}