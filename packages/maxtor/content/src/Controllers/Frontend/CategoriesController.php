<?php

namespace MaxTor\Content\Controllers\Frontend;

use App\Http\Flash;
use Gate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;

use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Post;
use MaxTor\Content\Requests\CategoryRequest;
use MaxTor\Content\Requests\PostRequest;

class CategoriesController extends Controller
{

}
