<?php

namespace MaxTor\Content\Controllers\Frontend;

use App\Http\Flash;
use Gate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;

use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Post;
use MaxTor\Content\Models\Redirect;
use MaxTor\Content\Requests\CategoryRequest;
use MaxTor\Content\Requests\PostRequest;

class ContentController extends Controller
{
    public function index()
    {
        return view('content::frontend.siteMap');
    }

    public function show($slugs)
    {
        $slugsInPath = explode('/', $slugs);

        $endSlug = end($slugsInPath);

        // Определяем не пост ли это
        if (Post::where('slug', $endSlug)->count()) {
            $post = Post::where('slug', $endSlug)->firstOrFail();
            views($post)->record();

            if ($post->frontend_url === route('frontend.content.category', ['url' => $slugs]))
                return view('content::frontend.posts.show', compact('post'));
        }

        // Если не пост, то возможно категория
        $category = Category::where('slug', $endSlug)->firstOrFail();
//        views($category)->record();

        $categoryPosts = Post::where('category_id', $category->id)
            ->paginate(site_config('base_paginate', config('mxtcore.paginate'))->value);


        if ($category->frontend_url === route('frontend.content.category', ['url' => $slugs])){
            if(view()->exists('pages.content.categories.' . $category->slug)){
                return view('pages.content.categories.' . $category->slug, compact('category', 'categoryPosts'));
            }

            return view('content::frontend.categories.show', compact('category', 'categoryPosts'));
        }

        return abort(404);
    }
}