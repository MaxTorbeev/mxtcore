<?php

namespace MaxTor\Content\Controllers\Frontend;

use Gate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;

use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Post;
use MaxTor\Content\Models\Tag;
use MaxTor\Content\Requests\PostRequest;
use MaxTor\Realty\Models\Realty;

class SitemapController extends Controller
{
    public function index()
    {
        $categories = Category::published()->get();

        return view('content::frontend.sitemap');
    }

    public function xml()
    {
        $categories = Category::published()->get();
        $realty = Realty::published()->get();

        $output = \View::make('content::frontend.sitemap-xml')
                 ->with(compact('categories', 'realty'))
                 ->render();

        $siteMap = '<?xml version="1.0" encoding="UTF-8"?>' . $output;

        return response($siteMap)->withHeaders(['Content-Type' => 'text/xml']);
    }
}