<?php

namespace MaxTor\Content\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use MaxTor\Content\Mail\BackCall;
use MaxTor\Content\Requests\BackCallRequest;
use MaxTor\MXTCore\Models\Role;

class FeedbackController extends Controller
{
    protected $rootUsersEmails = [];

    public function __construct()
    {
        $this->rootUsersEmails = Role::where('name', 'root')->first()->users->pluck('email');
    }


    public function feedback()
    {
//        Mail::to('MXTSoftProw@yandex.ru')->send(new ReviewAdd($review));
    }

    public function backCall(BackCallRequest $request)
    {
        if ($request->isMethod('get')){
            return [
                'title' => 'Обратный звонок',
                'submit' => 'Отправить',
            ];
        }

        Mail::to($this->rootUsersEmails)->send(new BackCall($request));

        return response()->json(['message' => 'Сообщение отправлено!']);
    }
}
