<?php

namespace MaxTor\Content\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use MaxTor\Content\Mail\ReviewAdd;
use MaxTor\Content\Models\Review;
use MaxTor\Content\Requests\ReviewRequest;
use MaxTor\MXTCore\Controllers\DashboardController;

class ReviewsController extends Controller
{
    public function index()
    {
        $reviews = Review::published()->get();

        return view('content::frontend.reviews.index', compact('reviews'));
    }

    public function create()
    {
        return view('content::frontend.reviews.create');
    }

    public function store(ReviewRequest $request)
    {



        $review = Review::create([
            'user_name' => request('user_name'),
            'user_email' => request('user_email'),
            'user_phone' => request('user_phone'),
            'user_ip' => request()->ip(),
            'status' => 0,
            'rating' => request('rating'),
            'name' => request('name'),
            'body' => request('body')
        ]);

//        Mail::to(config('mxtcore.mailsTo', env('MAIL_USERNAME')))->send(new ReviewAdd($review));

        return redirect(route('frontend.content.reviews'))->with('flash', 'Спасибо за оставленный отзыв!');
    }
}