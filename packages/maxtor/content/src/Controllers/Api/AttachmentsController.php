<?php

namespace MaxTor\Content\Controllers\Api;

use App\Http\Controllers\Controller;
use MaxTor\Content\Models\Attachment;

class AttachmentsController extends Controller
{
    public function list($type, $subject_id)
    {
        return Attachment::where(['subject_id' => (int)$subject_id, 'type' => $type])
            ->orderBy('_lft')
            ->published()
            ->get();
    }
}