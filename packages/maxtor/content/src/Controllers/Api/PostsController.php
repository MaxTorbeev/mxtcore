<?php

namespace MaxTor\Content\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Post;

class PostsController extends Controller
{
    public function getPhotos(Post $post)
    {
        return $post->photos()->published()->orderBy('_lft')->get();
    }
}