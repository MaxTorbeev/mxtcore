<?php

namespace MaxTor\Content\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use MaxTor\Content\Filters\PhotosFilter;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Post;
use MaxTor\Content\Requests\PhotoRequest;

class PhotosController extends Controller
{
    public function index(PhotosFilter $filters, Request $request)
    {
        return Photo::latest()
            ->published()
            ->orderBy('_lft')
            ->filters($filters)->get();
    }

    public function url(Photo $photo)
    {
        $url = asset($photo->path . '/' . $photo->name);

        if (request('width') || request('height'))
            $url = $photo->getThumb(request('width'), request('height'));

        return redirect($url);
    }

    public function getPhotoList($type, $subject_id)
    {
        return Photo::where(['subject_id' => $subject_id, 'type' => $type])->orderBy('_lft')->published()->get();
    }

    public function update(Photo $photo)
    {

    }
}
