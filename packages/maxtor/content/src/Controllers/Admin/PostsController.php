<?php

namespace MaxTor\Content\Controllers\Admin;

use Gate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;

use MaxTor\Content\Filters\PostsFilters;
use MaxTor\Content\Layouts\Admin\PostsListLayout;
use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Post;
use MaxTor\Content\Models\Tag;
use MaxTor\Content\Requests\PhotoRequest;
use MaxTor\Content\Requests\PostRequest;
use MaxTor\MXTCore\Controllers\DashboardController;

class PostsController extends DashboardController
{
    public function __construct()
    {
        $this->middleware('check.permission:access_dashboard');
    }

    public function index(PostsFilters $filters)
    {
        $posts = Post::latest()
            ->filters($filters)
            ->paginate(site_config('base_paginate', config('mxtcore.paginate', 20))->value);

        return view('content::dashboard.posts.index', [
            'table' => new PostsListLayout($posts),
            'categories' => $this->getList(Category::defaultOrder()->get()->toFlatTree())
        ]);
    }

    /**
     * Create a new blog post.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create_post', Post::class);

        return view('content::dashboard.posts.create', [
            'posts' => $this->getList(Post::all()),
            'categories' => $this->getList(Category::all()),
            'tags' => Tag::pluck('name', 'id'),
            'photos' => (new Post)->photos->pluck('original_name', 'id')
        ]);
    }

    public function store(PostRequest $request)
    {
        $this->authorize('create_post', Post::class);

        $tags = $request->input('tags');

        $request->offsetUnset('tags');
        $post = Post::create($request->all());

        if ($tags)
            $this->syncTags($post, $tags);


        if($request->get('action') == 'save_and_close'){
            return redirect(route('admin.content.posts.index'))->with('flash', 'Сохранено');
        }

        if($request->get('action') == 'save_and_create'){
            return redirect(route('admin.content.posts.create'))->with('flash', 'Сохранено и создаем новое');
        }

        return redirect(route('admin.content.posts.edit', ['id' => $post->id]))
            ->with('flash', 'Пост создан успешно');
    }

    public function edit(Post $post)
    {
        $this->authorize('create_post', Post::class);

        return view('content::dashboard.posts.edit', [
            'posts' => $this->getList(Post::where('id', '<>', $post->id), true),
            'post' => $post,
            'categories' => $this->getList(Category::all()),
            'tags' => Tag::pluck('name', 'id'),
            'photos' => (new Post)->photos->pluck('original_name', 'id')
        ]);
    }

    public function update(Post $post, PostRequest $request)
    {
        $this->authorize('create_post', Post::class);

        if($request->get('action') == 'delete'){
            $post->delete();
            return redirect(route('admin.content.posts.index'))->with('flash', 'Удалено');
        }

        if ($request->input('tags')) {
            $this->syncTags($post, $request->input('tags'));
        }

        $request->offsetUnset('tags');
        $post->update($request->all());

        if($request->get('action') == 'save_and_close'){
            return redirect(route('admin.content.posts.index'))->with('flash', 'Сохранено');
        }

        if($request->get('action') == 'save_and_create'){
            return redirect(route('admin.content.posts.create'))->with('flash', 'Сохранено и создаем новое');
        }

        return redirect(route('admin.content.posts.edit', ['id' => $post->id]))
            ->with('flash', 'Сохранено');
    }

    public function destroy(Post $post)
    {
        $this->authorize('delete_post', Post::class);

        $post->delete();

//        if (request()->wantsJson()) {
//            return response([], 204);
//        }

        return redirect(route('admin.content.posts.index'))->with('flash', 'Пост успешно удален');
    }

    public function addPhoto(Post $post, PhotoRequest $request)
    {
        $photo = $this->makePhoto($request->file('photos'));
        $post->addPhoto($photo);

        return 'Done';
    }

    protected function makePhoto(UploadedFile $file)
    {
        return Photo::named($file)->move($file);
    }
}