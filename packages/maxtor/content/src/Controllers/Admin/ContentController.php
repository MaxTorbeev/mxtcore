<?php

namespace MaxTor\Content\Controllers\Admin;

use App\Http\Requests;

use MaxTor\MXTCore\Controllers\DashboardController;

class ContentController extends DashboardController
{
    public function index()
    {
        return view('content::dashboard.index');
    }
}