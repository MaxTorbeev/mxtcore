<?php

namespace MaxTor\Content\Controllers\Admin;


use Illuminate\Http\Request;
use MaxTor\Content\Models\Attachment;
use MaxTor\MXTCore\Controllers\DashboardController;

final class AttachmentsController extends DashboardController
{
    public function update(Attachment $attachment, Request $request)
    {
        $attachment->update([
            'original_name' => $request->input('original_name'),
            'description' => $request->input('description'),
        ]);

        return response()->json(['message' => 'Файл успешно отредактирован']);
    }

    public function destroy(Attachment $attachment)
    {
        if ($attachment->delete()) {
            return response()->json(['message' => 'Файл ' . $attachment->original_name . ' успешно был удален']);
        }

        return abort(403, 'Что то пошло не так!');
    }
}
