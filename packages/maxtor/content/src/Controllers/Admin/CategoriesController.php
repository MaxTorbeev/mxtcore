<?php

namespace MaxTor\Content\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Gate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;

use MaxTor\Content\Layouts\Admin\CategoriesListLayout;
use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Post;
use MaxTor\Content\Models\Tag;
use MaxTor\Content\Requests\CategoryRequest;
use MaxTor\Content\Requests\PhotoRequest;
use MaxTor\Content\Requests\PostRequest;
use MaxTor\MXTCore\Controllers\DashboardController;

class CategoriesController extends DashboardController
{
    public function index()
    {
        $nodes = Category::defaultOrder()->withDepth()->get()->toFlatTree();

        $table = new CategoriesListLayout($nodes);

        return view('content::dashboard.categories.index', compact('table'));
    }

    public function create()
    {
        $this->authorize('create_category');

        $categories = $this->getList(Category::defaultOrder()->get()->toFlatTree());

        return view('content::dashboard.categories.create', compact('categories'));
    }

    public function store(CategoryRequest $request)
    {
        $this->authorize('create_category');

        $category = Category::create($request->all());

        return redirect(route('admin.content.categories.edit', ['menu' => $category->id]))
            ->with('flash', 'Категория создана успешно');
    }

    public function edit(Category $category)
    {
        $this->authorize('create_category');

        return view('content::dashboard.categories.edit', [
            'category' => $category,
            'categories' => $this->getList(Category::defaultOrder()->where('id', '!=', $category->id)->get()->toFlatTree()),
        ]);
    }

    public function update(Category $category, CategoryRequest $request)
    {
        $this->authorize('create_category');

        if($request->get('action') == 'delete'){
            $category->delete();
            return redirect(route('admin.content.categories.index'))->with('flash', 'Удалено');
        }

        if ($category->update($request->all())) {
            return redirect(route('admin.content.categories.edit', ['id' => $category->id]))
                ->with('flash', 'Категория была отредактирована');
        }

        return flash('flash', 'Не изменить категорию');
    }

    public function destroy(Category $category)
    {
        $this->authorize('delete_category');

        if ($category->protected == true) {
            return response(['message' => 'Категория защищена от удаления'], 403);
        }

        $category->delete();

        return response(['message' => 'Категория успешно была удалена'], 204);
    }

    public function move(Category $category)
    {
        switch (request('direction')) {
            case 'up':
                $category->up();
                break;

            case 'down':
                $category->up();
                break;
        }

        return back();
    }

    public function addPhoto($id, PhotoRequest $request)
    {
        $photo = $this->makePhoto($request->file('photos'));

        Category::where('id', $id)->firstOrFail()->addPhoto($photo);

        return 'Done';
    }

    protected function makePhoto(UploadedFile $file)
    {
        return Photo::named($file)->move($file);
    }
}
