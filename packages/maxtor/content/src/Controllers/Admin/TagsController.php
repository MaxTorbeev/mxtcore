<?php

namespace MaxTor\Content\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use MaxTor\Content\Models\Tag;
use MaxTor\Content\Requests\TagRequest;

class TagsController extends Controller
{
    public function __construct()
    {
        $this->middleware('check.permission:access_dashboard');
    }

    public function index()
    {
        return view('content::dashboard.tags.index', [
            'tags' => Tag::all(),
        ]);
    }

    public function create()
    {
        $this->authorize('create_tag', Tag::class);

        return view('content::dashboard.tags.create');
    }

    public function store(TagRequest $request)
    {
        $this->authorize('create_tag', Tag::class);

        $tag = Tag::create($request->all());

        return redirect(route('admin.content.tags.edit', ['id' => $tag->id]))
            ->with('flash', 'Тег создан успешно');
    }

    public function edit(Tag $tag)
    {
        return view('content::dashboard.tags.edit', compact('tag') );
    }

    public function update(Tag $tag, TagRequest $request)
    {
        $tag = $tag->update($request);

        return redirect(route('admin.content.tags.edit', ['id' => $tag->id]))
            ->with('flash', 'Тег создан бы редактирован');
    }

    public function destroy(Tag $tag)
    {
        if ($tag->delete()){
            return response()->json(['message' => 'Тег успешно был удален']);
        }

        return response([], 401)->json(['message' => 'Невозможно удалить тег']);
    }
}