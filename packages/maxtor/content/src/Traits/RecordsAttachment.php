<?php

namespace MaxTor\Content\Traits;

use Illuminate\Support\Facades\Cache;
use MaxTor\Content\Models\Attachment;
use MaxTor\Content\Models\Photo;

trait RecordsAttachment
{
    /**
     * A subject is composed of many photos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachment()
    {
        return $this->morphMany(Attachment::class, 'subject');
    }

    public function addAttachment(Attachment $attachment)
    {
        $attachment->type = $this->getPhotoType();

        return $this->attachment()->save($attachment);
    }

    public function getAttachmentType(): string
    {
        return strtolower((new \ReflectionClass($this))->getShortName());
    }

    protected static function getAttachmentToRecord()
    {
        return ['created', 'updated'];
    }
}