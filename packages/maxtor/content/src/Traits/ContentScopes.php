<?php

namespace MaxTor\Content\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Laravelrus\LocalizedCarbon\LocalizedCarbon;

trait ContentScopes
{
    public function scopeCreated($query)
    {
        $query->where('created_at', '<=', Carbon::now());
    }

    public function scopePublished($query, $ability = 'create_post')
    {
        return $query->where('enabled', true);
    }

    public function getUpdatedAtForHumanAttribute()
    {
        return LocalizedCarbon::instance($this->created_at)->diffForHumans();
    }

    public function getCreatedAtForHumanAttribute()
    {
        return LocalizedCarbon::instance($this->created_at)->diffForHumans();
    }
}