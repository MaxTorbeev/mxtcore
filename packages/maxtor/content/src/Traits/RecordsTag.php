<?php

namespace MaxTor\Content\Traits;

use Illuminate\Support\Facades\DB;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Tag;

trait RecordsTag
{
    protected static function bootRecordsTag()
    {
        static::deleting(function ($model) {
            $model->tags()->detach();
        });
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }
}