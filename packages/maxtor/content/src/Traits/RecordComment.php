<?php

namespace MaxTor\Content\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Laravelrus\LocalizedCarbon\LocalizedCarbon;
use MaxTor\Content\Models\Comment;

trait RecordComment
{
    /**
     * A subject is composed of many comments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'subject');
    }

    public function addComment(Comment $comment)
    {
        $comment->type = $this->getCommentType();

        return $this->comments()->save($comment);
    }

    protected function getCommentType(): string
    {
        return strtolower((new \ReflectionClass($this))->getShortName());
    }


}