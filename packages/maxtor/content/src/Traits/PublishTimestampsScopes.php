<?php

namespace MaxTor\Content\Traits;

use Carbon\Carbon;

trait PublishTimestampsScopes
{
    public function setPublishedAtAttribute($date)
    {
        if(is_null($date)){
            $this->attributes['published_at'] = Carbon::now();
        } else {
            $this->attributes['published_at'] = $date;
        }
    }
}