<?php

namespace MaxTor\Content\Traits;

use Illuminate\Support\Facades\Cache;
use MaxTor\Content\Models\Photo;

trait RecordsPhoto
{
    /**
     * A subject is composed of many photos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photos()
    {
        return $this->morphMany(Photo::class, 'subject');
    }

    public function addPhoto(Photo $photo)
    {
        $photo->type = $this->getPhotoType();

        return $this->photos()->save($photo);
    }

    public function getPhotoType(): string
    {
        return strtolower((new \ReflectionClass($this))->getShortName());
    }

    public function getPhotoPreviewAttribute()
    {
        return Cache::rememberForever($this->cacheKey() . 'photo_preview', function () {
            return $this->photos()->orderBy('_lft')->first();
        });
    }

    protected static function getPhotosToRecord()
    {
        return ['created', 'updated'];
    }
}