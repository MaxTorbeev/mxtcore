<?php

namespace MaxTor\Content\Requests;


use Illuminate\Foundation\Http\FormRequest;

class AttachmentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'PATCH':
                return [
                    'original_name' => 'required|max:190'
                ];

            case 'POST':
            default:
                return [
                    'attachments' => 'required|max:500000',
                ];
        }
    }
}