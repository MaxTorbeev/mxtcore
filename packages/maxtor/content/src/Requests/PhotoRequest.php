<?php

namespace MaxTor\Content\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhotoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'PATCH':
                return [
                    'original_name' => 'required|max:190'
                ];

            case 'POST':
            default:
                return [
                    'photos' => 'required|mimes:jpg,jpeg,png,bmp|max:500000',
                ];
        }
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'original_name.required' => 'У файла должно быть имя',
            'photos.required' => 'Необходимо прикрепить фото файл',
            'photos.mimes' => 'Файл должен иметь MIME типы: jpg,jpeg,png,bmp',
        ];
    }
}
