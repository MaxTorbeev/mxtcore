<?php

namespace MaxTor\Content\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BackCallRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'phone' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'   => 'Как к Вам обращаться?',
            'phone.required'  => 'Укажите свой номер телефона, что бы мы могли связаться с вами',
        ];
    }
}
