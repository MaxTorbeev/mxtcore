<?php

namespace MaxTor\Content\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_name'     => 'required|max:191',
            'user_email'    => 'required|max:191',
            'body'          => 'required|max:1000',
            'rating'        => 'required|max:1',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'user_name.required'   => 'Как к Вам обращаться?',
            'user_email.required'  => 'Укажите свой email',
            'body.required'        => 'Опишите свой отзыв',
        ];
    }
}
