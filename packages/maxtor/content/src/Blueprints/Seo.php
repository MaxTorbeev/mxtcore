<?php

namespace MaxTor\Content\Blueprints;

use Illuminate\Database\Schema\Blueprint;

class Seo
{
    const HEAD_TITLE = 'head_title';
    const META_KEYWORDS = 'metakey';
    const META_DESCRIPTION = 'metadesc';
    const META_DATA = 'metadata';

    /**
     * Add default columns to the table. Also create an index.
     *
     * @param \Illuminate\Database\Schema\Blueprint $table
     */
    public static function columns(Blueprint $table)
    {
        $table->string(self::META_KEYWORDS)->nullable();
        $table->string(self::META_DESCRIPTION)->nullable();
        $table->string(self::META_DATA)->nullable();
        $table->string(self::HEAD_TITLE)->nullable()->comment('Заголовок в голове HTML документа');
    }

    /**
     * Drop columns.
     *
     * @param \Illuminate\Database\Schema\Blueprint $table
     */
    public static function dropColumns(Blueprint $table)
    {
        $columns = static::getDefaultColumns();
        $table->dropIndex($columns);
        $table->dropColumn($columns);
    }

    /**
     * Get a list of default columns.
     *
     * @return array
     */
    public static function getDefaultColumns()
    {
        return [
            static::META_KEYWORDS,
            static::META_DESCRIPTION,
            static::META_DESCRIPTION,
            static::HEAD_TITLE,
        ];
    }
}