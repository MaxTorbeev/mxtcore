<?php

namespace MaxTor\Content\Blueprints;

use Illuminate\Database\Schema\Blueprint;

class PublishTimestamps
{
    /**
     * Дата публикации
     */
    const PUBLISHED_AT = 'published_at';

    /**
     * Дата начала публикации
     */
    const PUBLISH_UP = 'publish_up';

    /**
     * Дата конца публикации
     */
    const PUBLISH_DOWN = 'publish_down';

    /**
     * Add default columns to the table. Also create an index.
     *
     * @param \Illuminate\Database\Schema\Blueprint $table
     */
    public static function columns(Blueprint $table)
    {
        $table->timestamp(self::PUBLISHED_AT)->nullable()->comment('Дата публикации');
        $table->timestamp(self::PUBLISH_UP)->nullable()->comment('Дата начала публикации');
        $table->timestamp(self::PUBLISH_DOWN)->nullable()->comment('Дата конца публикации');
    }

    /**
     * Drop columns.
     *
     * @param \Illuminate\Database\Schema\Blueprint $table
     */
    public static function dropColumns(Blueprint $table)
    {
        $columns = static::getDefaultColumns();
        $table->dropIndex($columns);
        $table->dropColumn($columns);
    }

    /**
     * Get a list of default columns.
     *
     * @return array
     */
    public static function getDefaultColumns()
    {
        return [
            static::PUBLISHED_AT,
            static::PUBLISH_UP,
            static::PUBLISH_DOWN
        ];
    }
}