<?php

namespace MaxTor\Content;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use MaxTor\Content\Blueprints\PublishTimestamps;
use MaxTor\Content\Blueprints\Seo;
use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Post;
use MaxTor\Content\Models\Redirect;
use MaxTor\Content\Models\Review;
use MaxTor\MXTCore\Models\Section;
use MaxTor\Content\ContentPackageHelper;

class ContentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include_once __DIR__ . "/Commands/ContentCommand.php";

        $this->app->register('MaxTor\MXTCore\MXTCoreServiceProvider');

        $this->app->singleton('content::install', function () {
            return new \MaxTor\Content\Commands\ContentCommand();
        });

        $this->commands('content::install');

        $this->loadViewsFrom(__DIR__ . '/../resources/views', ContentPackageHelper::getPackageName());

        $this->publishes([
            __DIR__ . '/../../public' => public_path('packages/maxtor/blog')
        ]);

        //Migration
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        //Routes
        $this->loadRoutesFrom(__DIR__ . '/../routers/admin_routes.php');
        $this->loadRoutesFrom(__DIR__ . '/../routers/api_routes.php');
        $this->loadRoutesFrom(__DIR__ . '/../routers/frontend_routes.php');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        Blueprint::macro('seo', function () {
            Seo::columns($this);
        });

        Blueprint::macro('publishTimestamps', function () {
            PublishTimestamps::columns($this);
        });
    }
}
