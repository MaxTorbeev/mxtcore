<?php

namespace MaxTor\Content\Filters;

use App\User;
use Illuminate\Http\Request;
use MaxTor\Content\Models\Category;
use MaxTor\MXTCore\Filters\Filters;

class CategoryFilters extends Filters
{
    protected $filters = ['name', 'parent', 'author', ];

    protected function parent($id)
    {
        return $this->builder->where('parent_id', $id);
    }

    protected function author($username)
    {
        $user = User::where('name', $username)->firstOrFail();

        return $this->builder->where('created_user_id', $user->id);
    }

    protected function name($text)
    {
        return $this->builder->where('name', 'like', "%{$text}%");
    }
}