<?php

namespace MaxTor\Content\Filters;

use App\User;
use Illuminate\Http\Request;
use MaxTor\MXTCore\Filters\Filters;

class PostsFilters extends Filters
{
    protected $filters = ['by', 'category'];

    /**
     * Filter the query by a give username.
     *
     * @param $username
     * @return mixed
     */
    protected function by($username)
    {
        $user = User::where('name', $username)->firstOrFail();

        return $this->builder->where('created_user_id', $user->id);
    }

    /**
     * Filter the query according to most popular threads.
     *
     * @return mixed
     */
    protected function category($id)
    {
        return $this->builder->where('category_id', $id);
    }


}