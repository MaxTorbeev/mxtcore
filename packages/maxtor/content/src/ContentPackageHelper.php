<?php

namespace MaxTor\Content;

use DaveJamesMiller\Breadcrumbs\Exceptions\InvalidBreadcrumbException;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\ServiceProvider;
use MaxTor\MXTCore\AbstractPackageHelper;
use MaxTor\MXTCore\Interfaces\PackageHelperInterface;

class ContentPackageHelper extends AbstractPackageHelper implements PackageHelperInterface
{
    public static function getPackageName()
    {
        return 'content';
    }

    /**
     * Breadcrumbs
     * @docs https://github.com/davejamesmiller/laravel-breadcrumbs/tree/master
     */
    public static function getAdminBreadcrumbs($bundle)
    {
        if(!Breadcrumbs::exists('admin.' . self::getPackageName())) {
            Breadcrumbs::for ('admin.' . self::getPackageName(), function ($trail) {
                $trail->parent('admin');
                $trail->push(self::getPackageName(), route('admin.' . self::getPackageName() . '.index'));
            });
        }

        Breadcrumbs::for('admin.' .  self::getPackageName() . '.' . $bundle . '.index', function ($trail) use ($bundle) {
            $trail->parent('admin.' . self::getPackageName());
            $trail->push($bundle, route('admin.' . self::getPackageName() . '.' . $bundle .'.index'));
        });

        Breadcrumbs::for('admin.' .  self::getPackageName() . '.' . $bundle . '.create', function ($trail) use ($bundle) {
            $trail->parent('admin.' . self::getPackageName() . '.' . $bundle . '.index');
            $trail->push('create ' . $bundle, route('admin.' . self::getPackageName() . '.' . $bundle . '.create'));
        });

        Breadcrumbs::for('admin.' .  self::getPackageName() . '.' . $bundle . '.edit', function ($trail, $model) use ($bundle) {
            $trail->parent('admin.' . self::getPackageName() . '.' . $bundle . '.index' );
            $trail->push($model->name, route('admin.' . self::getPackageName() . '.' . $bundle . '.edit', ['id' => $model->id]));
        });
    }
}
