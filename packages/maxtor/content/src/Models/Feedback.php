<?php

namespace MaxTor\Content\Models;

use Illuminate\Database\Eloquent\Model;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\UserManagement;

class Feedback extends Model
{
    use Cacheable, UserManagement;

    protected $table = 'feedbacks';

    protected $guarded = ['id'];
}
