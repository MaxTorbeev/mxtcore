<?php

namespace MaxTor\Content\Models;

use App\User;
use Carbon\Carbon;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\Sluggable;
use Illuminate\Database\Eloquent\Model;
use MaxTor\MXTCore\Traits\UserManagement;

class Tag extends Model
{
    use Sluggable, UserManagement, ContentScopes;

    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($tag) {
//            $tag->posts->each->detach();
        });
    }

    /**
     * Get the articles associated with the give tag.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->morphedByMany(Post::class, 'taggable')->withTimestamps();
    }
}
