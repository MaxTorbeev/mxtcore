<?php

namespace MaxTor\Content\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Laravelrus\LocalizedCarbon\Traits\LocalizedEloquentTrait;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\PublishTimestampsScopes;
use MaxTor\Content\Traits\RecordComment;
use MaxTor\Content\Traits\Redirectenable;
use MaxTor\Content\Traits\Searchable;
use MaxTor\Content\Traits\RecordsTag;
use MaxTor\Content\Traits\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use MaxTor\Content\Traits\RecordsPhoto;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\UserManagement;
use MaxTor\MXTCore\Traits\Viewable;

use CyrildeWit\EloquentViewable\Contracts\Viewable as ViewableContract;

class Post extends Model implements ViewableContract
{
    use RecordsPhoto,
        RecordsTag,
        Sluggable,
        Cacheable,
        UserManagement,
        ContentScopes,
        LocalizedEloquentTrait,
        RecordComment,
        Searchable,
        PublishTimestampsScopes,
        Redirectenable,
        Viewable;

    protected $searchable = ['name', 'full_text'];

    protected $guarded = ['id', 'redirect_from', 'action'];

    protected $cacheKeys = [
        'widgets::content.posts.carousel::news',
    ];

    public static $featuredTypes = [
        '0' => 'Обычный пост',
        '10' => 'Показывать на главной'
    ];

    /**
     * Additional fields to treat as Carbon instances
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'published_at'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('ordering', function (Builder $builder) {
            $builder->orderByRaw('published_at DESC');
        });
    }

    public function getFrontendUrlAttribute()
    {
        $url = Cache::rememberForever($this->cacheKey() . ':frontend_url', function () {
            $url = '';
            $post = $this;
            $category = $post->category;
            $categories = [$category];

            while (!is_null($category) && !is_null($category = $category->parent)) {
                $categories[] = $category;
            }

            foreach (array_reverse($categories) as $category) {
                $url .= $category->slug . '/' . $post->slug;
            }

            return $url;
        });

        return route('frontend.content.category', ['slugs' => $url]);
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    /**
     * Preview image
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function previewImage()
    {
        return $this->hasOne(Photo::class, 'id', 'preview_photo_id');
    }

    public function scopeFilters($query, $filters)
    {
        return $filters->apply($query);
    }

    public function setLinksAttribute($value)
    {
        $this->attributes['links'] = json_encode($value);
    }

    public function getLinksAttribute($value)
    {
        return json_decode($value);
    }
}
