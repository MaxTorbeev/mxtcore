<?php

namespace MaxTor\Content\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Kalnoy\Nestedset\NodeTrait;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\UserManagement;

class Attachment extends Model
{
    use Cacheable, UserManagement, NodeTrait, ContentScopes;

    protected $guarded = ['id'];

    protected $baseDir = 'upload/attachments';

    protected $appends = [
        'url',
        'update',
        'delete'
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($attachment) {
            if (Attachment::where('name', $attachment->name)->count() === 1) {
                $attachment = public_path($attachment->makePath() . '/' . $attachment->name);

                if (file_exists($attachment))
                    unlink($attachment);
            }
        });
    }

    public function getUrlAttribute()
    {
        return asset($this->path . '/' . $this->name);
    }

    public function getUpdateAttribute()
    {
        return route('admin.content.attachments.update', ['id' => $this->id]);
    }

    public function getDeleteAttribute()
    {
        return route('admin.content.attachments.destroy', ['id' => $this->id]);
    }

    protected function makePath()
    {
        if (!app()->environment('testing')) {
            return $this->baseDir . '/' . substr($this->name, 0, 2);
        }

        return 'public/' . $this->baseDir . '/' . substr($this->name, 0, 2);
    }

    /**
     * @param string $filename
     * @return mixed
     */
    public static function named($filename)
    {
        return (new static)->saveAs($filename);
    }

    protected function saveAs($file)
    {
        $this->name = md5_file($file) . '.' . $file->getClientOriginalExtension();
        $this->path = $this->makePath();
        $this->original_name = $file->getClientOriginalName();

        return $this;

    }

    public function move($file)
    {
        $file->move($this->makePath(), $this->name);
        $this->mime_type = $file->getClientMimeType();

        return $this;
    }

    public function setAttribsAttribute($value)
    {
        $this->attributes['attribs'] = json_encode($value);
    }

    public function getAttribsAttribute()
    {
        return json_decode(json_decode($this->attributes['attribs']));
    }

}
