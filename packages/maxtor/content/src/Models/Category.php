<?php

namespace MaxTor\Content\Models;

use App\User;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\Cache;
use Kalnoy\Nestedset\NodeTrait;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\PublishTimestampsScopes;
use MaxTor\Content\Traits\RecordsPhoto;
use MaxTor\Content\Traits\Searchable;
use MaxTor\Content\Traits\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\Content\Models\Post;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\Filterable;
use MaxTor\MXTCore\Traits\UserManagement;
use Illuminate\Database\Eloquent\Builder;
use MaxTor\MXTCore\Traits\Viewable;
use CyrildeWit\EloquentViewable\Contracts\Viewable as ViewableContract;

class Category extends Model  implements ViewableContract
{
    use Cacheable,
        UserManagement,
        SoftDeletes,
        ContentScopes,
        Searchable,
        RecordsPhoto,
        Filterable,
        Viewable,
        PublishTimestampsScopes;

    use Sluggable , NodeTrait {
        NodeTrait::replicate as replicateNode;
        Sluggable::replicate as replicateSlug;
    }


    public function replicate(array $except = null)
    {
        $instance = $this->replicateNode($except);
        (new SlugService())->slug($instance, true);

        return $instance;
    }

    public $cacheKeys = [
        'mapCategories',
        'widgets::content.categories.default',
        'widgets::content.categories.horizontal'
    ];

    protected $table = 'categories';

    protected $searchable = ['name', 'description'];

    protected $guarded = ['id', 'after_neighbor_id'];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($category) {
            $category->posts->each->category_id = null;
        });

        static::addGlobalScope('ordering', function (Builder $builder) {
            $builder->orderByRaw('`_lft` asc');
        });
    }

    public function getFrontendUrlAttribute()
    {
        $url = Cache::remember($this->cacheKey() . ':frontend_url', 60, function () {
            $url = '';
            $category = $this;
            $categories = [$category];

            while (!is_null($category) && !is_null($category = $category->parent)) {
                $categories[] = $category;
            }

            foreach (array_reverse($categories) as $category) {
                $url .= $category->slug . '/';
            }

            return $url;
        });

        return route('frontend.content.category', ['categories' => $url]);
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'category_id')->published();
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id')->with('parent');

    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id')->with('children');
    }
}
