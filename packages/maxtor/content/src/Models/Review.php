<?php

namespace MaxTor\Content\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\Reviewable;
use MaxTor\MXTCore\Fields\Builder;
use MaxTor\MXTCore\Traits\Cacheable;

class Review extends Model
{
    use Cacheable, ContentScopes;

    protected $guarded = ['id'];

    protected $cacheKeys = ['mapReviews'];

    public $statuses = [
        'Администратор еще не просмотрел',
        'Публикация подтверждена',
        'Отказ в публикации'
    ];

    protected $appends = [
        'updated_at_for_human',
        'created_at_for_human',
    ];

    protected static function boot()
    {
        parent::boot();

//        static::addGlobalScope('ordering', function (Builder $builder) {
//            $builder->orderByRaw('published_at DESC');
//        });
    }

    public function setUserIpAttribute()
    {
        $this->attributes['user_ip'] = request()->ip();
    }

    public function getStatusTextAttribute()
    {
        return $this->statuses[$this->status];
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('status', 1);
    }

    public function getUserPhoneAttribute($value){
        $user = auth()->user();

        if ($user && auth()->user()->can('access_dashboard')){
            return $value;
        }

        return null;
    }

    public function getUserEmailAttribute($value){
        $user = auth()->user();

        if ($user && auth()->user()->can('access_dashboard')){
            return $value;
        }

        return null;
    }
}
