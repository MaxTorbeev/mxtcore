<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Support\Arr;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PhotoTest extends TestCase
{
    use RefreshDatabase;

    protected $photo;

    public function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
    }

    /**
     * Создаем 5 фэйковых фотографий
     * Обращаемся к маршруту API, что бы проверить наличие фотографий
     * Отправляем отсортированный массив
     *
     * @test
     */
    public function a_photos_can_be_sorted_by_nested_set()
    {
        $this->withOutExceptionHandling();
        $this->signIn(create(User::class), 'root', ['access_dashboard']);
        $this->createPhotos(5);

        $response = $this->getJson('/api/content/photos/')->json();

        $this->assertEquals([
            1 => '1photo.jpg',
            2 => '2photo.jpg',
            3 => '3photo.jpg',
            4 => '4photo.jpg',
            5 => '5photo.jpg',
        ], Arr::pluck($response, 'original_name', 'id'));

        $response = $this->json('POST', '/admin/content/photos/move/', [
            'move' => [
                ['id' => 1, 'original_name' => '1photo.jpg'],
                ['id' => 3, 'original_name' => '3photo.jpg'],
                ['id' => 5, 'original_name' => '5photo.jpg'],
                ['id' => 2, 'original_name' => '2photo.jpg'],
                ['id' => 4, 'original_name' => '4photo.jpg'],
            ],
        ]);

        $response = $this->getJson('/api/content/photos/')->json();

        $this->assertEquals([
            1 => '1photo.jpg',
            3 => '3photo.jpg',
            5 => '5photo.jpg',
            2 => '2photo.jpg',
            4 => '4photo.jpg',
        ], Arr::pluck($response, 'original_name', 'id'));
    }
}
