<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug', 100)->index();

            $table->text('description')->nullable()->default(null);
            $table->string('note')->nullable()->default(null);

            $table->text('attribs')->nullable();

            $table->boolean('protected')->default(0)->comment = "Защищен от удаления";

            $table->integer('hits')->default(0)->nullable();

            $table->publishTimestamps();
            $table->seo();
            $table->userManagement();
            $table->nestedSet();
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
