<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->text('message')->nullable();
            $table->json('attribs')->nullable();
            $table->smallInteger('status')->nullable();
            $table->string('senders_ip')->comment('IP отправителя');
            $table->string('senders_user_agent')->comment('User agent IP ');

            $table->unsignedInteger('subject_id')->nullable();
            $table->string('subject_type', 50)->nullable()->index();

            $table->userManagement();
            $table->nestedSet();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedbacks');
    }
}
