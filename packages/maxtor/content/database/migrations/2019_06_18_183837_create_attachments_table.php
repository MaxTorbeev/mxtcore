<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('subject_id')->nullable();
            $table->string('subject_type', 50)->nullable()->index();
            $table->string('type', 50)->index()->nullable();
            $table->string('path');
            $table->string('original_name')->index();
            $table->text('description')->nullable();
            $table->string('mime_type')->nullable();
            $table->json('attribs')->nullable();
            $table->userManagement();
            $table->nestedSet();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
    }
}
