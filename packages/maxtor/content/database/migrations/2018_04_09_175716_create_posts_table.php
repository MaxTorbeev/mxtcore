<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id')->nullable()->default(null);
            $table->text('preview_photo_id')->nullable();

            $table->string('name');
            $table->string('slug')->index();

            $table->text('intro_text')->nullable();
            $table->text('full_text');
            $table->integer('hits')->default(0);

            $table->publishTimestamps();
            $table->seo();
            $table->userManagement();
            $table->nestedSet();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
