<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
                [
                    'name' => 'Покупка',
                    'slug' => 'kupit',
                    'created_user_id' => 1,
                ],
                [
                    'name' => 'Продать',
                    'slug' => 'prodat',
                    'created_user_id' => 1,
                ],
                [
                    'name' => 'Оценка',
                    'slug' => 'ocenka',
                    'created_user_id' => 1,
                ],
                [
                    'name' => 'О нас',
                    'slug' => 'o-nas',
                    'created_user_id' => 1,
                ],
                [
                    'name' => 'Услуги',
                    'slug' => 'uslugi',
                    'created_user_id' => 1,
                ],
                [
                    'name' => 'Новости',
                    'slug' => 'news',
                    'created_user_id' => 1,
                ],
                [
                    'name' => 'Статьи',
                    'slug' => 'articles',
                    'created_user_id' => 1,
                ],
                [
                    'name' => 'Контакты',
                    'slug' => 'kontakty',
                    'created_user_id' => 1,
                ]
            ]
        );
    }
}
