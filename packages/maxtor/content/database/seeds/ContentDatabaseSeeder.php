<?php

use Illuminate\Database\Seeder;

class ContentDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesSeeder::class);
        $this->call(RolesAndPermissionsTableSeeder::class);
    }
}