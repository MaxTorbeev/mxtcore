<?php

Route::group([
    'prefix' => '/',
    'namespace' => 'MaxTor\Content\Controllers\Frontend',
], function () {

    Auth::routes();

    Route::get('/reviews', ['as' => 'frontend.content.reviews', 'uses' => 'ReviewsController@index']);
    Route::get('/reviews/create', ['as' => 'frontend.content.reviews.create', 'uses' => 'ReviewsController@create']);
    Route::post('/reviews/create', ['as' => 'frontend.content.reviews.store', 'uses' => 'ReviewsController@store']);

    Route::post('/feedback/backcall', ['as' => 'frontend.content.feedback.backcall', 'uses' => 'FeedbackController@backCall']);

    Route::get('/tags/{tag}', ['as' => 'frontend.content.tags.show', 'uses' => 'TagsController@show']);

    Route::get('/sitemap.xml', ['as' => 'frontend.content.category', 'uses' => 'SitemapController@xml']);
    Route::get('/sitemap', ['as' => 'frontend.content.sitemap', 'uses' => 'SitemapController@index']);

    Route::get('/{categories}', ['as' => 'frontend.content.category', 'uses' => 'ContentController@show'])
        ->where('categories', '^[a-zA-Z0-9-_\/]+$');
});

/**
 * Breadcrumbs
 * @docs https://github.com/davejamesmiller/laravel-breadcrumbs/tree/master
 */

// Home > Content
Breadcrumbs::for ('home.search', function ($trail) {
    $trail->parent('home');

    $trail->push('Поиск по сайту', route('frontend.content.search'));
});

// Home > SiteMap
Breadcrumbs::for ('home.sitemap', function ($trail) {
    $trail->parent('home');
    $trail->push('Карта сайта', route('frontend.content.sitemap'));
});


// Home > Content > Categories
Breadcrumbs::for ('home.content.categories', function ($trail, $category) {
    $trail->parent('home');

    if ($category->parent()->count()) {
        foreach ($category->parent()->get() as $parent) {
            $trail->push($parent->name, $parent->frontend_url);
        }
    }

    $trail->push($category->name, $category->frontend_url);
});

// Home > Content > Categories > Post
Breadcrumbs::for ('home.content.categories.post', function ($trail, $post) {
    $trail->parent('home.content.categories', $post->category);

    $trail->push($post->name, $post->frontend_url);
});

// Home > Content > Tag
Breadcrumbs::for ('home.content.tag', function ($trail, $tag) {
    $trail->parent('home');

    $trail->push($tag->name, route('frontend.content.tags.show', ['tag' => $tag->slug]));
});