<?php
Route::group([
    'prefix' => '/api/content',
    'namespace' => 'MaxTor\Content\Controllers\Api',
    'middleware' => 'web'
], function () {
    Route::get('/', ['as' => 'api.content.index', 'uses' => 'ContentController@index']);
    Route::get('/posts/{post}/photos', ['as' => 'api.content.posts.photo', 'uses' => 'PostsController@getPhotos']);
    Route::get('/tags/', ['as' => 'api.content.posts.tags', 'uses' => 'TagsController@getTags']);
    Route::get('/photos/', ['as' => 'api.content.photo.all', 'uses' => 'PhotosController@index']);
    Route::get('/photo/{photo}/url', ['as' => 'api.content.photo.url', 'uses' => 'PhotosController@url']);
    Route::get('/photo/{type}/{subject_id}/', ['as' => 'api.content.photo.getPhotoList', 'uses' => 'PhotosController@getPhotoList']);
    Route::get('/attachment/{type}/{subject_id}/', ['as' => 'api.content.attachment.list', 'uses' => 'AttachmentsController@list']);
});