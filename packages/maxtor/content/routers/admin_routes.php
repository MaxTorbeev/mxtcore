<?php

use \MaxTor\Content\ContentPackageHelper;

Route::group([
    'prefix' => '/admin/content',
    'namespace' => 'MaxTor\Content\Controllers\Admin',
    'middleware' => 'web'
], function() {
    Route::get('/', ['as' => 'admin.content.index', 'uses' => 'ContentController@index']);

    Route::resource('posts', 'PostsController', ['as' => 'admin.content']);
    Route::resource('categories', 'CategoriesController', ['as' => 'admin.content']);
    Route::resource('tags', 'TagsController', ['as' => 'admin.content']);
    Route::resource('photos', 'PhotosController', ['as' => 'admin.content']);
    Route::resource('attachments', 'AttachmentsController', ['as' => 'admin.content']);
    Route::resource('reviews', 'ReviewsController', ['as' => 'admin.content']);
    Route::resource('carousels', 'CarouselsController', ['as' => 'admin.content']);

    Route::post('/posts/{post}/photos', ['as' => 'admin.content.posts.photo', 'uses' => 'PostsController@addPhoto']);

    Route::post('/categories/{category}/photos', ['as' => 'admin.content.categories.photo', 'uses' => 'CategoriesController@addPhoto']);

    Route::get('/categories/{category}/move', ['as' => 'admin.content.categories.move', 'uses' => 'CategoriesController@move']);

    Route::post('/carousels/{slider}/photos', ['as' => 'admin.content.carousels.photo', 'uses' => 'CarouselsController@addPhoto']);

//    Route::post('/photos/move', ['as' => 'admin.content.photos.move', 'uses' => 'PhotosController@move']);
});

ContentPackageHelper::getAdminBreadcrumbs('posts');
ContentPackageHelper::getAdminBreadcrumbs('categories');
ContentPackageHelper::getAdminBreadcrumbs('reviews');
ContentPackageHelper::getAdminBreadcrumbs('carousels');
ContentPackageHelper::getAdminBreadcrumbs('tags');