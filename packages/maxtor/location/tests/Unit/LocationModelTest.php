<?php

namespace MaxTor\Location\Tests\Unit;

use MaxTor\Location\Models\Address;
use MaxTor\Location\Models\Country;
use MaxTor\Location\Models\District;
use MaxTor\Location\Models\LocalityName;
use MaxTor\Location\Models\Location;
use MaxTor\Location\Models\Metro;
use MaxTor\Location\Models\RailwayStation;
use MaxTor\Location\Models\Region;
use MaxTor\Location\Models\SubLocalityName;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LocationModelTest extends TestCase
{
    use RefreshDatabase;

    protected $location;

    public function setUp(): void
    {
        parent::setUp();
        $this->location = create(Location::class);
    }

    /** @test */
    function a_location_has_country()
    {
        $this->assertInstanceOf(Country::class, $this->location->country);
    }

    /** @test */
    function a_location_has_region()
    {
        $this->assertInstanceOf(Region::class, $this->location->region);
    }

    /** @test */
    function a_location_has_district()
    {
        $this->assertInstanceOf(District::class, $this->location->district);
    }

    /** @test */
    function a_location_has_locality_name()
    {
        $this->assertInstanceOf(LocalityName::class, $this->location->localityName);
    }

    /** @test */
    function a_location_has_sub_locality_name()
    {
        $this->assertInstanceOf(SubLocalityName::class, $this->location->subLocalityName);
    }

    /** @test */
    function a_location_has_address()
    {
        $this->assertInstanceOf(Address::class, $this->location->address);
    }

    /** @test */
    function a_location_has_metro()
    {
        $this->assertInstanceOf(Metro::class, $this->location->metro);
    }

    /** @test */
    function a_location_has_railway_station()
    {
        $this->assertInstanceOf(RailwayStation::class, $this->location->railwayStation);
    }
}
