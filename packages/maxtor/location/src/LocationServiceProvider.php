<?php

namespace MaxTor\Location;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;

class LocationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'location');

        $this->publishes([__DIR__ . '/../../public' => public_path('packages/maxtor/location')]);

        //Migration
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        //Factories
        $this->app->make(EloquentFactory::class)->load(__DIR__ . '/../database/factories/');

        //Routes
        $this->mapApiRoutes();
        $this->mapAdminRoutes();
        $this->mapFrontendRoutes();
    }

    protected function mapFrontendRoutes(): void
    {
        Route::middleware('web')
            ->namespace('MaxTor\Location\Controllers\Frontend')
            ->group(__DIR__ . '/../routers/frontend_routes.php');
    }

    protected function mapApiRoutes(): void
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace('MaxTor\Location\Controllers\Api')
            ->group(__DIR__ . '/../routers/api_routes.php');
    }

    protected function mapAdminRoutes(): void
    {
        Route::prefix('/admin/location/')
            ->namespace('MaxTor\Location\Controllers\Api')
            ->group(__DIR__ . '/../routers/api_routes.php');
    }
}