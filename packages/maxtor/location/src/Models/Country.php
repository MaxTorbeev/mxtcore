<?php

namespace MaxTor\Location\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\Filterable;
use MaxTor\MXTCore\Traits\UserManagement;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class Country
 * @package MaxTor\Location\Models
 */
class Country extends Model
{
    use Cacheable,
        UserManagement,
        Sluggable,
        SoftDeletes,
        Filterable;

    protected $table = 'location_countries';

    protected $guarded = ['id'];
}
