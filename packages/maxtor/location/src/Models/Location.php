<?php

namespace MaxTor\Location\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\Filterable;
use MaxTor\MXTCore\Traits\UserManagement;
use MaxTor\Realty\Models\Realty;

class Location extends Model
{
    use Cacheable,
        UserManagement,
        SoftDeletes,
        Filterable;

    public function realty()
    {
        return $this->hasMany(Realty::class, 'id', 'location_id');
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    public function region()
    {
        return $this->hasOne(Region::class, 'id', 'region_id');
    }

    public function district()
    {
        return $this->hasOne(District::class, 'id', 'district_id');
    }

    public function localityName()
    {
        return $this->hasOne(LocalityName::class, 'id', 'locality_name_id');
    }

    public function subLocalityName()
    {
        return $this->hasOne(SubLocalityName::class, 'id', 'sub_locality_name_id');
    }

    public function address()
    {
        return $this->hasOne(Address::class, 'id', 'address_id');
    }

    public function metro()
    {
        return $this->hasOne(Metro::class, 'id', 'metro_id');
    }

    public function railwayStation()
    {
        return $this->hasOne(RailwayStation::class, 'id', 'railway_station_id');
    }
}
