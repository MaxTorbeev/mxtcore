<?php

namespace MaxTor\Location\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\Filterable;
use MaxTor\MXTCore\Traits\UserManagement;
use MaxTor\Realty\Models\Realty;

class LocalityName extends Model
{
    use Cacheable,
        UserManagement,
        Sluggable,
        SoftDeletes,
        Filterable;

    protected $table = 'location_locality_names';

    protected $guarded = ['id'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function locations()
    {
        return $this->hasMany(Location::class);
    }

    public function region()
    {
        $this->hasOne(Region::class, 'id', 'country_id');
    }

    public function setLocalityNameByImportInternalId($id, $name, $regionId)
    {
        if(LocalityName::where('import_internal_id', $id)->exists()){
            $this->realty_category_id = LocalityName::where([
                'import_internal_id' => $id,
                'region_id' => (int)$regionId
            ])->first()->id;
        } else {
            $this->realty_category_id = LocalityName::create([
                'name' => (string)$name,
                'import_internal_id' => (int)$id,
                'region_id' => (int)$regionId
            ])->id;
        }
    }
}
