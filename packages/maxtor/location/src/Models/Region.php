<?php

namespace MaxTor\Location\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\Filterable;
use MaxTor\MXTCore\Traits\UserManagement;

class Region extends Model
{
    use Cacheable,
        UserManagement,
        Sluggable,
        SoftDeletes,
        Filterable;

    protected $table = 'location_regions';

    protected $guarded = ['id'];

    public function country()
    {
        $this->hasOne(Country::class, 'id', 'country_id');
    }
}
