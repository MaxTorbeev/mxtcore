<?php

namespace MaxTor\Location\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\Filterable;
use MaxTor\MXTCore\Traits\UserManagement;

class SubLocalityName extends Model
{
    use Cacheable,
        UserManagement,
        Sluggable,
        SoftDeletes,
        Filterable;

    protected $table = 'location_sub_locality_names';

    protected $guarded = ['id'];

    public function localityName()
    {
        $this->hasOne(LocalityName::class, 'id', 'locality_name_id');
    }
}
