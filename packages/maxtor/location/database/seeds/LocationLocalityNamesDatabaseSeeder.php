<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\Services\SlugService;
use MaxTor\Location\Models\LocalityName;

class LocationLocalityNamesDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('location_locality_names')->insert([
            [
                'name' => 'Казань',
                'slug' => SlugService::createSlug(LocalityName::class, 'slug', 'Казань'),
                'region_id' => DB::table('location_regions')->where('name', 'Республика Татарстан')->first()->id,
            ]
        ]);
    }
}
