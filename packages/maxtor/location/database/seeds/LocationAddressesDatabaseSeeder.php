<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\Services\SlugService;
use MaxTor\Location\Models\Address;

class LocationAddressesDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('location_addresses')->insert([
            [
                'name' => 'Рауиса Гареева ул., 102, к 1',
                'slug' => SlugService::createSlug(Address::class, 'slug', 'Рауиса Гареева ул., 102, к 1'),
                'locality_name_id' => DB::table('location_locality_names')->where('name', 'Казань')->first()->id
            ]
        ]);
    }
}
