<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\Services\SlugService;
use MaxTor\Location\Models\Metro;

class LocationMetroDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('location_metro')->insert([
            [
                'name' => 'Авиастроительная',
                'slug' => SlugService::createSlug(Metro::class, 'slug', 'Авиастроительная'),
                'locality_name_id' => DB::table('location_locality_names')->where('name', 'Казань')->first()->id,
            ],
            [
                'name' => 'Аметьево',
                'slug' => SlugService::createSlug(Metro::class, 'slug', 'Аметьево'),
                'locality_name_id' => DB::table('location_locality_names')->where('name', 'Казань')->first()->id,
            ],
            [
                'name' => 'Горки',
                'slug' => SlugService::createSlug(Metro::class, 'slug', 'Горки'),
                'locality_name_id' => DB::table('location_locality_names')->where('name', 'Казань')->first()->id,
            ],
            [
                'name' => 'Дубравная',
                'slug' => SlugService::createSlug(Metro::class, 'slug', 'Дубравная'),
                'locality_name_id' => DB::table('location_locality_names')->where('name', 'Казань')->first()->id,
            ],
        ]);
    }
}
