<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\Services\SlugService;
use \MaxTor\Location\Models\Region;

class LocationRegionsDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('location_regions')->insert([
            [
                'name' => 'Республика Татарстан',
                'slug' => SlugService::createSlug(Region::class, 'slug', 'Республика Татарстан'),
                'country_id' => DB::table('location_countries')->where('name', 'Россия')->first()->id
            ]
        ]);
    }
}
