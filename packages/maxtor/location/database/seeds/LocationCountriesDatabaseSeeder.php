<?php

use Illuminate\Database\Seeder;
use MaxTor\Location\Models\Country;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\DB;

class LocationCountriesDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('location_countries')->insert([
            [
                'name' => 'Россия',
                'slug' => 'russia'
            ]
        ]);
    }
}
