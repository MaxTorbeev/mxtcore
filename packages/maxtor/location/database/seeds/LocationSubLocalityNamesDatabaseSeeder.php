<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\Services\SlugService;
use MaxTor\Location\Models\SubLocalityName;

class LocationSubLocalityNamesDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('location_sub_locality_names')->insert([
            [
                'name' => 'Приволжский',
                'slug' => SlugService::createSlug(SubLocalityName::class, 'slug', 'Приволжский'),
                'locality_name_id' => DB::table('location_locality_names')->where('name', 'Казань')->first()->id,
            ]
        ]);
    }
}
