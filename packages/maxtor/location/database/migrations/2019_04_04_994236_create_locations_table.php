<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateLocationTable
 *
 * Таблица локаций (местонахождение объектов недвижимости)
 */
class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('country_id');
            $table->unsignedInteger('region_id');
            $table->unsignedInteger('district_id')->nullable()->comment('Района субъекта РФ');
            $table->unsignedInteger('locality_name_id')->nullable()->comment('Населенный пункт');
            $table->unsignedInteger('sub_locality_name_id')->nullable()->comment('Район населенного пункта');
            $table->unsignedInteger('address_id')->nullable();
            $table->unsignedInteger('railway_station_id')->nullable()->comment('Ближайшая железнодорожная станция');

            $table->unsignedInteger('metro_id')->nullable();
            $table->string('metro_time_on_foot')->nullable();
            $table->string('metro_time_on_transport')->nullable();

            $table->string('latitude')->comment('Географическая широта')->nullable();
            $table->string('longitude')->comment('Географическая долгота')->nullable();

            $table->foreign('country_id')
                ->references('id')
                ->on('location_countries')->onDelete('cascade');

            $table->foreign('region_id')->references('id')
                ->on('location_regions')->onDelete('cascade');

            $table->foreign('district_id')->references('id')
                ->on('location_districts')->onDelete('cascade');

            $table->foreign('locality_name_id')->references('id')
                ->on('location_locality_names')->onDelete('cascade');

            $table->foreign('sub_locality_name_id')->references('id')
                ->on('location_sub_locality_names')->onDelete('cascade');

            $table->foreign('address_id')->references('id')
                ->on('location_addresses')->onDelete('cascade');

            $table->foreign('railway_station_id')->references('id')
                ->on('location_railway_stations')->onDelete('cascade');

            $table->foreign('metro_id')->references('id')
                ->on('location_metro')->onDelete('cascade');

            $table->userManagement();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
