<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateLocationLocalityNamesTable
 *
 * Таблица названий населенных пунктов.
 */
class CreateLocationLocalityNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_locality_names', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('slug')->nullable()->index();

            $table->unsignedInteger('region_id');
            $table->unsignedInteger('district_id')->nullable();

            $table->foreign('region_id')->references('id')
                ->on('location_regions')->onDelete('cascade');

            $table->foreign('district_id')->references('id')
                ->on('location_districts')->onDelete('cascade');

            $table->userManagement();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_locality_names');
    }
}
