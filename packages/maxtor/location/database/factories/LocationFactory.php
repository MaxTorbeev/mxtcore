<?php

use App\User;

use MaxTor\Location\Models\Country;
use MaxTor\Location\Models\Region;
use MaxTor\Location\Models\LocalityName;
use MaxTor\Location\Models\SubLocalityName;
use MaxTor\Location\Models\Address;
use MaxTor\Location\Models\District;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\MaxTor\Location\Models\Location::class, function (Faker $faker) {
    return [
        'country_id' => function () {
            return factory(Country::class)->create()->id;
        },
        'region_id' => function () {
            return factory(Region::class)->create()->id;
        },
        'district_id' => function () {
            return factory(\MaxTor\Location\Models\District::class)->create()->id;
        },
        'locality_name_id' => function () {
            return factory(LocalityName::class)->create()->id;
        },
        'sub_locality_name_id' => function () {
            return factory(SubLocalityName::class)->create()->id;
        },
        'address_id' => function () {
            return factory(Address::class)->create()->id;
        },
        'metro_id' => function () {
            return factory(\MaxTor\Location\Models\Metro::class)->create()->id;
        },
        'railway_station_id' => function () {
            return factory(\MaxTor\Location\Models\RailwayStation::class)->create()->id;
        },

        'metro_time_on_foot' => $faker->randomDigit,
        'metro_time_on_transport' => $faker->randomDigit,

        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
    ];
});