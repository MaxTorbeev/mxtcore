<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

Route::get('/', ['as' => 'frontend.realty.index', 'uses' => 'RealtyController@index']);

Route::get('/filters', [
    'as' => 'frontend.realty.filters', 'uses' => 'FiltersController@index'
]);

Route::get('/show/{realty}', [
    'as' => 'frontend.realty.show', 'uses' => 'RealtyController@show'
]);

Route::post('/show/{realty}/booking', [
    'as' => 'frontend.realty.booking', 'uses' => 'RealtyController@booking'
]);

Route::get('/{locality}', [
    'as' => 'frontend.realty.locality', 'uses' => 'RealtyController@locality'
]);

Route::get('/{locality}/{type}', [
    'as' => 'frontend.realty.type', 'uses' => 'RealtyController@type'
]);

Route::get('/{locality}/{type}/{category}', [
    'as' => 'frontend.realty.category', 'uses' => 'RealtyController@category'
]);

// Home > Realty
Breadcrumbs::for ('home.realty', function ($trail) {
    $trail->parent('home');
    $trail->push('Каталог недвижимости', route('frontend.realty.index'));
});

// Home > Realty > Locality
Breadcrumbs::for ('home.realty.locality', function ($trail, $locality) {
    $trail->parent('home.realty');
    $trail->push($locality->name, route('frontend.realty.locality', $locality));
});

// Home > Realty > Locality > Type
Breadcrumbs::for ('home.realty.locality.type', function ($trail, $locality, $type) {
    $trail->parent('home.realty.locality', $locality);
    $trail->push(capital_letter($type->name), route('frontend.realty.type', [$locality, $type]));
});

// Home > Realty > Locality > Type > Category
Breadcrumbs::for ('home.realty.locality.type.category', function ($trail, $locality, $type, $category) {
    $trail->parent('home.realty.locality.type', $locality, $type);
    $trail->push(capital_letter($category->name), route('frontend.realty.category', [$locality, $type, $category]));
});

// Home > Realty > Locality > Type > Category > Realty
Breadcrumbs::for ('home.realty.show', function ($trail, $realty) {
    $trail->parent('home.realty.locality.type.category', $realty->location->localityName, $realty->type, $realty->category);
    $trail->push(capital_letter($realty->name), route('frontend.realty.show', $realty));
});