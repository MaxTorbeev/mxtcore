<?php

Route::match(['get', 'post'], '/filters/build', [
    'as' => 'api.realty.filters.build',
    'uses' => 'FiltersController@build'
]);

