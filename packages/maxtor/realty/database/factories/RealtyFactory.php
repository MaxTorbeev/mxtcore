<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(\MaxTor\Realty\Models\Realty::class, function (Faker $faker) {
    return [
        'name' => $faker->title,
        'realty_type_id' => function () {
            return factory(\MaxTor\Realty\Models\RealtyType::class)->create()->id;
        },
        'realty_property_type_id' => function () {
            return factory(\MaxTor\Realty\Models\RealtyPropertyType::class)->create()->id;
        },
        'location_id' => function () {
            return factory(\MaxTor\Location\Models\Location::class)->create()->id;
        },
        'realty_category_id' => function () {
            return factory(\MaxTor\Realty\Models\RealtyCategory::class)->create()->id;
        },
        'realty_deal_status_id' => function () {
            return factory(\MaxTor\Realty\Models\RealtyDealStatus::class)->create()->id;
        },
        'rent_period_id' => function () {
            return factory(\MaxTor\Realty\Models\RealtyRentPeriod::class)->create()->id;
        },
        'realty_vas_id' => function () {
            return factory(\MaxTor\Realty\Models\RealtyVas::class)->create()->id;
        },
        'realty_ownership_id' => function () {
            return factory(\MaxTor\Realty\Models\RealtyOwnership::class)->create()->id;
        },
        'realty_renovation_id' => function () {
            return factory(\MaxTor\Realty\Models\RealtyRenovation::class)->create()->id;
        },
        'realty_quality_id' => function () {
            return factory(\MaxTor\Realty\Models\RealtyQuality::class)->create()->id;
        },
        'description' => $faker->text,
        'cadastral_number' => $faker->randomDigit,
        'prepayment' => $faker->boolean,
    ];
});