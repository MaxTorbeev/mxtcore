<?php

use App\User;
use Faker\Generator as Faker;
use MaxTor\Location\Models\Location;

$factory->define(\MaxTor\Realty\Models\RealtyRentPeriod::class, function (Faker $faker) {
    return [
        'name' => $faker->title,
        'permission_id' => function () {
            return factory(\MaxTor\MXTCore\Models\Permission::class)->create()->id;
        },
        'role_id' => function () {
            return factory(\MaxTor\MXTCore\Models\Role::class)->create()->id;
        },
        'locked' => false,
        'enabled' => true,
        'created_user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'modified_user_id' => function () {
            return factory(User::class)->create()->id;
        }
    ];
});