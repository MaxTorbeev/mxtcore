<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RealtyDealStatusesDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * Для значения «raise» («Поднятие») можно применить ежедневное автоподключение в определенное время.
     * Для этого внутри vas укажите атрибут start-time,
     * дату и время в формате YYYY-MM-DDTHH:mm:ss+00:00 и значение raise.
     *
     * Пример:
     * <vas start-time="2018-06-04T10:00:00+03:00">raise</vas>
     * Должна быть указана дата первого применения опции. Обновлять атрибут не нужно.
     * Объявление, к которому применена опция, будет подниматься ежедневно в указанное время.
     *
     * Опции нельзя подключить к объявлениям без фотографий.
     *
     * @return void
     */
    public function run()
    {
        DB::table('realty_deal_statuses')->insert([
            [
                'name' => 'первичная продажа',
                'slug' => 'primary-sale',
                'associations' => json_encode(['продажа от застройщика'])
            ],
            [
                'name' => 'переуступка',
                'slug' => 'reassignment',
                'associations' => json_encode(['reassignment'])
            ],
            [
                'name' => 'прямая продажа',
                'slug' => 'sale',
                'associations' => json_encode(['sale'])
            ],
            [
                'name' => 'первичная продажа вторички',
                'slug' => 'primary-sale-of-secondary',
                'associations' => json_encode(['primary sale of secondary'])
            ],
            [
                'name' => 'встречная продажа',
                'slug' => 'countersale',
                'associations' => json_encode(['countersale'])
            ],
        ]);
    }
}
