<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RealtyAreaUnitsDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('realty_area_units')->insert([
            [
                'name' => 'кв. м',
            ],
            [
                'name' => 'cотка',
            ],
            [
                'name' => 'гектар',
            ],
        ]);
    }
}
