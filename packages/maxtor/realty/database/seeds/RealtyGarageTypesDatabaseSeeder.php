<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RealtyGarageTypesDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('realty_garage_types')->insert([
            [
                'name' => 'гараж',
                'slug' => 'garage',
                'associations' => json_encode(['garage'])
            ],
            [
                'name' => 'машиноместо',
                'slug' => 'parking-place',
                'associations' => json_encode(['parking place'])
            ],
            [
                'name' => 'бокс',
                'slug' => 'box',
                'associations' => json_encode(['box'])
            ],
        ]);
    }
}
