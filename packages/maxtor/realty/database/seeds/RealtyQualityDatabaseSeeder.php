<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RealtyQualityDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('realty_quality')->insert([
            [
                'name' => 'отличное',
                'slug' => 'great'
            ],
            [
                'name' => 'хорошее',
                'slug' => 'good'
            ],
            [
                'name' => 'нормальное',
                'slug' => 'normal'
            ],
            [
                'name' => 'плохое',
                'slug' => 'bad'
            ],
        ]);
    }
}
