<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RealtyBuildingTypesDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('realty_building_types')->insert([
            [
                'name' => 'блочный',
                'slug' => 'blocky'
            ],
            [
                'name' => 'деревянный',
                'slug' => 'wooden'
            ],
            [
                'name' => 'кирпичный',
                'slug' => 'brick'
            ],
            [
                'name' => 'кирпично-монолитный',
                'slug' => 'brick-monolithic'
            ],
            [
                'name' => 'монолит',
                'slug' => 'monolith'
            ],
            [
                'name' => 'панельный',
                'slug' => 'panel'
            ],
        ]);
    }
}
