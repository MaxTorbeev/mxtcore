<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RealtyRenovationDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('realty_renovations')->insert([
            [
                'name' => 'дизайнерский',
                'slug' => 'designer'
            ],
            [
                'name' => 'евро',
                'slug' => 'euro'
            ],
            [
                'name' => 'с отделкой',
                'slug' => 'finished'
            ],
            [
                'name' => 'требует ремонта',
                'slug' => 'needs-repair'
            ],
            [
                'name' => 'хороший',
                'slug' => 'good'
            ],
            [
                'name' => 'частичный ремонт',
                'slug' => 'partial-repair'
            ],
            [
                'name' => 'черновая отделка',
                'slug' => 'rough-finish'
            ],
        ]);
    }
}
