<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RealtyTypesDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('realty_types')->insert([
            [
                'name' => 'продажа',
                'slug' => 'sale',
                'import_internal_id' => 1
            ],
            [
                'name' => 'аренда',
                'slug' => 'rent',
                'import_internal_id' => 2
            ],
        ]);
    }
}
