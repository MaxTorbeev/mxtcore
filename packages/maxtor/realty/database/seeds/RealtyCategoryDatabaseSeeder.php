<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RealtyCategoryDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('realty_categories')->insert([
            [
                'name' => 'квартира',
                'slug' => 'flats',
                'import_internal_id' => 1,
                'associations' => json_encode(['flat'])
            ],
            [
                'name' => 'комната',
                'slug' => 'rooms',
                'import_internal_id' => 2,
                'associations' => json_encode(['room'])
            ],
            [
                'name' => 'дом',
                'slug' => 'houses',
                'import_internal_id' => 3,
                'associations' => json_encode(['house'])
            ],
            [
                'name' => 'земельный участок',
                'slug' => 'land-plots',
                'import_internal_id' => 4,
                'associations' => null
            ],
            [
                'name' => 'коммерческая',
                'slug' => 'commercial',
                'import_internal_id' => 5,
                'associations' => null
            ],
            [
                'name' => 'новые квартиры',
                'slug' => 'new-flats',
                'import_internal_id' => null,
                'associations' => null
            ],
            [
                'name' => 'гараж',
                'slug' => 'garages',
                'import_internal_id' => 7,
                'associations' => null
            ],
        ]);
    }
}
