<?php

use Illuminate\Database\Seeder;

class RealtyDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RealtyTypesDatabaseSeeder::class);
         $this->call(RealtyCategoryDatabaseSeeder::class);
         $this->call(RealtyRenovationDatabaseSeeder::class);
         $this->call(RealtyRenovationDatabaseSeeder::class);
         $this->call(RealtyQualityDatabaseSeeder::class);
         $this->call(RealtyGarageTypesDatabaseSeeder::class);
         $this->call(RealtyBuildingTypesDatabaseSeeder::class);
//         $this->call(RealtyAreaUnitsDatabaseSeeder::class);
    }
}
