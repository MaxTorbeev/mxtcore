<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealtyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('realty', function (Blueprint $table) {
            $table->increments('id');

            $table->string('import_internal_id')
                ->nullable()
                ->index()
                ->comment('Внутренний индетификатор импортированного объекта');

            $table->string('name')->nullable();
            $table->string('title')->nullable();

            $table->text('address')
                ->nullable()
                ->comment('Адресс объекта');

            $table->string('area')
                ->nullable()
                ->comment('Площадь объекта');

            $table->unsignedInteger('realty_type_id');
            $table->unsignedInteger('realty_property_type_id');
            $table->unsignedInteger('realty_category_id');
            $table->unsignedInteger('location_id');
            $table->unsignedInteger('realty_deal_status_id')->nullable();
            $table->unsignedInteger('rent_period_id')
                ->nullable()
                ->comment('Период для расчета стоимости аренды');
            $table->unsignedInteger('realty_vas_id')
                ->nullable()
                ->comment('Дополнительная опция по продвижению объявления');
            $table->unsignedInteger('realty_renovation_id')
                ->nullable()
                ->comment('Ремонт');
            $table->unsignedInteger('realty_quality_id')
                ->nullable()
                ->comment('Состояние объекта');
            $table->unsignedInteger('realty_ownership_id')
                ->nullable()
                ->comment('Статус собственности');

            $table->text('description')->nullable()->comment('Описание объекта недвижимости');
            $table->string('cadastral_number')->nullable()->comment('Кадастровый номер');

            $table->decimal('prepayment', 3, 2)->nullable()->default(0)->comment('Предоплата. Указывается числовое значение в процентах без знака «%»');
            $table->decimal('agent_fee', 3, 2)->default(0)->comment('Комиссия агента. Указывается числовое значение в процентах без знака «%». Максимальное значение — 100');
            $table->boolean('haggle')->nullable()->comment('Возможен торг');
            $table->boolean('mortgage')->nullable()->comment('Возможна ипотека');

            $table->boolean('not_for_agents')
                ->nullable()
                ->comment('Пометка «Просьба агентам не звонить»');

            $table->boolean('utilities_included')
                ->nullable()
                ->comment('Коммунальные услуги включены в стоимость в договоре аренды');

            $table->boolean('rent_pledge')
                ->nullable()
                ->comment('Арендный залог');

            $table->unsignedInteger('agent_user_id')
                ->nullable()
                ->comment('Индетификатор пользователя-агента');

            /** @see  https://yandex.ru/support/realty/requirements/requirements-sale-housing.html#concept8 */
            $table->json('dwelling_description')->nullable()->comment('Описание жилого помещения');
            $table->smallInteger('rooms')->nullable()->comment('Общее количество комнат в квартире');
            $table->smallInteger('rooms_offered')->nullable()->comment('Количество комнат, участвующих в сделке');
            $table->smallInteger('floor')->nullable()->comment('Этаж');

            /** @see https://yandex.ru/support/realty/requirements/requirements-sale-housing.html#concept9 */
            $table->json('building_description')->nullable()->comment('Описание здания');
            $table->unsignedInteger('realty_building_type_id')
                ->nullable()
                ->comment('Тип дома');
            $table->smallInteger('floors_total')->nullable()->comment('Общее количество этажей в доме');
            $table->string('built_year')->nullable()->comment('Год сдачи (год постройки)');

            /** @see https://yandex.ru/support/realty/requirements/requirements-sale-housing.html#garage */
            $table->json('garage_description')->nullable()->comment('Описание гаража');
            $table->unsignedInteger('garage_type_id')->nullable()->comment('Категория гаража');

            $table->foreign('realty_type_id')
                ->references('id')
                ->on('realty_types')->onDelete('cascade');

            $table->foreign('realty_property_type_id')
                ->references('id')
                ->on('realty_property_types')->onDelete('cascade');

            $table->foreign('realty_category_id')
                ->references('id')
                ->on('realty_categories')->onDelete('cascade');

            $table->foreign('location_id')
                ->references('id')
                ->on('locations')->onDelete('cascade');

            $table->foreign('realty_deal_status_id')
                ->references('id')
                ->on('realty_deal_statuses')->onDelete('cascade');

            $table->foreign('realty_vas_id')
                ->references('id')
                ->on('realty_vas')->onDelete('cascade');

            $table->foreign('rent_period_id')
                ->references('id')
                ->on('realty_rent_periods')->onDelete('cascade');

            $table->foreign('realty_renovation_id')
                ->references('id')
                ->on('realty_renovations')->onDelete('cascade');

            $table->foreign('realty_quality_id')
                ->references('id')
                ->on('realty_quality')->onDelete('cascade');

            $table->foreign('realty_ownership_id')
                ->references('id')
                ->on('realty_ownerships')->onDelete('cascade');

            $table->foreign('agent_user_id')
                ->references('id')
                ->on('users')->onDelete('cascade');

            $table->foreign('realty_building_type_id')
                ->references('id')
                ->on('realty_building_types')->onDelete('cascade');

            $table->foreign('garage_type_id')
                ->references('id')
                ->on('realty_garage_types')->onDelete('cascade');

            $table->seo();
            $table->userManagement();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('realty_price', function (Blueprint $table) {

            $table->unsignedInteger('realty_id');
            $table->foreign('realty_id')
                ->references('id')
                ->on('realty')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedInteger('price_id');
            $table->foreign('price_id')
                ->references('id')
                ->on('prices')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::create('realty_photo', function (Blueprint $table) {

            $table->unsignedInteger('realty_id');
            $table->foreign('realty_id')
                ->references('id')
                ->on('realty')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedInteger('photo_id');
            $table->foreign('photo_id')
                ->references('id')
                ->on('photos')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::create('realty_area', function (Blueprint $table) {

            $table->unsignedInteger('realty_id');
            $table->foreign('realty_id')
                ->references('id')
                ->on('realty')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedInteger('area_id');
            $table->foreign('area_id')
                ->references('id')
                ->on('realty_areas')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('realty');
        Schema::dropIfExists('realty_price');
    }
}
