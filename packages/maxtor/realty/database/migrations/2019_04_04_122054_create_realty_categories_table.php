<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealtyCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('realty_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_internal_id')->nullable()->index();
            $table->string('name');
            $table->string('slug')->index();
            $table->json('associations')->nullable();

            $table->seo();
            $table->userManagement();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('realty_categories');
    }
}
