@extends('layouts.app')

@section('head-meta')
    <title>Каталог недвижимости</title>
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('home.realty') }}
@endsection

@section('top')
    @widget('realtyFiltersForms')
@endsection

@section('content')
    <section class="realty realty_list">
        <h1 class="page_title">
            Каталог недвижимости
        </h1>

        <div class="alert alert-success">
            {{ trans_choice('realty.search.results', $realty->total()) }}
        </div>

        <div class="mb-3">
            На странице:
            @foreach(config('realty.limitsOnPage') as $limit)
                <a href="{{ route_params_merge('frontend.realty.filters', ['limit' => $limit ]) }}"
                   class="btn btn-sm btn-outline-dark {{ request('limit') == $limit ? 'active' : '' }}"
                >
                    {{ $limit }}
                </a>
            @endforeach
        </div>

        <div class="row">
            @foreach($realty as $item)
                <div class="col-xl-2 col-md-3 col-6 mb-4">
                    @include('realty::components.realty.card', ['item' => $item])
                </div>
            @endforeach
        </div>

        {{ $realty->appends(request()->input())->links() }}
    </section>
@endsection
