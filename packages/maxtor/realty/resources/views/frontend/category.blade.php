@extends('layouts.app')

@section('head-meta')
    <title>Недвижимость в {{ $locality->name }}</title>
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('home.realty.locality.type.category', $locality, $type, $category) }}
@endsection

@section('content')
    <section class="realty realty_list">
        <h1 class="page_title">
            Недвижимость в {{ $locality->name }}
        </h1>
        <div class="row">
            @foreach($realty as $item)
                <div class="col-xl-2 col-md-3 col-sm-6 mb-4">
                    @include('realty::components.realty.card', ['item' => $item])
                </div>
            @endforeach
        </div>
        {{ $realty->links() }}
    </section>
@endsection
