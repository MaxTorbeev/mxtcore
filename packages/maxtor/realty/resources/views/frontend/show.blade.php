@extends('layouts.app')

@section('head-meta')
    <title>
        {{ capital_letter($realty->type->name) }}, {{ $realty->name }} - {{site_config('site_name')->value}}
    </title>
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('home.realty.show', $realty) }}
@endsection

@section('content')
    <article class="realty realty_show">
        <div class="row">
            <div class="col-md-8">
                @include('content::components.gallery', ['item' => $realty])
            </div>

            <div class="col-md-4">
                <div class="card card-withShadow">
                    <div class="card-body">
                        <div class="price price-small price-mute">
                            ~
                            @money($realty->getPrice('cost-per-unit')->value,$realty->getPrice('cost-per-unit')->currency->name)
                        </div>
                        <div class="price price-large">
                            @money($realty->getPrice()->value,$realty->getPrice()->currency->name)
                        </div>

                        <div class="realty_fullAddress mb-2">
                            {{ $realty->address }}
                        </div>

                        @include('realty::components.realty.summary', [
                            'item' => $realty,
                            'amountOfSummary' => 'medium'
                        ])
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9">

                <h1 class="h2 page_title my-4">
                    {{ capital_letter($realty->type->name) }}, {{ $realty->name }}
                </h1>

                {!! $realty->description !!}

                <div class="yandex-map"
                     style="height: 400px"
                     data-map-height="400"
                     data-zoom="12"
                     data-map-text="{{ $realty->name }}"
                     data-map-coordinate="{{$realty->location->latitude}},{{$realty->location->longitude}}">
                </div>

                @include('realty::components.realty.summary', [
                    'item' => $realty,
                    'amountOfSummary' => 'large'
                ])
            </div>

{{--            @isset($realty->agentUser->contact)--}}

                <div class="col-md-3">
                    @include('personal::components.contact.card', [
                        'title' => 'Ваш риелтор',
                        'item' => $realty->agentUser->contact,
                    ])

                    <a href="#"
                       class="btn btn-outline-primary btn-sm btn-block mt-4"
                       onclick='event.preventDefault(); modalShow("back-call",  @json(["apiUrl" => route("frontend.realty.booking", $realty)]))'
                    >Бронировать</a>
                </div>
{{--            @endisset--}}
        </div>
    </article>
@endsection
