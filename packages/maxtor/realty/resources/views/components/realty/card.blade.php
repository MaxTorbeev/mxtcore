@inject(summaryWidget, 'MaxTor\Realty\Widgets\SummaryWidget')

<div class="realty_card card card-withShadow">
    <a href="{{ route('frontend.realty.show', $item) }}" title="{{ $item->name }}">
        @isset($item->photo_preview)
            <img class="card-img-top" src="{{ $item->photo_preview->getThumb(262, 190) }}" alt="{{ $item->name }}">
        @else
            <img class="card-img-top" src="{{ config('realty.no-photo') }}" alt="{{ $item->name }}">
        @endif
    </a>

    <div class="card-body">
        <h5 class="card-title">
            <a href="{{ route('frontend.realty.show', $item) }}" title="{{ $item->name }}">
                {{ capital_letter($item->name, ',') }}
            </a>
        </h5>
        <div class="realty_card_price">
            @isset($item->getPrice()->value)
                @money($item->getPrice()->value, $item->getPrice()->currency->name)
            @endisset
        </div>
        <div class="realty_card_address">
            {{ $item->address }}
        </div>
        <div class="realty_summary mt-2">
            {!! $summaryWidget->handle([
                'item' => $item,
                'cacheKey' => 'realty.summary.' , $item->id
            ]) !!}
        </div>
        <div class="realty_card_footer">
            <a href="#"
               class="btn btn-outline-dark btn-sm btn-block"
               onclick='event.preventDefault(); modalShow("back-call",  @json(["apiUrl" => route("frontend.realty.booking", $item)]))'
            >Бронировать</a>
        </div>
    </div>
</div>
