<ul class="realty_summary-list realty_summary-small text-muted">
    @if($item->area && !isset($amountOfSummary))
        <li class="realty_summary_row">
            <div class="realty_summary_title">
                @isset($item->area_model->type)
                    @lang('realty.areas.' . $item->area_model->type):
                @endisset
            </div>
            <div class="realty_summary_value">
                {{ $item->area }}
            </div>
        </li>
    @endif

    @if($item->type()->first()->slug === 'sale' && $item->area)
        <li class="realty_summary_row">
            <div class="realty_summary_title">
                @isset($item->area_model->unit->name)
                    Цена за {{trans_choice('realty.areas.units.' . $item->area_model->unit->name, -1)}}:
                @endisset
            </div>
            <div class="realty_summary_value">


                @isset($item->getPrice('cost-per-unit')->value)
                    @money($item->getPrice('cost-per-unit')->value,$item->getPrice('cost-per-unit')->currency->name)
                @endisset
            </div>
        </li>
    @endif

    @if($item->floor > 0 && $item->floors_total > 0)
        <li class="realty_summary_row">
            <div class="realty_summary_title">Этаж/Этажность:</div>
            <div class="realty_summary_value">{{ $item->floor }} / {{ $item->floors_total }}</div>
        </li>
    @elseif ($item->floors_total > 0)
        <li class="realty_summary_row">
            <div class="realty_summary_title">Этажность:</div>
            <div class="realty_summary_value">{{ $item->floors_total }}</div>
        </li>
    @endif

    @if($item->garage_type_id !== null)
        <li class="realty_summary_row">
            <div class="realty_summary_title">Категория гаража:</div>
            <div class="realty_summary_value">{{ $item->garageType->name }}</div>
        </li>
    @endif

    @isset($amountOfSummary)
        @if($amountOfSummary === 'medium' || $amountOfSummary === 'large')
            @if($item->rooms)
                <li class="realty_summary_row">
                    <div class="realty_summary_title">Количество комнат:</div>
                    <div class="realty_summary_value">{{ $item->rooms }}</div>
                </li>
            @endif
            @if($item->areas)
                @foreach($item->areas as $area)
                    <li class="realty_summary_row">
                        <div class="realty_summary_title">@lang('realty.areas.' . $area->type):</div>
                        <div class="realty_summary_value"> {{ trans_choice('realty.areas.units-only.' . $area->unit->name, $area->value) }}</div>
                    </li>
                @endforeach
            @endif
            @isset($item->buildingType->name)
                <li class="realty_summary_row">
                    <div class="realty_summary_title">Здание:</div>
                    <div class="realty_summary_value">{{ capital_letter($item->buildingType->name) }}</div>
                </li>
            @endisset
            @if($item->built_year)
                <li class="realty_summary_row">
                    <div class="realty_summary_title">Год сдачи:</div>
                    <div class="realty_summary_value">{{ $item->built_year }}</div>
                </li>
            @endif
            @isset($item->location->metro->name)
                <li class="realty_summary_row">
                    <div class="realty_summary_title">Метро:</div>
                    <div class="realty_summary_value">
                        {{ $item->location->metro->name }}
                    </div>
                </li>
            @endisset
        @endif

        @if($amountOfSummary === 'large')
            @foreach($item->dwelling_description as $ddKey => $dwelling_description)
                @if($dwelling_description != null)
                    <li class="realty_summary_row">
                        <div class="realty_summary_title">@lang('realty.dwelling_description.' . $ddKey):</div>
                        <div class="realty_summary_value">{{ capital_letter($dwelling_description) }}</div>
                    </li>
                @endif
            @endforeach
            @foreach($item->building_description as $bdKey => $building_description)
                @if($building_description != null)
                    <li class="realty_summary_row">
                        <div class="realty_summary_title">@lang('realty.building_description.' . $bdKey):</div>
                        <div class="realty_summary_value">{{ capital_letter($building_description) }}</div>
                    </li>
                @endif
            @endforeach
            @foreach($item->garage_description as $gdKey => $garage_description)
                @if($garage_description)
                    <li class="realty_summary_row">
                        <div class="realty_summary_title">@lang('realty.garage_description.' . $gdKey):</div>
                        <div class="realty_summary_value">{{ capital_letter($garage_description) }}</div>
                    </li>
                @endif
            @endforeach
        @endif
    @endisset
</ul>
