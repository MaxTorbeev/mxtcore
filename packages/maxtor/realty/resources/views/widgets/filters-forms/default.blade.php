<div
    class="realty_filter {{ request()->route()->getName() == 'home' ? 'realty_filter-withTop' : 'mt-3' }} realty_filter_form ">
    <form action="{{ route('frontend.realty.filters') }}" method="GET" class="form">
        <div class="container">
            <div class="card card-withShadow">
                <ul class="nav card-header-tabs realty_filter_categories realty_filter_categories-tabs">
                    <li class="nav-item">
                        <a href="{{ route_params_merge('frontend.realty.filters', ['category' => null ]) }}"
                           class="nav-link {{ is_null(request('category'))  ? 'active' : '' }}">
                            Все объекты
                        </a>
                    </li>
                    @foreach($formData['categories'] as $category)
                        <li class="nav-item">
                            <a href="{{ route_params_merge('frontend.realty.filters', ['category' => $category->id]) }}"
                               class="nav-link {{ (!is_null(request('category')) && $formData['currentCategory']->id == $category->id) ? 'active' : '' }}">
                                {{ $category->name  }}
                            </a>
                        </li>
                    @endforeach
                    <li class="nav-item">
                        <a href="http://xn----7sbajnkidicdf6aepba9akiv.xn--p1ai/" target="_blank" rel="nofollow" class="nav-link btn btn-block btn-warning">
                            <b>Новостройки</b>
                        </a>
                    </li>
                </ul>

                {{--<a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">--}}
                {{--Collapse--}}
                {{--</a>--}}

                <div class="card-body">
                    <div class="form-row">
                        <div class="col-md-2 col-xl-1">
                            @foreach($formData['types'] as $type)
                                <a href="{{ route_params_merge('frontend.realty.filters', ['type' => $type->id ]) }}"
                                   class="btn btn-block btn-sm {{ request('type') == $type->id ? 'btn-primary' : 'btn-outline-primary' }}"
                                >
                                    {{ $type->name }}
                                </a>
                            @endforeach
                        </div>
                        <div class="col-md-10 col-xl-11 mb-2">
                            @if (view()->exists("realty::widgets.filters-forms.forms.{$formData['currentCategory']->slug}"))
                                @include("realty::widgets.filters-forms.forms.{$formData['currentCategory']->slug}",compact('formData'))
                            @else
                                @include("realty::widgets.filters-forms.forms.default", compact('formData'))
                            @endif
                        </div>
                    </div>

                    <input type="hidden" name="category" value="{{ request('category') }}">
                    <input type="hidden" name="type" value="{{ request('type') ? request('type') : 1 }}">
                    <input type="submit" class="btn btn-primary-withShadow pull-right" value="Найти">
                </div>
            </div>
        </div>
    </form>
</div>

