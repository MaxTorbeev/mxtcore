<div class="form-row">
    <div class="form-group col-md-4">
        <label for="district">Район</label>
        <select name="district[]" id="district" class="form-control select2" multiple>
            <option>Город / район</option>
            @foreach($formData['districts'] as $district)
                <option value="{{ $district->id }}"
                @if(request()->has('district'))
                    {{ in_array($district->id, request('district')) ? 'selected' : '' }}
                        @endif
                >
                    {{ $district->name }}
                </option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-2">
        <label for="rooms">Количество комнат</label>
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            @foreach($formData['rooms'] as $room)
                <label class="btn btn-secondary {{ request('rooms') == $room ? 'active' : ''}}">
                    <input type="radio" id="rooms" name="rooms" value="{{$room}}" {{ request('rooms') == $room ? 'checked' : ''}}>
                    {{ $room }}
                </label>
            @endforeach
        </div>
    </div>

    <div class="form-group col-md-3">
        <label for="district">Тип строения</label>
        <select name="buildingType[]" id="buildingType" class="form-control select2" multiple>
            @foreach($formData['buildingTypes'] as $buildingType)
                <option value="{{ $buildingType->id }}"
                        {{ request()->has('buildingType')
                                ?  in_array($buildingType->id, request('buildingType')) ? 'selected' : ''
                                : ''
                        }}
                >
                    {{ $buildingType->name }}
                </option>
            @endforeach
        </select>
    </div>
</div>