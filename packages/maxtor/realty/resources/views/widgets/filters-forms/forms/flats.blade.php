<div class="form-row">
    <div class="form-group col-md-3 col-xl-4">
        <label for="district">Район</label>
        <select name="district[]" id="district" class="form-control select2" multiple>
            <option>Город / район</option>
            @foreach($formData['districts'] as $district)
                <option value="{{ $district->id }}"
                @if(request()->has('district'))
                    {{ in_array($district->id, request('district')) ? 'selected' : '' }}
                        @endif
                >
                    {{ $district->name }}
                </option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-3 col-xl-2">
        <label for="rooms">Количество комнат</label>
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            @foreach($formData['rooms'] as $room)
                <label class="btn btn-secondary {{ request('rooms') == $room ? 'active' : ''}}">
                    <input type="radio" id="rooms" name="rooms" value="{{$room}}" {{ request('rooms') == $room ? 'checked' : ''}}>
                    {{ $room }}
                </label>
            @endforeach
        </div>
    </div>

    <div class="form-group col-md-2">
        <label for="district">Тип строения</label>
        <select name="buildingType[]" id="buildingType" class="form-control select2" multiple>
            @foreach($formData['buildingTypes'] as $buildingType)
                <option value="{{ $buildingType->id }}"
                        {{ request()->has('buildingType')
                                ?  in_array($buildingType->id, request('buildingType')) ? 'selected' : ''
                                : ''
                        }}
                >
                    {{ $buildingType->name }}
                </option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-2">
        <label for="rooms">Стоимость</label>
        <div class="form-row align-items-center">
            <div class="col-6">
                <label class="sr-only" for="costs_from">Стоимость от</label>
                <input name="costs[from]" value="{{ request('costs')['from'] ?? null }}" type="text" id="costs_from" class="form-control mb-2" placeholder="От">
            </div>
            <div class="col-6">
                <label class="sr-only" for="costs_to">Стоимость до</label>
                <input name="costs[to]" value="{{ request('costs')['to'] ?? null }}" type="text" id="costs_to" class="form-control mb-2" placeholder="До">
            </div>
        </div>
    </div>

    <div class="form-group col-md-2">
        <label for="rooms">Площадь</label>
        <div class="form-row align-items-center">
            <div class="col-6">
                <label class="sr-only" for="areas_from">Площадь от</label>
                <input name="areas[from]" value="{{ request('areas')['from'] ?? null }}" type="text" id="costs_from" class="form-control mb-2" placeholder="От">
            </div>
            <div class="col-6">
                <label class="sr-only" for="areas_to">Площадь до</label>
                <input name="areas[to]" value="{{ request('areas')['to'] ?? null }}" type="text" id="costs_to" class="form-control mb-2" placeholder="До">
            </div>
        </div>
    </div>
</div>
<div class="form-row">
{{--    <div class="form-group col-md-4">--}}
{{--        <label class="sr-only" for="floor">Этаж</label>--}}
{{--        <div class="input-group">--}}
{{--            <div class="input-group-prepend">--}}
{{--                <div class="input-group-text">Этаж</div>--}}
{{--            </div>--}}
{{--            <select name="floor" id="floor" class="form-control select2">--}}
{{--                @foreach($formData['floor'] as $keyFloor => $floor)--}}
{{--                    <option value="{{ $keyFloor }}"--}}
{{--                            --}}{{--@if(request()->has('floor'))--}}
{{--                            --}}{{--{{ in_array($keyFloor, request('floor')) ? 'selected' : '' }}--}}
{{--                            --}}{{--@endif--}}
{{--                    >--}}
{{--                        {{ $floor }}--}}
{{--                    </option>--}}
{{--                @endforeach--}}
{{--            </select>--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class="form-group col-md-3">
        <label class="sr-only" for="floor">Возраст до</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">Возраст до</div>
            </div>
            <input type="text" name="age" class="form-control" value="{{ request('floor') }}">
            <div class="input-group-prepend">
                <div class="input-group-text">лет</div>
            </div>
        </div>
    </div>
</div>
