<?php

namespace MaxTor\Realty\Filters;

use Illuminate\Support\Facades\DB;
use MaxTor\MXTCore\Filters\Filters;

class RealtyRangesFilters extends Filters
{
    public function getFieldRanges()
    {
        $builder = DB::table('realty')
            ->select(
                DB::raw('MIN(`prices`.`value`) as `min_price`, MAX(`prices`.`value`) as `max_price`'),
                DB::raw('MIN(`realty_areas`.`value`) as `min_area`, MAX(`realty_areas`.`value`) as `max_area`')
            )
            ->join('realty_price', 'realty.id', '=', 'realty_price.realty_id')
            ->join('prices', 'prices.id', '=', 'realty_price.price_id')
            ->join('realty_area', 'realty.id', '=', 'realty_area.realty_id')
            ->join('realty_areas', 'realty_areas.id', '=', 'realty_area.area_id')
            ->where('prices.price_type_id', 1);

        if ($this->request->has('metro')) {
            $builder->join('locations', 'realty.location_id', '=', 'locations.id')
                ->join('location_metro', 'locations.metro_id', '=', 'location_metro.id')
                ->where(['location_metro.id' => (int)$this->request->get('metro')])
                ->orWhere(['location_metro.name' => (string)$this->request->get('metro')]);
        }

        if ($this->request->has('locality')) {
            $locationAlias = rand(2, 15) . 'location';
            $localityAlias = rand(2, 15) . 'locality';

            $builder->join('locations as ' . $locationAlias, 'realty.location_id', '=', $locationAlias . '.id')
                ->join('location_locality_names as ' . $localityAlias, $locationAlias . '.locality_name_id', '=', $localityAlias . '.id')
                ->where([$localityAlias . '.id' => (int)$this->request->has('locality')]);
        }


        return $builder;
    }
}