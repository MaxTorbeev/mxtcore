<?php

namespace MaxTor\Realty\Filters;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use MaxTor\MXTCore\Filters\Filters;

class RealtyFilters extends Filters
{
    const FLOORS_FILTERS = [
        'Не первый',
        'Не последний',
        'Не первый и не последний',
    ];

    protected $filters = [
        'areas',
        'floor',
        'rooms',
        'buildingType',
        'category',
        'costs',
        'district',
        'type',
        'locality',
        'metro',
    ];

    public function locality($id)
    {
        $locationAlias = rand(2, 15) . '_location';
        $localityAlias = rand(2, 15) . '_locality';

        return $this->builder
            ->join('locations as ' . $locationAlias, 'realty.location_id', '=', $locationAlias . '.id')
            ->join('location_locality_names as ' . $localityAlias, $locationAlias . '.locality_name_id', '=', $localityAlias . '.id')
            ->where([$localityAlias . '.id' => (int)$id]);
    }

    public function district($ids)
    {
        $this->builder->getQuery()->orders = [];

        return $this->builder
            ->select('realty.*')
            ->join('locations as location', 'realty.location_id', '=', 'location.id')
            ->whereIn('location.district_id', $ids);
    }

    public function floor($floor)
    {
        $this->builder->getQuery()->orders = [];

        $builder = $this->builder->select('realty.*');

        // Не первый
        if($floor === '0'){
            return $builder->where('floor', '>', 1);
        }

        if($floor === '1'){
            return $builder->where('floor', '<', 'floor_total');
        }

        return $builder
            ->where('floor', '>', 1)
            ->where('floor', '<', 'floor_total');
    }

    public function metro($metro)
    {
        return $this->builder
            ->select('realty.*')
            ->join('locations', 'realty.location_id', '=', 'locations.id')
            ->join('location_metro', 'locations.metro_id', '=', 'location_metro.id')
            ->where(['location_metro.id' => (int)$metro])
            ->orWhere(['location_metro.name' => (string)$metro]);
    }

    public function type($id)
    {
        return $this->builder
            ->select('realty.*')
            ->where('realty_type_id', $id);
    }

    public function buildingType($ids)
    {
        return $this->builder->whereIn('realty_building_type_id', $ids);
    }

    public function category($id)
    {
        if (is_null($id))
            return $this->builder;

        return $this->builder->where('realty_category_id', $id);
    }

    public function rooms($rooms)
    {
        if($rooms === '5+'){
            return $this->builder->where('rooms', '>=', intval($rooms));
        }

        return $this->builder->where('rooms', $rooms);
    }

    public function areas($areas)
    {
        $this->builder->getQuery()->orders = [];

        $realtyAreaAlias = rand(265, 1000) . '_realty_area';
        $realtyAreasAlias = rand(265, 1000) . '_realty_areas';

        $builder = $this->builder
            ->join('realty_area AS ' . $realtyAreaAlias, 'realty.id', '=', $realtyAreaAlias . '.realty_id')
            ->join('realty_areas AS ' . $realtyAreasAlias, function ($join) use ($realtyAreaAlias, $realtyAreasAlias) {
                $join->on($realtyAreasAlias . '.id', '=', $realtyAreaAlias . '.area_id')->where($realtyAreasAlias . '.type', 'area');
            });

        if ($areas['to'] === null) {
            $builder->where($realtyAreasAlias . '.value', '>', floatval($areas['to']));
        } else {
            $builder->whereBetween($realtyAreasAlias . '.value', [floatval($areas['from']) ?? 0, floatval($areas['to']) ?? 0]);
        }

        return $builder;
    }

    public function costs($costs)
    {
        $this->builder->getQuery()->orders = [];

        $realtyPriceAlias = rand(2, 15) . '_realty_price';
        $pricesAlias = rand(2, 15) . '_price';

        $builder = $this->builder
            ->select($pricesAlias . '.value', 'realty.*')
            ->join('realty_price AS ' . $realtyPriceAlias, 'realty.id', '=', $realtyPriceAlias . '.realty_id')
            ->join('prices AS ' . $pricesAlias, $pricesAlias . '.id', '=', $realtyPriceAlias . '.price_id')
            ->where($pricesAlias . '.price_type_id', 1);

        if ($costs['to'] === null) {
            $builder->where($pricesAlias . '.value', '>', floatval($costs['to']));
        } else {
            $builder->whereBetween($pricesAlias . '.value', [floatval($costs['from']) ?? 0, floatval($costs['to']) ?? 0]);
        }

        return $builder;
    }

//    public function getFieldRanges()
//    {
//        $this->builder->getQuery()->orders = [];
//
//        $realtyPriceAlias = rand(265, 1000) . '_realty_price';
//        $pricesAlias = rand(265, 1000) . '_price';
//
//        $realtyAreaAlias = rand(265, 1000) . '_realty_area';
//        $realtyAreasAlias = rand(265, 1000) . '_realty_areas';
//
//        return $this->builder
//            ->select(
//                DB::raw('MIN(`' . $pricesAlias . '`.`value`) as `min_price`, MAX(`' . $pricesAlias . '`.`value`) as `max_price`')
//                , DB::raw('MIN(`' . $realtyAreasAlias . '`.`value`) as `min_area`, MAX(`' . $realtyAreasAlias . '`.`value`) as `max_area`')
//            )
//            ->leftJoin('realty_price AS ' . $realtyPriceAlias, 'realty.id', '=', $realtyPriceAlias . '.realty_id')
//            ->leftJoin('prices AS ' . $pricesAlias, $pricesAlias . '.id', '=', $realtyPriceAlias . '.price_id')
//            ->where($pricesAlias . '.price_type_id', 1)
//            ->leftJoin('realty_area AS ' . $realtyAreaAlias, 'realty.id', '=', $realtyAreaAlias . '.realty_id')
//            ->leftJoin('realty_areas AS ' . $realtyAreasAlias, function ($join) use ($realtyAreaAlias, $realtyAreasAlias) {
//                $join->on($realtyAreasAlias . '.id', '=', $realtyAreaAlias . '.area_id')->where($realtyAreasAlias . '.type', 'area');
//            });
//    }
}
