<?php

namespace MaxTor\Realty\Commands;

use Illuminate\Console\Command;
use MaxTor\Realty\Models\Realty;
use MaxTor\Realty\Services\Import\YRLImport;

class RealtyImportCommand extends Command
{

    /**
     * The console command description.
     */
    protected $description = 'Import realty catalog from url.';

    protected $signature = 'realty:import {url}';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(YRLImport $import)
    {
        $url =  $this->argument('url');

        $this->info('Начинаем загрузку с источника: ' . $url);

        $import->setUrl($url);
        $import->setFeeds();

        Realty::query()->update(['enabled' => false]);
        $this->info('Все объекты были отключены');

        foreach ($import->getFeeds() as $feed){
            try{
                if($realty = $import::recordFeedToDatabase($feed)){
                    $this->info('Добавлен: feed-id: ' . $feed->attributes()->{'internal-id'});
                }
            } catch (\Exception $e){
                $this->table(['feed id', 'line', 'file', 'message'], [[$feed->attributes()->{'internal-id'}, $e->getLine(), $e->getFile(), $e->getMessage()]]);
            }
        }
    }
}
