<?php

namespace MaxTor\Realty;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use MaxTor\Realty\Commands\RealtyImportCommand;

class RealtyServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                RealtyImportCommand::class
            ]);
        }

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'realty');

        $this->publishes([__DIR__ . '/../../public' => public_path('packages/maxtor/realty')]);

        //Migration
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        //Factories
        $this->app->make(EloquentFactory::class)->load(__DIR__ . '/../database/factories/');

        //Routes
        $this->mapApiRoutes();
        $this->mapAdminRoutes();
        $this->mapFrontendRoutes();

        Blade::component('realty::components.realty.card', 'realtyCard');

    }

    protected function mapFrontendRoutes(): void
    {
        Route::prefix('/realty/')
            ->middleware('web')
            ->namespace('MaxTor\Realty\Controllers\Frontend')
            ->group(__DIR__ . '/../routers/frontend_routes.php');
    }

    protected function mapApiRoutes(): void
    {
        Route::prefix('/api/realty/')
            ->middleware('api')
            ->namespace('MaxTor\Realty\Controllers\Api')
            ->group(__DIR__ . '/../routers/api_routes.php');
    }

    protected function mapAdminRoutes(): void
    {
        Route::prefix('/admin/realty/')
            ->namespace('MaxTor\Realty\Controllers\Api')
            ->group(__DIR__ . '/../routers/api_routes.php');
    }
}
