<?php

namespace MaxTor\Realty\Widgets;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use MaxTor\Location\Models\District;
use MaxTor\Location\Models\LocalityName;
use MaxTor\MXTCore\Widgets\Widget;
use MaxTor\Realty\Filters\RealtyFilters;
use MaxTor\Realty\Models\RealtyBuildingType;
use MaxTor\Realty\Models\RealtyCategory;
use MaxTor\Realty\Models\RealtyType;

class FiltersFormsWidget extends Widget
{
    public $packageName = 'realty';

    public $cacheKey = 'widgets::realty.filters.forms';

    public function handle($attributes = null)
    {
        $view = $attributes['view'] ?? 'default';

        $formData = [
            'types' => $this->getTypes(),
            'categories' => $this->getCategories(),
            'currentCategory' => $this->getCurrentCategory(),
            'districts' => $this->getDistricts(),
            'locality' => $this->getLocality(),
            'rooms' => ['1', '2', '3', '4', '5+'],
            'floor' => RealtyFilters::FLOORS_FILTERS,
            'buildingTypes' => $this->getBuildingTypes()
        ];
        
		return $this->view($view, [
			'formData' => $formData
		]);
    }

    public function getDistricts()
    {
        return District::enabled()->select('id', 'slug', 'name')->get();
    }

    public function getTypes()
    {
        return RealtyType::enabled()->select('id', 'slug', 'name')->get();
    }

    public function getCategories()
    {
        return RealtyCategory::select('id', 'slug', 'name')
            ->withCount(['realty'])
            ->has('realty', '>', 0)
            ->get();
    }

    public function getLocality()
    {
        return LocalityName::enabled()->select('id', 'slug', 'name')->get();
    }

    public function getBuildingTypes()
    {
        return RealtyBuildingType::select('id', 'slug', 'name')
            ->withCount(['realty'])
            ->has('realty', '>', 0)
            ->get();
    }

    protected function getCurrentCategory()
    {
        $categoryId = is_null(request('category')) ? 1 : request('category');

        return RealtyCategory::where('id', $categoryId)->first();
    }

}