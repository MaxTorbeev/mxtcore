<?php

namespace MaxTor\Realty\Widgets;

use MaxTor\MXTCore\Widgets\Widget;

class SummaryWidget extends Widget
{
    public $packageName = 'realty';

    public function handle($attributes = null)
    {
        $item = $attributes['item'] ?? null;

        $this->cacheKey = $attributes['cacheKey'] ?? $this->cacheKey;

        if(!is_null($item)){
//            return \Cache::remember($this->cacheKey, $this->cacheTime, function () use ($item) {
                return $this->view('realty::components.realty.summary', ['item' => $item]);
//            });
        }
    }
}
