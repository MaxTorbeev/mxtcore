<?php

namespace MaxTor\Realty\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\Content\Traits\RecordsPhoto;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\Filterable;
use MaxTor\MXTCore\Traits\UserManagement;

class RealtyArea extends Model
{
    protected $table = 'realty_areas';

    protected $with = ['unit'];

    protected $guarded = ['id'];

    public function unit()
    {
        return $this->hasOne(RealtyAreaUnits::class, 'id', 'area_unit_id');
    }

    public function getValueAttribute($value)
    {
        return floatval($value);
    }
}
