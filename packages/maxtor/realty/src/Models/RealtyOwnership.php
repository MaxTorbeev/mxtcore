<?php

namespace MaxTor\Realty\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\Filterable;
use MaxTor\MXTCore\Traits\UserManagement;

class RealtyOwnership extends Model
{
    use Cacheable,
        UserManagement,
        Sluggable,
        SoftDeletes,
        Filterable;

    protected $table = 'realty_ownerships';

    protected $guarded = ['id'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['name', 'type'],
                'separator' => '-'
            ],
        ];
    }
}