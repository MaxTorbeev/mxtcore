<?php

namespace MaxTor\Realty\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\Content\Traits\Seo;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\Location\Models\Location;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\Filterable;
use MaxTor\MXTCore\Traits\UserManagement;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RealtyCategory extends Model
{
    use Cacheable,
        UserManagement,
        Sluggable,
        Seo,
        SoftDeletes,
        Filterable;

    protected $table = 'realty_categories';

    protected $guarded = ['id'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function realty()
    {
        return $this->hasMany(Realty::class, 'realty_category_id', 'id');
    }
}
