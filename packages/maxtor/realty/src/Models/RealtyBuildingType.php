<?php

namespace MaxTor\Realty\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\MXTCore\Traits\UserManagement;

class RealtyBuildingType extends Model
{
    use SoftDeletes;

    protected $table = 'realty_building_types';

    protected $guarded = ['id'];

    public function realty()
    {
        return $this->hasMany(Realty::class, 'realty_building_type_id', 'id');
    }

    public function getNameAttribute($value)
    {
        return capital_letter($value);
    }
}


