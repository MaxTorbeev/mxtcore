<?php

namespace MaxTor\Realty\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\Location\Models\Location;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\Filterable;
use MaxTor\MXTCore\Traits\UserManagement;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RealtyGarageType extends Model
{
    use Cacheable,
        UserManagement,
        Sluggable,
        SoftDeletes,
        Filterable;

    protected $table = 'realty_garage_types';

    protected $guarded = ['id'];

    public function realty()
    {
        return $this->hasMany(Realty::class, 'id', 'garage_type_id');
    }
}
