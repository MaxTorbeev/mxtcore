<?php

namespace MaxTor\Realty\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\Location\Models\Location;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\Filterable;
use MaxTor\MXTCore\Traits\UserManagement;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class RealtyVas
 *
 * Дополнительная опция по продвижению объявления.
 * Элемент указывается, если к объявлению должна быть применена дополнительная опция.
 *
 * Опции нельзя подключить к объявлениям без фотографий.
 *
 * @package MaxTor\Realty\Models
 */
class RealtyVas extends Model
{
    use Cacheable,
        UserManagement,
        Sluggable,
        SoftDeletes,
        Filterable;

    protected $table = 'realty_vas';

    public function realty()
    {
        return $this->hasMany(Realty::class, 'id', 'realty_vas_id');
    }
}
