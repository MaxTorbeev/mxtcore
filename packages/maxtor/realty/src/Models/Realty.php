<?php

namespace MaxTor\Realty\Models;

use App\User;
use MaxTor\MXTCore\Traits\Viewable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\RecordsPhoto;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\Location\Models\Location;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\Filterable;
use MaxTor\MXTCore\Traits\UserManagement;
use MaxTor\Trade\Models\Price;
use MaxTor\Trade\Models\PriceType;
use CyrildeWit\EloquentViewable\Contracts\Viewable as ViewableContract;

class Realty extends Model implements ViewableContract
{
    use RecordsPhoto,
        ContentScopes,
        Cacheable,
        UserManagement,
        SoftDeletes,
        Viewable,
        Filterable;

    protected $table = 'realty';

    protected $with = ['prices', 'agentUser', 'areas', 'type'];

    protected $guarded = ['id'];

    public function scopePublished($query, $ability = 'create_post')
    {
        return $query->where('realty.enabled', true);
    }

    public function resolveRouteBinding($value)
    {
        return $this->where('import_internal_id', $value)->published()->firstOrFail();
    }

    public function getRouteKeyName()
    {
        return 'import_internal_id';
    }

    public function getPrice($type = 'total-cost')
    {
        return Cache::rememberForever($this->cacheKey() . $type, function () use ($type) {
            $totalCostType = PriceType::where('slug', $type)->first(['id']);

            return $this->prices()->where('price_type_id', $totalCostType->id)->first();
        });
    }

    public function setNameAttribute()
    {
        $address = '';

        $area = trans_choice('realty.areas.units-only.' . $this->area_model->unit->name, $this->area_model->value);

        if ($this->category->name === 'квартира') {
            $name = __('realty.' . ($this->dwelling_description->{'studio'} ? 'studio' : 'apartment') . '.name', [
                'rooms' => $this->rooms,
                'area' => $area
            ]);
        } else {
            if (isset($this->location->address->name))
                $address = $this->location->address->name;

            $name = rtrim($this->category->name . ', ' . $address, ',');
        }

        $this->attributes['name'] = rtrim(trim($name), ',');
    }

    public function getFullAddressAttribute()
    {
        return Cache::rememberForever($this->cacheKey() . ':full_address', function () {
            $subLocalityName = isset($this->location->subLocalityName) ? ', ' . $this->location->subLocalityName->name : null;
            $address = isset($this->location->address) ? ', ' . $this->location->address->name : null;

            return $this->location->localityName->name . $subLocalityName . $address;
        });
    }

    /**
     * @todo refactoring this/
     * @return mixed
     */
    public function getAreaModelAttribute()
    {
        if (app()->runningInConsole()) {
            if ($area = $this->areas()->where('type', 'area')->first()) {
                return $area;
            } else {
                return $this->areas()->where('type', 'lot-area')->first();
            }
        }

        return Cache::rememberForever($this->cacheKey() . ':area_model', function () {
            if ($area = $this->areas()->where('type', 'area')->first()) {
                return $area;
            } else {
                return $this->areas()->where('type', 'lot-area')->first();
            }
        });
    }

    public function setAreaAttribute(): void
    {
        if ($area = $this->areas()->where('type', 'area')->first()) {
        } else {
            $area = $this->areas()->where('type', 'lot-area')->first();
        }

        $this->attributes['area'] = trans_choice('realty.areas.units-only.' . $area->unit->name, $area->value);
    }

    public function setAddressAttribute()
    {
        $subLocalityName = isset($this->location->subLocalityName) ? ', ' . $this->location->subLocalityName->name : null;
        $address = isset($this->location->address) ? ', ' . $this->location->address->name : null;

        $this->attributes['address'] = $this->location->localityName->name . $subLocalityName . $address;
    }

    public function setTypeByName($name)
    {
        if (strlen((string)$name) > 0) {
            $this->realty_type_id = RealtyType::firstOrCreate(['name' => (string)$name])->id;
        }

        return null;
    }

    public function setCategoryByImportInternalId($id, $name, $feed = null)
    {
        if (RealtyCategory::where('import_internal_id', $id)->exists()) {
            $this->realty_category_id = RealtyCategory::where('import_internal_id', $id)->first()->id;
        } else {
            $this->realty_category_id = RealtyCategory::create([
                'name' => (string)$name,
                'import_internal_id' => $id
            ])->id;
        }

//        if ((string)$feed->{'deal-status'} == 'прямая продажа' && (string)$feed->{'category'} == 'квартира') {
//            // Записываем в новостройку
//            $this->realty_category_id = RealtyCategory::where('slug', 'new-flats')->first()->id;
//        } else {
//
//        }
    }

    public function setRenovationByName($name)
    {
        if (strlen((string)$name) > 0) {
            $this->realty_renovation_id = RealtyRenovation::firstOrCreate(['name' => (string)$name])->id;
        }
    }

    public function setOwnershipByOffer($offer)
    {
        $this->realty_ownership_id = RealtyOwnership::firstOrCreate([
            'type' => (string)$offer->{'ownership-type'},
            'name' => (string)$offer->{'ownership-type-name'}
        ])->id;
    }

    public function setBuildingTypeByName($name)
    {
        if (strlen((string)$name) > 0) {
            $this->realty_building_type_id = RealtyBuildingType::firstOrCreate(['name' => (string)$name])->id;
        }
    }

    public function setGarageTypeByName($name)
    {
        if (strlen((string)$name) > 0) {
            $this->garage_type_id = RealtyGarageType::firstOrCreate(['name' => (string)$name])->id;
        }
    }

    public function setRentPeriodByName($name, $period = null)
    {
        if (strlen((string)$name) > 0) {
            $this->rent_period_id = RealtyRentPeriod::firstOrCreate([
                'name' => (string)$name,
                'period' => (string)$period
            ])->id;
        }
    }

    public function type()
    {
        return $this->hasOne(RealtyType::class, 'id', 'realty_type_id');
    }

    public function location()
    {
        return $this->hasOne(Location::class, 'id', 'location_id');
    }

    public function prices()
    {
        return $this->belongsToMany(Price::class, 'realty_price');
    }

    public function rentPeriod()
    {
        return $this->hasOne(RealtyRentPeriod::class, 'id', 'rent_period_id');
    }

    public function propertyType()
    {
        return $this->hasOne(RealtyPropertyType::class, 'id', 'realty_property_type_id');
    }

    public function category()
    {
        return $this->hasOne(RealtyCategory::class, 'id', 'realty_category_id');
    }

    public function vas()
    {
        return $this->hasOne(RealtyVas::class, 'id', 'realty_vas_id');
    }

    public function renovation()
    {
        return $this->hasOne(RealtyRenovation::class, 'id', 'realty_renovation_id');
    }

    public function quality()
    {
        return $this->hasOne(RealtyQuality::class, 'id', 'realty_quality_id');
    }

    public function ownership()
    {
        return $this->hasOne(RealtyOwnership::class, 'id', 'realty_ownership_id');
    }

    public function photos()
    {
        return $this->belongsToMany(Photo::class, 'realty_photo');
    }

    public function areas()
    {
        return $this->belongsToMany(RealtyArea::class, 'realty_area', 'realty_id', 'area_id');
    }

    public function buildingType()
    {
        return $this->hasOne(RealtyBuildingType::class, 'id', 'realty_building_type_id');
    }

    public function agentUser()
    {
        return $this->hasOne(User::class, 'id', 'agent_user_id');
    }

    public function garageType()
    {
        return $this->hasOne(RealtyGarageType::class, 'id', 'garage_type_id');
    }

    public function setRoomsAttribute($value)
    {
        if (strlen((string)$value) === 0) {
            $this->attributes['rooms'] = null;
        } else {
            if (mb_strtolower((string)$value) === 'студия') {
                $this->attributes['rooms'] = -1;
            } else {
                $this->attributes['rooms'] = (int)$value;
            }
        }
    }

    public function setRoomsOfferedAttribute($value)
    {
        if (strlen((string)$value) === 0) {
            $this->attributes['rooms_offered'] = null;
        } else {
            $this->attributes['rooms_offered'] = (int)$value;
        }
    }

    public function setFloorAttribute($value)
    {
        if (strlen((string)$value) === 0) {
            $this->attributes['floor'] = null;
        } else {
            $this->attributes['floor'] = (int)$value;
        }
    }

    public function setFloorsTotalAttribute($value)
    {
        if (strlen((string)$value) === 0) {
            $this->attributes['floors_total'] = null;
        } else {
            $this->attributes['floors_total'] = (int)$value;
        }
    }

    public function setMortgageAttribute($value)
    {
        if (strlen((string)$value) === 0) {
            $this->attributes['mortgage'] = null;
        } else {
            $this->attributes['mortgage'] = (boolean)$value;
        }
    }

    public function setRentPledgeAttribute($value)
    {
        if (strlen((string)$value) === 0) {
            $this->attributes['rent_pledge'] = null;
        } else {
            $this->attributes['rent_pledge'] = (boolean)$value;
        }
    }

    public function setPrepaymentAttribute($value)
    {
        $this->attributes['prepayment'] = floatval($value);
    }

    public function getDwellingDescriptionAttribute($value)
    {
        return json_decode($value);
    }

    public function setDwellingDescriptionAttribute($value)
    {
        $this->attributes['dwelling_description'] = json_encode($value);
    }

    public function getBuildingDescriptionAttribute($value)
    {
        return json_decode($value);
    }

    public function setBuildingDescriptionAttribute($value)
    {
        $this->attributes['building_description'] = json_encode($value);
    }

    public function setGarageDescriptionAttribute($value)
    {
        $this->attributes['garage_description'] = json_encode($value);
    }

    public function getGarageDescriptionAttribute($value)
    {
        return json_decode($value);
    }
}
