<?php

namespace MaxTor\Realty\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use MaxTor\Content\Mail\BackCall;
use MaxTor\Content\Models\Feedback;
use MaxTor\Content\Requests\BackCallRequest;
use MaxTor\Location\Models\LocalityName;
use MaxTor\Realty\Filters\RealtyFilters;
use MaxTor\Realty\Models\Realty;
use MaxTor\Realty\Models\RealtyCategory;
use MaxTor\Realty\Models\RealtyType;

class RealtyController extends Controller
{
    public function index(RealtyFilters $filters, Request $request)
    {
        $realty = Realty::latest()
            ->published()
            ->filters($filters)
            ->paginate($request->get('limit') ?? config('mxtcore.paginate', 18));

        return view('realty::frontend.index', compact('realty'));
    }

    public function locality(LocalityName $locality, RealtyFilters $filters, Request $request)
    {
        $realty = Realty::select('realty.*')
            ->join('locations', 'realty.location_id', '=', 'locations.id')
            ->join('location_locality_names', 'locations.locality_name_id', '=', 'location_locality_names.id')
            ->where(['location_locality_names.id' => (int)$locality->id])
            ->filters($filters)
            ->published()
            ->paginate($request->get('limit') ?? config('mxtcore.paginate', 18));

        return view('realty::frontend.locality', compact('locality', 'realty'));
    }

    public function type(RealtyFilters $filters, LocalityName $locality, RealtyType $type)
    {
        $realty = Realty::select('realty.*')
            ->join('locations', 'realty.location_id', '=', 'locations.id')
            ->join('location_locality_names', 'locations.locality_name_id', '=', 'location_locality_names.id')
            ->where(['location_locality_names.id' => (int)$locality->id])
            ->where(['realty_type_id' => $type->id])
            ->filters($filters)
            ->published()
            ->paginate(config('mxtcore.paginate', 18));

        return view('realty::frontend.type', compact('locality', 'type', 'realty'));
    }

    public function category(RealtyFilters $filters, LocalityName $locality, RealtyType $type, RealtyCategory $category)
    {
        $realty = Realty::select('realty.*')
            ->join('locations', 'realty.location_id', '=', 'locations.id')
            ->join('location_locality_names', 'locations.locality_name_id', '=', 'location_locality_names.id')
            ->where(['location_locality_names.id' => (int)$locality->id])
            ->where(['realty_type_id' => $type->id])
            ->where(['realty_category_id' => $category->id])
            ->filters($filters)
            ->published()
            ->paginate(config('mxtcore.paginate', 18));

        return view('realty::frontend.category', compact('locality', 'type', 'category', 'realty'));
    }

    public function show(Realty $realty)
    {
        views($realty)->record();

        return view('realty::frontend.show', compact('realty'));
    }

    public function booking(Realty $realty, BackCallRequest $request)
    {
        Feedback::create($request->all());

        Mail::to($realty->agentUser()->email)->send(new BackCall($request));

        return response()->json(['message' => 'Сообщение отправлено!']);
    }
}
