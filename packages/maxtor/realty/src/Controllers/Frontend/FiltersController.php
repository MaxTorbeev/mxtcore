<?php

namespace MaxTor\Realty\Controllers\Frontend;

use App\Http\Controllers\Controller;
use MaxTor\Location\Models\LocalityName;
use MaxTor\Realty\Filters\RealtyFilters;
use MaxTor\Realty\Models\Realty;
use MaxTor\Realty\Models\RealtyCategory;
use MaxTor\Realty\Models\RealtyType;
use Symfony\Component\HttpFoundation\Request;

class FiltersController extends Controller
{
    public function index(RealtyFilters $filters, Request $request)
    {
        $realty = Realty::latest()
            ->published()
            ->filters($filters)
            ->paginate($request->get('limit') ?? config('mxtcore.paginate', 18));

        return view('realty::frontend.index', compact('realty'));
    }
}