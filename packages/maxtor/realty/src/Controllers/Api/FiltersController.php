<?php

namespace MaxTor\Realty\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use MaxTor\Location\Models\LocalityName;
use MaxTor\Location\Models\Metro;
use MaxTor\Realty\Filters\RealtyFilters;
use MaxTor\Realty\Filters\RealtyRangesFilters;
use MaxTor\Realty\Models\Realty;
use MaxTor\Realty\Models\RealtyBuildingType;
use MaxTor\Realty\Models\RealtyCategory;
use MaxTor\Realty\Models\RealtyType;

class FiltersController extends Controller
{


    public function build(Request $request, RealtyFilters $filters)
    {
        $realty = $realty = Realty::latest()
            ->published()
            ->filters($filters);

        $metro = $request->get('locality')
            ? Metro::enabled()->groupBy('name')
                ->where('locality_name_id', $request->get('locality'))
                ->get('name')
            : null;

        return [
            'ranges' => [
                $filters->getFieldRanges()->first()
            ],
            'types' => RealtyType::enabled()->select('id', 'slug', 'name')->get(),
            'categories' => RealtyCategory::enabled()->select('id', 'slug', 'name')->get(),
            'fields' => [
                'locality' => LocalityName::enabled()->select('id', 'slug', 'name')->get(),
                'metro' => $metro,
                'rooms' => ['1', '2', '3', '4', '5+'],
                'buildingTypes' => RealtyBuildingType::select('id', 'slug', 'name')->get(),
                'floors' => RealtyFilters::FLOORS_FILTERS,
            ]
        ];
    }
}
