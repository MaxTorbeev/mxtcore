<?php

namespace MaxTor\Realty\Services\Import;

use App\User;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use MaxTor\Content\Models\Photo;
use MaxTor\Location\Models\Address;
use MaxTor\Location\Models\Country;
use MaxTor\Location\Models\District;
use MaxTor\Location\Models\LocalityName;
use MaxTor\Location\Models\Location;
use MaxTor\Location\Models\Metro;
use MaxTor\Location\Models\Region;
use MaxTor\Location\Models\SubLocalityName;
use MaxTor\MXTCore\Services\Import\AbstractXMLImport;
use MaxTor\Personal\Models\ContactPosition;
use MaxTor\Realty\Models\Realty;
use MaxTor\Realty\Models\RealtyArea;
use MaxTor\Realty\Models\RealtyAreaUnits;
use MaxTor\Realty\Models\RealtyCategory;
use MaxTor\Realty\Models\RealtyDealStatus;
use MaxTor\Realty\Models\RealtyPropertyType;
use MaxTor\Realty\Models\RealtyRentPeriod;
use MaxTor\Realty\Models\RealtyType;
use MaxTor\Trade\Models\Price;
use MaxTor\Trade\Models\PriceCurrency;
use MaxTor\Trade\Models\PricePeriod;
use MaxTor\Trade\Models\PriceType;
use MaxTor\Trade\Models\PriceUnit;

/**
 * Class YRLImport - импорт XML-фида для жилой недвижимости.
 *
 * @create 05.06.2019
 * @author me@maxtor.name
 */
class YRLImport extends AbstractXMLImport
{
    public function setFeeds()
    {
        parent::setFeeds();
    }

    static function recordFeedToDatabase($feed)
    {
        if ($internalId = $feed->attributes()->{'internal-id'}) {
            if ($model = Realty::where('import_internal_id', $internalId)->first()) {
                $realty = $model;
                $realty->enabled = true;
            } else {
                $realty = new Realty();
                $realty->import_internal_id = $internalId;
            }

            $realty->title = (string)$feed->{'title'};

            $realty->setTypeByName($feed->type);

            $realtyPropertyType = RealtyPropertyType::firstOrCreate(['name' => $feed->{'property-type'}]);
            $realty->realty_property_type_id = $realtyPropertyType->id;

            $realty->setCategoryByImportInternalId($feed->{'category-id'}, $feed->{'category'}, $feed);

            $realty->location_id = self::location($feed)->id;
            $realty->rent_period_id = self::firstOrCreate($feed->{'price-area-base'}->{'period'}, RealtyRentPeriod::class);
            $realty->realty_deal_status_id = self::firstOrCreate($feed->{'deal-status'}, RealtyDealStatus::class);

            $realty->setRenovationByName($feed->renovation);
            $realty->setRentPeriodByName($feed->{'price'}->{'period'}, $feed->{'price'}->{'rent-period'});

            $realty->cadastral_number = (string)$feed->{'cadastral_number'};
            $realty->haggle = (string)$feed->{'haggle'};
            $realty->mortgage = (string)$feed->{'mortgage'};
            $realty->rent_pledge = (string)$feed->{'rent-pledge'};

            $realty->dwelling_description = [
                'apartments' => self::booleanChoice($feed->{'rent-apartments'}),
                'studio' => (string)$feed->{'studio'},
                'open_plan' => self::booleanChoice($feed->{'open-plan'}),
                'rooms_type' => (string)$feed->{'rooms-type'},
                'window_view' => (string)$feed->{'window-view'},
                'balcony' => (string)$feed->{'balcony'},
                'bathroom_unit' => (string)$feed->{'bathroom-unit'},
                'air_conditioner' => self::booleanChoice($feed->{'air-conditioner'}),
                'phone' => self::booleanChoice($feed->{'phone'}),
                'internet' => self::booleanChoice($feed->{'internet'}),
                'room_furniture' => self::booleanChoice($feed->{'room-furniture'}),
                'kitchen-furniture' => self::booleanChoice($feed->{'kitchen-furniture'}),
                'television' => self::booleanChoice($feed->{'television'}),
                'washing_machine' => self::booleanChoice($feed->{'washing-machine'}),
                'dishwasher' => self::booleanChoice($feed->{'dishwasher'}),
                'refrigerator' => self::booleanChoice($feed->{'refrigerator'}),
                'built_in_tech' => self::booleanChoice($feed->{'built-in-tech'}),
                'floor_covering' => self::booleanChoice($feed->{'floor-covering'}),
                'with_children' => self::booleanChoice($feed->{'with-children'}),
                'with_pets' => self::booleanChoice($feed->{'with-pets'}),
            ];

            $realty->setBuildingTypeByName($feed->{'building-type'});
            $realty->building_description = [
                'building_section' => (string)$feed->{'building-section'},
                'ceiling_height' => (string)$feed->{'ceiling-height'},
                'guarded_building' => (string)$feed->{'guarded-building'},
                'pmg' => self::booleanChoice($feed->{'pmg'}),
                'access_control_system' => self::booleanChoice($feed->{'access-control-system'}),
                'lift' => self::booleanChoice($feed->{'lift'}),
                'rubbish_chute' => self::booleanChoice($feed->{'rubbish-chute'}),
                'electricity_supply' => self::booleanChoice($feed->{'rubbish-chute'}),
                'water_supply' => self::booleanChoice($feed->{'water-supply'}),
                'gas_supply' => self::booleanChoice($feed->{'gas-supply'}),
                'sewerage_supply' => self::booleanChoice($feed->{'sewerage-supply'}),
                'heating_supply' => self::booleanChoice($feed->{'heating-supply'}),
                'toilet' => self::booleanChoice($feed->{'toilet'}),
                'shower' => self::booleanChoice($feed->{'shower'}),
                'pool' => self::booleanChoice($feed->{'pool'}),
                'billiard' => self::booleanChoice($feed->{'billiard'}),
                'sauna' => self::booleanChoice($feed->{'sauna'}),
                'parking' => self::booleanChoice($feed->{'parking'}),
                'parking_places' => self::booleanChoice($feed->{'parking-places'}),
                'parking_place_price' => self::booleanChoice($feed->{'parking-place-price'}),
                'parking_guest' => self::booleanChoice($feed->{'parking-guest'}),
                'parking_guest_places' => self::booleanChoice($feed->{'parking-guest-places'}),
                'alarm' => self::booleanChoice($feed->{'alarm'}),
                'flat_alarm' => self::booleanChoice($feed->{'flat-alarm'}),
                'security' => self::booleanChoice($feed->{'security'}),
                'is_elite' => self::booleanChoice($feed->{'is-elite'}),
            ];

            $realty->setGarageTypeByName((string)$feed->{'garage_type'});

            $realty->garage_description = [
                'garage_name' => (string)$feed->{'garage_name'},
                'parking_type' => (string)$feed->{'parking-type'},
                'heating_supply' => self::booleanChoice($feed->{'heating-supply'}),
                'electricity_supply' => self::booleanChoice($feed->{'electricity-supply'}),
                'water_supply' => self::booleanChoice($feed->{'water-supply'}),
                'security' => self::booleanChoice($feed->{'security'}),
                'automatic_gates' => self::booleanChoice($feed->{'automatic-gates'}),
                'cctv' => self::booleanChoice($feed->{'cctv'}),
                'fire_alarm' => self::booleanChoice($feed->{'fire-alarm'}),
                'access_control_system' => self::booleanChoice($feed->{'access-control-system'}),
                'inspection_pit' => self::booleanChoice($feed->{'inspection-pit'}),
                'cellar' => self::booleanChoice($feed->{'cellar'}),
                'car_wash' => self::booleanChoice($feed->{'car-wash'}),
                'auto_repair' => self::booleanChoice($feed->{'auto-repair'}),
                'new_parking' => self::booleanChoice($feed->{'new-parking'}),
            ];

            $realty->prepayment = (string)$feed->{'prepayment'};
            $realty->description = (string)$feed->{'description'};

            if (config('realty.import.agents', true)) {
                $realty->agent_user_id = self::setSalesAgent($feed)->id;
            }

            $realty->rooms = $feed->{'rooms'};
            $realty->rooms_offered = $feed->{'rooms-offered'};
            $realty->floors_total = $feed->{'floors-total'};
            $realty->floor = $feed->{'floor'};
            $realty->built_year = $feed->{'built-year'};

            $realty->setOwnershipByOffer($feed);

            $realty->save();

            foreach ($realty->areas as $record) {
                $record->delete();
            }
            $realty->areas()->sync(self::areas($feed));

            /** @todo refactoring here */
            foreach ($realty->prices as $record) {
                $record->delete();
            }

            $realty->prices()->sync(self::prices($feed, $realty));

            foreach ($realty->photos() as $photo) {
                if($photo){
                    $photo->delete();
                }
            }

            unset($record);

            foreach ($feed->{'image'} as $photo) {
                self::uploadPhotoFile($realty, $photo);
            }

            $realty->name = 'generated name';
            $realty->area = 'generated name';
            $realty->address = 'generated name';
            $realty->save();

            return $realty;
        }

        return null;
    }

    protected static function setSalesAgent($feed)
    {
        $user = User::firstOrNew([
            'email' => (string)$feed->{'sales-agent'}->{'email'},
        ]);

        $user->name = (string)$feed->{'sales-agent'}->{'name'};

        if(!$user->password){
            $user->password = bcrypt(Str::random(10));
        }

        $user->save();

        $user->contact()->update([
            'first_name' => $feed->{'sales-agent-firstname'},
            'last_name' => $feed->{'sales-agent-lastname'},
            'father_name' => $feed->{'sales-agent-fathername'},
            'contact_position_id' => ContactPosition::firstOrCreate(['name' => (string)$feed->{'sales-agent-role'}])->id,
            'phones' => json_encode([$feed->{'sales-agent-phone'}]),
            'emails' => json_encode((string)$feed->{'sales-agent'}->{'email'}),
        ]);

        if (config('realty.import.agents-photo', false)) {
            if ($feed->{'sales-agent-photo'}) {
                self::uploadPhotoFile($user->contact, $feed->{'sales-agent-photo'});
            }
        }

        return $user;
    }

    protected static function areas($offer)
    {
        $areas = [];

        if ($offer->{'area'}) {
            $areas[] = RealtyArea::create([
                'value' => (string)$offer->{'area'}->{'value'},
                'area_unit_id' => RealtyAreaUnits::firstOrCreate(['name' => (string)$offer->{'area'}->{'unit'}])->id,
                'type' => 'area'
            ]);
        }

        if ($offer->{'room-space'}) {
            foreach ($offer->{'room-space'} as $roomSpace) {
                $areas[] = RealtyArea::create([
                    'value' => (string)$roomSpace->{'value'},
                    'area_unit_id' => RealtyAreaUnits::firstOrCreate(['name' => $roomSpace->{'unit'}])->id,
                    'type' => 'room-space'
                ]);
            }
        }

        if ($offer->{'kitchen-space'}) {
            $areas[] = RealtyArea::create([
                'value' => floatval($offer->{'kitchen-space'}->{'value'}),
                'area_unit_id' => RealtyAreaUnits::firstOrCreate(['name' => (string)$offer->{'kitchen-space'}->{'unit'}])->id,
                'type' => 'kitchen-space'
            ]);
        }

        if ($offer->{'lot-area'}) {
            $areas[] = RealtyArea::create([
                'value' => floatval($offer->{'lot-area'}->{'value'}),
                'area_unit_id' => RealtyAreaUnits::firstOrCreate(['name' => (string)$offer->{'lot-area'}->{'unit'}])->id,
                'type' => 'lot-area'
            ]);
        }


        $ids = [];

        foreach ($areas as $area) {
            $ids[] = $area->id;
        }

        return $ids;
    }

    protected static function uploadPhotoFile($linkedModel, $photo)
    {
        if (strlen((string)$photo) == 0)
            return null;

        $filesystem = new Filesystem();

        $uploadFile = (new YRLImport)->saveTempFile((string)$photo);
        $photo = Photo::named($uploadFile);

        $photoDir = public_path($photo->path);

        if (!$filesystem->exists($photoDir)) {
            $filesystem->makeDirectory($photoDir, 0755, true);
        }

        if (copy($uploadFile->getRealPath(), public_path($photo->path . '/' . $photo->name))) {
            if ($linkedModel->addPhoto($photo)) {
                unlink($uploadFile->getRealPath());
            }
        }
    }

    protected static function prices($feed, $realty)
    {
        $totalPrice = new Price();

        $totalPrice->price_type_id = PriceType::where('slug', 'total-cost')->first()->id;
        $totalPrice->value = (float)$feed->{'price'}->{'value'};
        $totalPrice->setCurrencyByName($feed->{'price'}->{'currency'});
        $totalPrice->setPeriodByName($feed->{'price'}->{'period'});
        $totalPrice->setUnitByName($feed->{'price'}->{'unit'});
        $totalPrice->utilities = $feed->{'price'}->{'utilities_included'};

        $totalPrice->save();

        $costPerUnitPrice = new Price();

        if ($realty->areas()->where('type', 'area')->exists()) {
            $area = $realty->areas()->where('type', 'area')->first();
        } else {
            $area = $realty->areas()->where('type', 'lot-area')->first();
        }

        $costPerUnitPrice->price_type_id = PriceType::where('slug', 'cost-per-unit')->first()->id;
        $costPerUnitPrice->value = $totalPrice->value / $area->value;
        $costPerUnitPrice->setCurrencyByName($feed->{'price-area-base'}->{'currency'});
        $costPerUnitPrice->setPeriodByName($feed->{'price-area-base'}->{'period'});
        $costPerUnitPrice->setUnitByName($area->unit->name);
        $costPerUnitPrice->utilities = $feed->{'price-area-base'}->{'utilities_included'};

        $costPerUnitPrice->save();

        return [$totalPrice->id, $costPerUnitPrice->id];
    }

    protected static function location($feed)
    {
        $location = new Location();

        $location->country_id = self::country($feed)->id;
        $location->region_id = self::region($feed, $location->country_id)->id;

        $location->district_id = self::district($feed, $location->region_id)->id;

        if ($localityName = self::localityName($feed, $location)) {
            $location->locality_name_id = $localityName->id;
        }

        if ($address = self::address($feed, $location->locality_name_id)) {
            $location->address_id = $address->id;
        }

        if ($subLocalityName = self::subLocalityName($feed, $location->locality_name_id)) {
            $location->sub_locality_name_id = $subLocalityName->id;
        }

        if ($metro = self::metro($feed, $location->locality_name_id)) {
            $location->metro_id = $metro->id;
        }

        $location->metro_time_on_foot = $feed->{'metro'}->{'time-on-foot'};
        $location->metro_time_on_transport = $feed->{'metro'}->{'time-on-transport'};
        $location->latitude = $feed->{'Latitude'};
        $location->longitude = $feed->{'Longitude'};

        $location->save();

        return $location;
    }

    protected static function country($feed)
    {
        return Country::firstOrCreate(['name' => (string)$feed->{'location'}->{'country'}]);
    }

    protected static function region($feed, $countryId)
    {
        return Region::firstOrCreate(['name' => (string)$feed->{'location'}->{'region'}, 'country_id' => $countryId]);
    }

    protected static function district($feed, $regionId)
    {

        switch ((string)$feed->{'location'}->{'locality-name'}) {
            case 'г. Казань':
                return District::firstOrCreate(['name' => 'Городской округ Казань', 'region_id' => $regionId]);
                break;

            case 'Чебакса с.':
            case 'Каймары с.':
                return District::firstOrCreate(['name' => 'Высокогорский', 'region_id' => $regionId]);
                break;

            case 'Обухово д.':
                return District::firstOrCreate(['name' => 'Лаишевский', 'region_id' => $regionId]);
                break;
        }

        if ($name = (string)$feed->{'location'}->{'district'}) {
            return District::firstOrCreate(['name' => (string)$feed->{'location'}->{'district'}, 'region_id' => $regionId]);
        }

        //@todo исправить это
        $null = new \stdClass();
        $null->id = null;

        return $null;
    }

    protected static function localityName($feed, $location)
    {
        if ($name = (string)$feed->{'location'}->{'locality-name'}) {
            return LocalityName::firstOrCreate([
                'name' => $name,
                'region_id' => $location->region_id,
                'district_id' => $location->district_id,
            ]);
        }

        return null;
    }

    protected static function subLocalityName($feed, $localityNameId)
    {


        if ($name = (string)$feed->{'location'}->{'sub-locality-name'}) {
            return SubLocalityName::firstOrCreate(['name' => $name, 'locality_name_id' => $localityNameId]);
        }

        return null;
    }

    protected static function address($feed, $localityNameId)
    {
        if ($name = (string)$feed->{'location'}->{'address'}) {
            return Address::firstOrCreate(['name' => $name, 'locality_name_id' => $localityNameId]);
        }

        return null;
    }

    protected static function metro($feed, $localityNameId)
    {
        if ($name = (string)$feed->{'location'}->{'metro'}->{'name'}) {
            return Metro::firstOrCreate(['name' => $name, 'locality_name_id' => $localityNameId]);
        }

        return null;
    }

    private static function firstOrCreate($input, $class, $foreignKey = 'name')
    {
        if (strlen((string)($input)) > 0) {
            return $class::firstOrCreate([$foreignKey => $input])->id;
        }

        return null;
    }

    private static function booleanChoice($feed)
    {
        if (is_null($feed))
            return null;

        if ((string)$feed === 'да' || (string)$feed === '1' || (string)$feed === 'true')
            return 'да';

        if ((string)$feed === 'нет' || (string)$feed === '0' || (string)$feed === 'false')
            return 'нет';

        return (string)$feed;
    }

    protected function makePhoto(UploadedFile $file)
    {
        return Photo::named($file)->move($file);
    }
}
