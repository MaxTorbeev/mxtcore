<?php

namespace MaxTor\Realty\Tests\Unit;

use Illuminate\Support\Collection;
use MaxTor\Location\Models\Location;
use MaxTor\Realty\Models\RealtyCategory;
use MaxTor\Realty\Models\RealtyPropertyType;
use MaxTor\Realty\Models\RealtyType;
use MaxTor\Trade\Models\Price;
use MaxTor\Realty\Models\Realty;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RealtyCategoryModelTest extends TestCase
{
    use RefreshDatabase;

    protected $category;

    public function setUp(): void
    {
        parent::setUp();
        $this->category = create(RealtyCategory::class);
    }

    /** @test */
    function a_category_has_realty()
    {
        $this->assertInstanceOf(Collection::class, $this->category->realty);
    }
}
