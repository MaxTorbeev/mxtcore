<?php

namespace MaxTor\Realty\Tests\Unit;

use Illuminate\Support\Collection;
use MaxTor\Location\Models\Location;
use MaxTor\Realty\Models\RealtyCategory;
use MaxTor\Realty\Models\RealtyGarageType;
use MaxTor\Realty\Models\RealtyOwnership;
use MaxTor\Realty\Models\RealtyPropertyType;
use MaxTor\Realty\Models\RealtyQuality;
use MaxTor\Realty\Models\RealtyRenovation;
use MaxTor\Realty\Models\RealtyRentPeriod;
use MaxTor\Realty\Models\RealtyType;
use MaxTor\Realty\Models\RealtyVas;
use MaxTor\Trade\Models\Price;
use MaxTor\Realty\Models\Realty;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RealtyModelTest extends TestCase
{
    use RefreshDatabase;

    protected $realty;

    public function setUp(): void
    {
        parent::setUp();
        $this->realty = create(Realty::class);
    }

    /** @test */
    function a_realty_has_prices()
    {
        $this->assertInstanceOf(Collection::class, $this->realty->prices);
    }

    /** @test */
    function a_realty_has_type()
    {
        $this->assertInstanceOf(RealtyType::class, $this->realty->type);
    }

    /** @test */
    function a_realty_has_property_type()
    {
        $this->assertInstanceOf(RealtyPropertyType::class, $this->realty->propertyType);
    }

    /** @test */
    function a_realty_has_location()
    {
        $this->assertInstanceOf(Location::class, $this->realty->location);
    }

    /** @test */
    function a_realty_has_category()
    {
        $this->assertInstanceOf(RealtyCategory::class, $this->realty->category);
    }

    /** @test */
    function a_realty_has_vas()
    {
        $this->assertInstanceOf(RealtyVas::class, $this->realty->vas);
    }

    /** @test */
    function a_realty_has_rent_period()
    {
        $this->assertInstanceOf(RealtyRentPeriod::class, $this->realty->rentPeriod);
    }

    /** @test */
    function a_realty_has_renovation()
    {
        $this->withOutExceptionHandling();

        $this->assertInstanceOf(RealtyRenovation::class, $this->realty->renovation);
    }

    /** @test */
    function a_realty_has_quality()
    {
        $this->assertInstanceOf(RealtyQuality::class, $this->realty->quality);
    }

    /** @test */
//    function a_realty_has_garage_type()
//    {
//        $this->assertInstanceOf(RealtyGarageType::class, $this->realty->garageType);
//    }

    /** @test */
    function a_realty_has_ownership()
    {
        $this->assertInstanceOf(RealtyOwnership::class, $this->realty->ownership);
    }

    function a_realty_set_type()
    {
        $this->realty->setType('Перепродажи');

        $this->assertEquals($this->realty->type->name, 'Перепродажи');
    }

    function a_realty_has_photos()
    {
        $this->assertInstanceOf(Collection::class, $this->realty->photos);
    }

}
