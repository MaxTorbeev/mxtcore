<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('father_name')->nullable();

            $table->json('attribs')->nullable();
            $table->json('phones')->nullable();
            $table->json('emails')->nullable();
            $table->json('address')->nullable();

            $table->string('organization')->nullable()->index();
            $table->unsignedInteger('contact_position_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();

            $table->text('description')->nullable()->default(null);
            $table->string('note')->nullable()->default(null);

            $table->integer('hits')->default(0);

            $table->seo();
            $table->userManagement();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('contact_position_id')
                ->references('id')
                ->on('contact_positions')
                ->onDelete('cascade');

//            $table->foreign('user_id')
//                ->references('id')
//                ->on('users')
//                ->onDelete('cascade');
        });

        Schema::create('contact_photo', function (Blueprint $table) {
            $table->unsignedInteger('contact_id');
            $table->foreign('contact_id')
                ->references('id')
                ->on('contacts')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedInteger('photo_id');
            $table->foreign('photo_id')
                ->references('id')
                ->on('photos')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
