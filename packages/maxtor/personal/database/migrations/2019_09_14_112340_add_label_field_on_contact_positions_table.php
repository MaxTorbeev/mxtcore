<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLabelFieldOnContactPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact_positions', function (Blueprint $table) {
            $table->string('label')->nullable()->comment('Название публикуемое на сайте');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact_positions', function (Blueprint $table) {
            $table->dropColumn('label');
        });
    }
}
