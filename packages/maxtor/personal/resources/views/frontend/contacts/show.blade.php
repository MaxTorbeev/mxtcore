@extends('layouts.app')

@section('head-meta')
    <title>{{ $contact->full_name }}</title>
    <meta name="keywords" content="{{ $contact->full_name }}">
    <meta name="description" content="{{ $contact->metadesc }}">
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('home.personal.positions.contacts', $content, $contact) }}
@endsection

@section('content')

    <div class="row">
        <div class="col-md-3">
            @isset($contact->photo_preview->url)
            <a href="{{ $contact->photo_preview->url }}" data-fancybox="gallery">
                <img src="{{ $contact->avatar(398, 398) }}" alt="{{ $contact->name }}" class="img-thumbnail">
            </a>
            @endisset

            @isset($contact->attribs->socials)
                <ul class="socialMenu">
                    @foreach($contact->attribs->socials as $socialName => $socialUrl)
                        @if($socialUrl)
                        <li>
                            <a href="{{ $socialUrl }}" class="socialMenu_item socialMenu_item-{{ $socialName }}" target="_blank" rel="nofollow">
                                <i class="fa fa-{{$socialName}}"></i>
                            </a>
                        </li>
                        @endif
                    @endforeach
                </ul>
            @endisset

            <ul class="list-group mt-3">
                @foreach($contact->phones as $phone)
                    @if($phone[0])
                    <li class="list-group-item">
                        <a href="tel:{{ parse_phone($phone[0], true, true) }}" target="_blank" rel="nofollow">
                            {{ parse_phone($phone[0]) }}  {{ $phone[1] ?? null }}
                        </a>
                    </li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div class="col-md-9">
            <h1>{{ $contact->full_name }}</h1>

            <div>
                @foreach($contact->positions as $position)
                    {{ !is_null($position->label) ? $position->label : $position->name }}{{!$loop->last ? ',' : '.' }}
                @endforeach
            </div>

            <div>{!! $contact->description !!}</div>
            @foreach($contact->reviews as $review)
                <div class="card card-withShadow card-hovered card-review mb-4">
                    <div class="card-body">
                        <i class="fas fa-quote-right"></i>
                        <i>{{ $review->user_name }}</i> - <i>{{ $review->title }}</i> <br>
                        {{ $review->body }}
                        <i class="fas fa-quote-right"></i>
                    </div>
                </div>
            @endforeach

            <ul class="list-group mt-3">
                @foreach($contact->attachment as $attachment)
                    <li class="list-group-item">
                        <a href="{{ $attachment->url }}" target="_blank">{{ $attachment->original_name }}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

@endsection
