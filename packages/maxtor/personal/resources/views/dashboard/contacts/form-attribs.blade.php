<div class="form-row align-items-center">
    <div class="col-6">
        <label class="sr-only" for="attribs-socials-vk">Вконтакте</label>
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-vk" aria-hidden="true"></i></div>
            </div>
            <input id="attribs-socials-vk" name="attribs[socials][vk]" type="text" class="form-control"
                   value="{{ isset($attribs->socials->vk) && $attribs->socials->vk ? $attribs->socials->vk : null }}"
                   placeholder="Вконтакте"
            >
        </div>
    </div>
    <div class="col-6">
        <label class="sr-only" for="attribs-socials-facebook">Facebook</label>
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-facebook" aria-hidden="true"></i></div>
            </div>
            <input id="attribs-socials-facebook" name="attribs[socials][facebook]" type="text" class="form-control"
                   value="{{ isset($attribs->socials->facebook) && $attribs->socials->facebook ? $attribs->socials->facebook : null }}"
                   placeholder="Facebook"
            >
        </div>
    </div>
</div>

<div class="form-row align-items-center">
    <div class="col-6">
        <label class="sr-only" for="attribs-socials-instagram">Instagram</label>
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-instagram" aria-hidden="true"></i></div>
            </div>
            <input id="attribs-socials-instagram" name="attribs[socials][instagram]" type="text" class="form-control"
                   value="{{ isset($attribs->socials->instagram) && $attribs->socials->instagram ? $attribs->socials->instagram : null }}"
                   placeholder="Instagram"
            >
        </div>
    </div>
    <div class="col-6">
        <label class="sr-only" for="attribs-socials-twitter">Twitter</label>
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-twitter" aria-hidden="true"></i></div>
            </div>
            <input id="attribs-socials-twitter" name="attribs[socials][twitter]" type="text" class="form-control"
                   value="{{ isset($attribs->socials->twitter) && $attribs->socials->twitter ? $attribs->socials->twitter : null }}"
                   placeholder="Twitter"
            >
        </div>
    </div>
</div>

<div class="form-row align-items-center">
    <div class="col-6">
        <label class="sr-only" for="attribs-socials-whatsapp">Whatsap</label>
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-whatsapp" aria-hidden="true"></i></div>
            </div>
            <input id="attribs-socials-whatsapp" name="attribs[socials][whatsapp]" type="text" class="form-control"
                   value="{{ isset($attribs->socials->whatsapp) && $attribs->socials->whatsapp ? $attribs->socials->whatsapp : null }}"
                   placeholder="Whatsapp"
            >
        </div>
    </div>
    <div class="col-6">
        <label class="sr-only" for="attribs-socials-telegram">Telegram</label>
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-telegram" aria-hidden="true"></i></div>
            </div>
            <input id="attribs-socials-telegram" name="attribs[socials][telegram]" type="text" class="form-control"
                   value="{{ isset($attribs->socials->telegram) && $attribs->socials->telegram ? $attribs->socials->telegram : null }}"
                   placeholder="Telegram"
            >
        </div>
    </div>
</div>

<div class="form-row align-items-center">
    <div class="col-6">
        <label class="sr-only" for="attribs-socials-skype">Skype</label>
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-skype" aria-hidden="true"></i></div>
            </div>
            <input id="attribs-socials-skype" name="attribs[socials][skype]" type="text" class="form-control"
                   value="{{ isset($attribs->socials->skype) && $attribs->socials->skype ? $attribs->socials->skype : null }}"
                   placeholder="Skype"
            >
        </div>
    </div>
</div>