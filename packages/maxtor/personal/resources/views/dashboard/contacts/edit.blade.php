@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.personal.contacts.edit', $contact) }} @endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Редактирование контакта
        </div>

        <div class="card-body">
            <div class="form-row">
                <div class="col-md-9">
                    {!! Form::model($model = $contact, ['url' => route('admin.personal.contacts.update', $contact), 'files' => true]) !!}
                    @method('PATCH')
                    @include ('personal::dashboard.contacts.form', ['submitButtonText' => 'Редактировать контакт' ])
                    {!! Form::close() !!}
                </div>
                <div class="col-md-3">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a id="nav-photos-tab" class="nav-item nav-link active" data-toggle="tab" href="#photos-tab"
                               role="tab" aria-controls="nav-home" aria-selected="true">
                                Фото
                            </a>
                            <a id="nav-attach-tab" class="nav-item nav-link" data-toggle="tab" href="#attach-tab"
                               role="tab" aria-controls="nav-profile" aria-selected="false">
                                Прикрепленные файлы
                            </a>
                        </div>
                    </nav>

                    <div class="tab-content" id="nav-tabContent">
                        <div id="photos-tab" class="tab-pane fade show active">
                            <upload-files url="{{ route('admin.personal.contacts.photo', ['id' => $contact->id]) }}"
                                          field-name="photos">
                            </upload-files>
                            <file-thumbs url="{{ route('api.personal.contacts.photos', $model) }}"></file-thumbs>
                        </div>

                        <div id="attach-tab" class="tab-pane fade ">
                            <upload-files url="{{ route('admin.personal.contacts.attachment', ['id' => $contact->id]) }}"
                                          field-name="attachments">
                            </upload-files>
                            <file-thumbs url="{{ route('api.content.attachment.list', ['subject_id' => $model->id, 'type' => $model->getPhotoType()]) }}"></file-thumbs>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">Отзывы</div>
        <div class="card-body">
            {!! Form::model($review = new MaxTor\Content\Models\Review(), ['url' => route('admin.personal.contacts.reviews.store', $contact)]) !!}
            @method('POST')
            @include ('content::dashboard.reviews.form', ['submitButtonText' => 'Добавить отзыв' ])
            {!! Form::close() !!}
            <hr>

            @foreach($contact->reviews as $review)
                <div class="card">
                    <div class="card-body">
                        <div>
                            {{ $review->name }}
                            <small>{{$review->user_name }} {{ $review->user_phone }}</small>
                        </div>
                        <div>
                            {{ $review->body }}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
