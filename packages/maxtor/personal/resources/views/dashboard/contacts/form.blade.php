<div class="form-row">
    <div class="col-md-4">
        <div class="form-group  {{ $errors->has('first_name') ? 'has-danger' : '' }}">
            {!! Form::label('first_name', 'Имя:') !!}
            {!! Form::text('first_name', $model->first_name ? $model->first_name : $model->first_name, ['class'=>'form-control']) !!}
            @if ($errors->has('first_name'))
                <small class="form-control-feedback">{{ $errors->first('first_name') }}</small>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group  {{ $errors->has('first_name') ? 'has-danger' : '' }}">
            {!! Form::label('last_name', 'Фамилия:') !!}
            {!! Form::text('last_name', $model->last_name ? $model->last_name : $model->last_name, ['class'=>'form-control']) !!}
            @if ($errors->has('last_name'))
                <small class="form-control-feedback">{{ $errors->first('last_name') }}</small>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group  {{ $errors->has('father_name') ? 'has-danger' : '' }}">
            {!! Form::label('father_name', 'Отчество:') !!}
            {!! Form::text('father_name', $model->father_name ? $model->father_name : $model->father_name, ['class'=>'form-control']) !!}
            @if ($errors->has('father_name'))
                <small class="form-control-feedback">{{ $errors->first('father_name') }}</small>
            @endif
        </div>
    </div>
</div>

<div class="form-row">
    <div class="col-sm-8">
        @include('personal::dashboard.contacts.form-attribs', ['attribs' => $model->attribs])
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-envelope"></i></div>
            </div>
            <input name="emails" type="text"
                   class="form-control"
                   value="{{ $model->emails ?? null  }}"
                   placeholder="Электронная почта"
            >
        </div>
        @for ($i = 0; $i < 5; $i++)
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-phone"></i></div>
                </div>
                <input name="phones[{{ $i }}][0]" type="text" class="form-control"
                       value="{{ $model->phones[$i][0] ?? null  }}"
                       placeholder="Номер телефона"
                >
                <input name="phones[{{ $i }}][1]" type="text" class="form-control"
                       value="{{ $model->phones[$i][1] ?? null  }}"
                       placeholder="Комментарий"
                >
            </div>
        @endfor
        <div class="form-group">
            {!! Form::label('description', 'Описание:') !!}
            @if ($errors->has('description'))
                <small class="form-control-feedback">{{ $errors->first('description') }}</small>
            @endif
            <tinymce name="description" html="{{ $model->description }}"></tinymce>
        </div>
    </div>
    <div class="col-sm-4">
        {{--@if($model->exists )--}}
            {{--<file-upload-once--}}
                    {{--upload-url="{{ route('admin.personal.contacts.photo', ['id' => $model->id]) }}"--}}
                    {{--file-list-url="{{ route('api.content.photo.getPhotoList', ['subject_id' => $model->id, 'type' => $model->getPhotoType()]) }}"--}}
            {{--></file-upload-once>--}}
        {{--@else--}}
            {{--<div class="alert alert-info">--}}
                {{--Что бы добавить изображение, необходимо создать контакт--}}
            {{--</div>--}}
        {{--@endif--}}

        {{--<upload-files--}}
                {{--url="{{ route('admin.personal.contacts.attachment', ['id' => $model->id]) }}"--}}
                {{--field-name="attachments"--}}
        {{--></upload-files>--}}

        {{--<file-thumbs--}}
                {{--url="{{ route('api.content.attachment.list', ['subject_id' => $model->id, 'type' => $model->getPhotoType()]) }}"--}}
        {{--></file-thumbs>--}}

        <div class="form-group">
            {!! Form::label('', 'Должности из CRM: ' . $model->positionInCRM->name ) !!}
        </div>

        <div class="form-group">
            {!! Form::label('tags', 'Должности, которые будут опубликованы на сайте:') !!}
            {!! Form::select('positions[]', $positions, $model->positions, ['class'=>'form-control select2', 'multiple', 'data-tags' => 'true']) !!}
        </div>

{{--        <div class="form-group">--}}
{{--            {!! Form::label('contact_position_id', 'Должность:') !!}--}}
{{--            {!! Form::select('contact_position_id', $positions, null, ['class'=>'form-control select']) !!}--}}
{{--        </div>--}}

        @include ('mxtcore::forms.user-management', ['model' => $model])
        @include ('content::forms.seo', ['model' => $model])
        @include ('content::forms.redirect', ['model' => $model])
    </div>
</div>

<div class="panel form_buttonGroups form_buttonGroups-fixed">
    <div class="form-group">
        {!! Form::submit( $submitButtonText, ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('admin.users.index') }}" class="btn btn btn-secondary">Отменить и перейти к списку</a>

        @if($model->exists)
            <button type="submit" name="action" value="delete" class="pull-right btn btn btn-danger">Удалить</button>
        @endif
    </div>
</div>
