@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.personal.contacts.create', $contact) }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            Создание контакта
        </div>

        <div class="card-body">
            {!! Form::model($model = new \MaxTor\Personal\Models\Contact(), ['url' => route('admin.personal.contacts.store', $user), 'files' => true, 'method' => 'post']) !!}
            @include ('personal::dashboard.contacts.form', ['submitButtonText' => 'Создать контакт' ])
            {!! Form::close() !!}
        </div>
    </div>
@endsection