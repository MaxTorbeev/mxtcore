<div class="row no-gutters">
    @foreach($contacts as $contact)
        @isset($contact->position->name)
            <div class="col-lg-2 col-md-3">
                @include('personal::components.contact.card', ['item' => $contact])
            </div>
        @endisset
    @endforeach
</div>
