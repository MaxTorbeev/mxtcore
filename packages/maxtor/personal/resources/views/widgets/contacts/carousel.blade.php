<div class="swiper-container"
     data-slides data-slides-per-view="4"
     data-slides-per-view-xs="2"
>
    <div class="swiper-wrapper">
        @foreach($contacts as $contact)
            @isset($contact->position->name)
                <div class="swiper-slide">
                    @include('personal::components.contact.card', [
                        'item' => $contact,
                    ])
                </div>
            @endisset
        @endforeach
    </div>

    <div class="sliderNavigators sliderNavigators-inWrapper swiper-navigation">
        <div class="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
        <div class="next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
    </div>
</div>
