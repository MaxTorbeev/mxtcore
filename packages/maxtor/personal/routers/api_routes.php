<?php

Route::get('/personal/contacts/{contact}/photos', [
    'as'    => 'api.personal.contacts.photos',
    'uses'  => 'ContactsController@photos'
]);