<?php

Route::get('/users/contacts/{contact}/create', [
    'as'    => 'admin.personal.contacts.create',
    'uses'  => 'ContactsController@create'
]);

Route::post('/users/contacts/{contact}', [
    'as'    => 'admin.personal.contacts.store',
    'uses'  => 'ContactsController@store'
]);

Route::get('/users/contacts/{contact}/edit', [
    'as'    => 'admin.personal.contacts.edit',
    'uses'  => 'ContactsController@edit'
]);

Route::match(['PATCH','PUT'],'/users/contacts/{contact}', [
    'as'    => 'admin.personal.contacts.update',
    'uses'  => 'ContactsController@update'
]);

Route::post('/users/{contact}/photos', [
    'as' => 'admin.personal.contacts.photo',
    'uses' => 'ContactsController@addPhoto'
]);

Route::post('/users/{contact}/attachments', [
    'as' => 'admin.personal.contacts.attachment',
    'uses' => 'ContactsController@addAttachment'
]);

Route::get('/users/contacts/{contact}/edit', [
    'as'    => 'admin.personal.contacts.edit',
    'uses'  => 'ContactsController@edit'
]);

//Reviews

Route::get('/users/contacts/{contact}/review', [
    'as'    => 'admin.personal.contacts.reviews',
    'uses'  => 'ContactsController@reviews'
]);

Route::post('/users/contacts/{contact}/review', [
    'as'    => 'admin.personal.contacts.reviews.store',
    'uses'  => 'ContactsController@reviewStore'
]);

Breadcrumbs::for('admin.personal.contacts.edit', function ($trail, $user) {
    $trail->parent('admin.users.edit', $user);
    $trail->push('Редактирование контакта пользователя ' . $user->name , route('admin.personal.contacts.edit', $user));
});
