<?php

Route::get('/{contactPosition}', [
    'as' => 'frontend.personal.contactPositions.show',
    'uses' => 'ContactPositionsController@show'
]);

Route::get('/{contactPosition}/{contact}', [
    'as' => 'frontend.personal.contacts.show',
    'uses' => 'ContactsController@show'
]);

Route::get('/{contactPosition}/{contact}/reviews/', [
    'as' => 'frontend.personal.contacts.reviews',
    'uses' => 'ContactsController@reviews'
]);

Route::post('/{contactPosition}/{contact}/reviews/', [
    'as' => 'frontend.personal.contacts.addReview',
    'uses' => 'ContactsController@addReview'
]);


// Home > Content > Contacts
Breadcrumbs::for ('home.personal.positions.contacts', function ($trail, $content, $contact) {
    $trail->parent('home.content.categories', $content);
//    $trail->push($contact->name, '/');
//    $trail->push($contact->name, route('frontend.personal.contactPositions.show', [
//        'contactPosition' => $contact->position->slug,
//        'contact'  => $contact->id
//    ]));
});
