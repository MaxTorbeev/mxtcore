<?php

namespace MaxTor\Personal\Models;

use App\User;
use Illuminate\Support\Facades\Cache;
use Laravelrus\LocalizedCarbon\Traits\LocalizedEloquentTrait;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Review;
use MaxTor\Content\Traits\AttribsRecord;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\RecordComment;
use MaxTor\Content\Traits\RecordsAttachment;
use MaxTor\Content\Traits\RecordsPhoto;
use MaxTor\Content\Traits\RecordsReview;
use MaxTor\Content\Traits\RecordsTag;
use MaxTor\Content\Traits\Redirectenable;
use MaxTor\Content\Traits\Searchable;
use MaxTor\Content\Traits\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\Content\Models\Post;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\UserManagement;
use Illuminate\Database\Eloquent\Builder;


class Contact extends Model
{
    use RecordsPhoto,
        RecordsReview,
        RecordsAttachment,
        RecordComment,
        RecordsTag,
        Redirectenable,
        Cacheable,
        UserManagement,
        ContentScopes,
        LocalizedEloquentTrait,
        SoftDeletes,
        Searchable;

    protected $guarded = ['id', '_token', '_method', 'redirect_from'];


    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->father_name;
    }

    public function getFrontendUrlAttribute()
    {
        return route('frontend.personal.contacts.show', [
            'position' => $this->position,
            'id' => $this->id
        ]);
    }

    public function positionInCRM()
    {
        return $this->hasOne(ContactPosition::class, 'id', 'contact_position_id');
    }

    public function position()
    {
        return $this->hasOne(ContactPosition::class, 'id', 'contact_position_id');
    }

    public function positions()
    {
        return $this->belongsToMany(ContactPosition::class, 'contact_position', 'contact_id', 'position_id');
    }

    public function getPhonesAttribute($value)
    {
        return json_decode($value, true);
    }

    public function setPhonesAttribute($value)
    {
        $this->attributes['phones'] = json_encode($value);
    }

    public function getEmailsAttribute($value)
    {
        return json_decode($value);
    }

    public function setEmailsAttribute($value)
    {
        $this->attributes['phones'] = json_encode($value);
    }

    public function getAttribsAttribute($value)
    {
        return json_decode($value);
    }

    public function setAttribsAttribute($value)
    {
        $this->attributes['attribs'] = json_encode($value);
    }

    public function avatar($width = null, $height = null)
    {
        if ($this->photos()->exists()) {
            return $this->photos()->first()->getThumb($width, $height);
        } else {
            return config('mxtcore.common.defaults.avatar');
        }
    }

    public function user()
    {
        $this->hasOne(User::class, 'id', 'user_id');
    }

    public function addPhoto(Photo $photo)
    {
        $photo->type = $this->getPhotoType();

        return $this->photos()->save($photo);
    }

    public function photos()
    {
        return $this->belongsToMany(Photo::class, 'contact_photo');
    }

    public function reviews()
    {
        return $this->morphMany(Review::class, 'subject');
    }
}
