<?php

namespace MaxTor\Personal\Models;

use App\User;
use Illuminate\Support\Facades\Cache;
use Laravelrus\LocalizedCarbon\Traits\LocalizedEloquentTrait;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\RecordComment;
use MaxTor\Content\Traits\RecordsPhoto;
use MaxTor\Content\Traits\RecordsReview;
use MaxTor\Content\Traits\RecordsTag;
use MaxTor\Content\Traits\Searchable;
use MaxTor\Content\Traits\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\Content\Models\Post;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\UserManagement;
use Illuminate\Database\Eloquent\Builder;


class ContactPosition extends Model
{
    use RecordsPhoto,
        RecordComment,
        Sluggable,
        Cacheable,
        UserManagement,
        ContentScopes,
        LocalizedEloquentTrait;

    protected $guarded = ['id', 'redirect_from'];


    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function contactInCRM()
    {
        return $this->hasMany(Contact::class, 'contact_position_id');
    }

    public function contact()
    {
        return $this->belongsToMany(Contact::class, 'contact_position', 'contract_id');
    }
}
