<?php

namespace MaxTor\Personal\Controllers\Admin;


use App\User;
use Gate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;

use MaxTor\Content\Layouts\Admin\CategoriesListLayout;
use MaxTor\Content\Models\Attachment;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Review;
use MaxTor\Content\Models\Tag;
use MaxTor\Content\Requests\AttachmentRequest;
use MaxTor\Content\Requests\CategoryRequest;
use MaxTor\Content\Requests\PhotoRequest;
use MaxTor\Content\Requests\PostRequest;
use MaxTor\Content\Requests\ReviewRequest;
use MaxTor\MXTCore\Controllers\DashboardController;
use MaxTor\Personal\Models\Contact;
use MaxTor\Personal\Models\ContactPosition;

class ContactsController extends DashboardController
{
    protected $contactPositions = [];

    public function __construct()
    {
        parent::__construct();

        $this->contactPositions = ContactPosition::published()->get()->pluck('name', 'id');
    }

    public function create(Contact $contact)
    {
        $positions = $this->contactPositions;

        return view('personal::dashboard.contacts.create', compact('contact', 'positions'));
    }

    public function store(Contact $contact, Request $request)
    {
        $positions = $request->input('positions');

        $request->offsetUnset('positions');
        $contact->create($request->all());

        if($positions) {
            $this->syncPositions($contact, $positions);
        }

        return redirect(route('admin.personal.contacts.edit', $contact))->with('flash', 'Создано');
    }

    public function edit(Contact $contact)
    {
        $positions = $this->contactPositions;
        $reviews = $contact->reviews();

        return view('personal::dashboard.contacts.edit', compact('contact', 'positions', 'reviews'));
    }

    public function update(Contact $contact, Request $request)
    {
        $positions = $request->input('positions');

        $request->offsetUnset('positions');
        $contact->update($request->all());

        if($positions) {
            $this->syncPositions($contact, $positions);
        }

        return redirect(route('admin.personal.contacts.edit', $contact))->with('flash', 'Обновлено');
    }

    public function addAttachment($id, AttachmentRequest $request)
    {
        $attachment = $this->makeAttachment($request->file('attachments'));

        Contact::where('id', $id)->firstOrFail()->addAttachment($attachment);

        return 'Done';
    }

    public function reviewStore(Contact $contact, ReviewRequest $request)
    {
        $review = Review::create($request->all());

        $contact->reviews()->save($review);

        return redirect()->back();
    }


    public function addPhoto(Contact $contact, PhotoRequest $request)
    {
        $photo = $this->makePhoto($request->file('photos'));

        $contact->addPhoto($photo);

        return 'Done';
    }

    protected function makePhoto(UploadedFile $file)
    {
        return Photo::named($file)->move($file);
    }

    protected function makeAttachment(UploadedFile $file)
    {
        return Attachment::named($file)->move($file);
    }

    protected function syncPositions($model, array $tags)
    {
        $tagsInDb = [];

        foreach ($tags as $i => $tag) {
            if (is_numeric($tag)) {
                $tagsInDb[] = ContactPosition::firstOrCreate(['id' => $tag]);
            } else {
                $tagsInDb[] = ContactPosition::firstOrCreate(['name' => $tag]);
            }
        }

        $model->positions()->sync(collect($tagsInDb)->pluck('id'));
    }

}
