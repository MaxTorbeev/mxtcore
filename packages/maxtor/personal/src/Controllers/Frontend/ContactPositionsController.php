<?php

namespace MaxTor\Personal\Controllers\Frontend;

use App\User;
use Gate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use MaxTor\Personal\Models\ContactPosition;


class ContactPositionsController extends Controller
{
    public function show($contactPositionSlug)
    {
        $contactPosition = ContactPosition::where('slug', $contactPositionSlug)->firstOrFail();

        return view('personal::frontend.contacts.show', compact('contact'));
    }
}