<?php

namespace MaxTor\Personal\Controllers\Frontend;

use App\User;
use Gate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Post;
use MaxTor\Content\Models\Review;
use MaxTor\Content\Requests\ReviewRequest;
use MaxTor\Personal\Models\Contact;
use MaxTor\Personal\Models\ContactPosition;


class ContactsController extends Controller
{
    public function show($contactPositionSlug, Contact $contact)
    {
        $content = Category::where('slug', 'kontakty')->first();

        if (!$contact->enabled) {
            abort(404);
        }

        return view('personal::frontend.contacts.show', compact('content', 'contact'));
    }

    public function reviews($contactPositionSlug, $contactSlug)
    {
        $contactPosition = ContactPosition::where('slug', $contactPositionSlug)->firstOrFail();

        $contact = $contactPosition
            ->contact
            ->where('slug', $contactSlug)
            ->first();

        return $contact->reviews()->published()->get();
    }

    public function addReview($contactPositionSlug, $contactSlug, ReviewRequest $request)
    {
        $contactPosition = ContactPosition::where('slug', $contactPositionSlug)->firstOrFail();
        $contact = $contactPosition->contact->where('slug', $contactSlug)->first();

        if ($request->isMethod('post')) {
            if ($review = Review::create($request->all())) {
                $contact->addReview($review);

                return response(['message' => 'Спасибо! Отзыв добавлен и ожидает модерации.'], 200);
            }

            return response(['message' => 'Нет доступа'], 222);
        }

        return abort(422);
    }

}
