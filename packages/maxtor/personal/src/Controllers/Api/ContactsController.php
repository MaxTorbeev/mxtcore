<?php

namespace MaxTor\Personal\Controllers\Api;

use App\Http\Controllers\Controller;
use MaxTor\Personal\Models\Contact;

class ContactsController extends Controller
{
    public function photos(Contact $contact)
    {
        return $contact->photos()
            ->orderBy('_lft')
//            ->select('id', 'name')
            ->get()
            ;
    }
}