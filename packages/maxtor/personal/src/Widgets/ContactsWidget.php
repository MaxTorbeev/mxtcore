<?php

namespace MaxTor\Personal\Widgets;

use MaxTor\MXTCore\Widgets\Widget;
use MaxTor\Personal\Models\Contact;

class ContactsWidget extends Widget
{
    public $packageName = 'personal';

    public function handle($attributes = null)
    {
        $this->attributes = $attributes;
        $this->view = $attributes['view'] ?? 'default';

        $this->cacheKey = 'widgets::pesonal.contacts::' . $this->view;

        return \Cache::remember($this->cacheKey, $this->cacheTime, function () use ($attributes) {
            return $this->view($this->view, ['attributes' => $attributes, 'contacts' => $this->getContacts()]);
        });
    }

    protected function getContacts()
    {
        if ($this->cacheKey !== null) {
            return \Cache::remember($this->cacheKey, $this->cacheTime, function () {
                return $this->getDate();
            });
        } else {
            return $this->getDate();
        }
    }

    protected function getDate()
    {
        return Contact::enabled()
            ->select('contacts.*')
            ->rightJoin('contact_positions as positions', 'contacts.contact_position_id', '=', 'positions.id')
            ->orderBy('positions.ordering', 'ASC')
            ->orderBy('contacts.ordering', 'ASC')
            ->with('position')
            ->get();
    }

}
