<?php

namespace MaxTor\Personal;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use MaxTor\Personal\Models\Contact;
use MaxTor\Personal\Policies\ContactPolicy;
use Illuminate\Support\Facades\Gate;

class PersonalServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'personal');

        $this->publishes([
            __DIR__ . '/../../public' => public_path('packages/maxtor/personal')
        ]);

        //Migration
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        //Routes
        $this->mapAdminRoutes();
        $this->mapApiRoutes();
        $this->mapFrontendRoutes();
    }

    protected function mapFrontendRoutes(): void
    {
        Route::prefix('/personals/')
            ->middleware('web')
            ->namespace('MaxTor\Personal\Controllers\Frontend')
            ->group(__DIR__ . '/../routers/frontend_routes.php');
    }

    protected function mapApiRoutes(): void
    {
        Route::prefix('/api/personals/')
            ->middleware('api')
            ->namespace('MaxTor\Personal\Controllers\Api')
            ->group(__DIR__ . '/../routers/api_routes.php');
    }

    protected function mapAdminRoutes(): void
    {
        Route::prefix('/admin/')
            ->middleware('web')
            ->namespace('MaxTor\Personal\Controllers\Admin')
            ->group(__DIR__ . '/../routers/admin_routes.php');
    }
}