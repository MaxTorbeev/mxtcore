<?php

namespace MaxTor\Personal\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use MaxTor\Personal\Models\Contact;

class ContactPolicy
{
//    use HandlesAuthorization;

    public function before(?User $user)
    {
        if ($user && $user->can('access_dashboard')) {
            return true;
        }

        return true;
    }

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function show(?User $user, Contact $contact)
    {
        if(!$contact->published) {
            return $user->id === $contact->user_id;
        }

        return false;
    }
}
