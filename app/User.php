<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use MaxTor\Content\Models\Post;
use MaxTor\Content\Models\Tag;
use MaxTor\MXTCore\Traits\HasRoles;
use MaxTor\Personal\Models\Contact;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, HasRoles, SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::created(function ($user) {
            $user->contact()->create();
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function owns($related)
    {
        return $this->id == $related->created_user_id;
    }

    /**
     * A user can have many posts
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class, 'created_user_id');
    }

    /**
     * A user can have many categories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories()
    {
        return $this->hasMany(Post::class, 'created_user_id');
    }

    /**
     * A user can have many posts
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function extensions()
    {
        return $this->hasMany('MaxTor\MXTCore\Models\Extension', 'created_user_id');
    }

    /**
     * A user can have many tags
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tags()
    {
        return $this->hasMany(Tag::class, 'created_user_id');
    }

    public function contact()
    {
        return $this->hasOne(Contact::class, 'user_id');
    }
}
