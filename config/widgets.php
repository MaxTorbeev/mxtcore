<?php
/**
 * Widgets list
 */
return [
    'carousels' => \MaxTor\Content\Widgets\CarouselsWidget::class,
    'categories' => \MaxTor\Content\Widgets\CategoriesWidget::class,
    'posts' => \MaxTor\Content\Widgets\PostsWidget::class,
    'contacts' => \MaxTor\Personal\Widgets\ContactsWidget::class,
    'realtyFiltersForms' => \MaxTor\Realty\Widgets\FiltersFormsWidget::class,
];