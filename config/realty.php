<?php

return [
    'limitsOnPage' => [18, 36, 60],
    'no-photo' => '/images/templates/realty/no-photo.svg',

    // Конфиги импортирования объектов недвижимости
    'import' => [
        'agents' => true,
        'agents-photo' => false
    ],
];