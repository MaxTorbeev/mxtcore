<?php
/**
 * Configure file for MXTCore
 */
return [
    'baseUrl'       => 'realty.local',
    'title'         => '',
    'keywords'      => '',
    'description'   => '',
    'generator'     => 'MXTCore',
    'common'        => [
        'defaults'  => [
            'avatar' => '/images/templates/personal/no-photo.svg'
        ]
    ],
    'dashboard'     => [
        'logo'      => [
                'minimized' => '/images/templates/dashboard/logo/logo_small.svg',
                'full' => '/images/templates/dashboard/logo/logo.svg',
        ]
    ],
    'frontend'     => [
        'logo'      => [
            'full' => '/images/templates/realty/logo/full.png?0507',
        ],
        'socials'  => [
            'vk' => '//vk.com/vashnadejnyirieltor',
            'instagram' => '//www.instagram.com/nadezhnyirieltor/',
            'odnoklassniki' => '//ok.ru/group/59327159271470',
            'facebook' => '//www.facebook.com/groups/589602347891260',
        ]
    ],
    'paginate'     => 18,
    'watermark'    => null,
];
