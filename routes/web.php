<?php

Auth::routes();

Route::get('/', 'PagesController@home')->name('home');

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Главная', route('home'));
});