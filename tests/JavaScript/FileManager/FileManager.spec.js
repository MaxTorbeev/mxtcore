import {mount} from '@vue/test-utils'
import expect from 'expect'
import FileManager from './../../../resources/js/Components/Dashboard/FileManager/FileManager.vue'

describe('FileManager', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(FileManager)
    });

    it('presents the title and the body', () => {
        expect(wrapper.html()).toContain('The title', {
            question: {

            }
        });
    })
});